<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Scopes\ActiveScope;
use OwenIt\Auditing\Contracts\Auditable;

class Rider extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    
    protected $auditInclude = [
        'id','name','icno','email','phone','address','accno','bank_id','branch_id','created_by','updated_by','disable',
    ];
	
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new ActiveScope);
    }
	
	public function bank()
    {
        return $this->belongsTo('App\Bank');
    }
	
	public function branch()
    {
        return $this->belongsTo('App\Branch');
    }
	
	public function creator()
    {
        return $this->belongsTo('App\User', 'created_by');
    }
	
	public function updater()
    {
        return $this->belongsTo('App\User', 'updated_by');
    }
}
