<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use App\Parcel;
use DB;

class DashboardController extends Controller
{	
	public function dashboard(Request $request){
		$inbound = Parcel::where('dateout', date('Y-m-d', strtotime("-1 days")))->sum('inbound');
		$outbound = Parcel::where('dateout', date('Y-m-d', strtotime("-1 days")))->sum('outbound');
		$failed = Parcel::where('dateout', date('Y-m-d', strtotime("-1 days")))->sum('failed');
		$lost = Parcel::where('dateout', date('Y-m-d', strtotime("-1 days")))->sum('lost');
		
		$month = date('m');
        $year = date('Y');
        $previousYear = date('Y')-1;
		
		$prevYear = $year;
		$prevMonth = str_pad($month-1, 2, '0', STR_PAD_LEFT);
		
		if($prevMonth == 0){
			$prevMonth = 12;
			$prevYear = $year-1;
		}
		
		$query = "select 
					b.name,
					sum(case when dateout between '28/12/".$previousYear."'::date and '27/01/".$year."'::date then inbound else 0 end) as inbound1,
					sum(case when dateout between '28/01/".$year."'::date and '27/02/".$year."'::date then inbound else 0 end) as inbound2,
					sum(case when dateout between '28/02/".$year."'::date and '27/03/".$year."'::date then inbound else 0 end) as inbound3,
					sum(case when dateout between '28/03/".$year."'::date and '27/04/".$year."'::date then inbound else 0 end) as inbound4,
					sum(case when dateout between '28/04/".$year."'::date and '27/05/".$year."'::date then inbound else 0 end) as inbound5,
					sum(case when dateout between '28/05/".$year."'::date and '27/06/".$year."'::date then inbound else 0 end) as inbound6,
					sum(case when dateout between '28/06/".$year."'::date and '27/07/".$year."'::date then inbound else 0 end) as inbound7,
					sum(case when dateout between '28/07/".$year."'::date and '27/08/".$year."'::date then inbound else 0 end) as inbound8,
					sum(case when dateout between '28/08/".$year."'::date and '27/09/".$year."'::date then inbound else 0 end) as inbound9,
					sum(case when dateout between '28/09/".$year."'::date and '27/10/".$year."'::date then inbound else 0 end) as inbound10,
					sum(case when dateout between '28/10/".$year."'::date and '27/11/".$year."'::date then inbound else 0 end) as inbound11,
					sum(case when dateout between '28/11/".$year."'::date and '27/12/".$year."'::date then inbound else 0 end) as inbound12
				from
					branches b
				left join parcels p on p.branch_id = b.id and p.disable = 'ACTIVE'
				where b.disable = 'ACTIVE'
				group by b.name, b.id
				order by b.id";
		
		$result = DB::select(DB::raw($query));
		
		for($i=0; $i < sizeOf($result); $i++){
			$rs[$i][] = $result[$i]->name;
			$rs[$i][] = $result[$i]->inbound1;
			$rs[$i][] = $result[$i]->inbound2;
			$rs[$i][] = $result[$i]->inbound3;
			$rs[$i][] = $result[$i]->inbound4;
			$rs[$i][] = $result[$i]->inbound5;
			$rs[$i][] = $result[$i]->inbound6;
			$rs[$i][] = $result[$i]->inbound7;
			$rs[$i][] = $result[$i]->inbound8;
			$rs[$i][] = $result[$i]->inbound9;
			$rs[$i][] = $result[$i]->inbound10;
			$rs[$i][] = $result[$i]->inbound11;
			$rs[$i][] = $result[$i]->inbound12;
		}
		
		$querypie = "select 
						b.name,
						sum(case when p.inbound is null then 0 else p.inbound end) as inbound
					from
						branches b
					left join parcels p on p.branch_id = b.id and p.dateout between '".$prevYear."-".$prevMonth."-28'::date and '".$year."-".$month."-27'::date and p.disable = 'ACTIVE'
					where b.disable = 'ACTIVE'
					group by b.name, b.id
					order by b.id";
		
		$resultpie = DB::select(DB::raw($querypie));
		
		for($i=0; $i < sizeOf($resultpie); $i++){
			$pie[$i][] = $resultpie[$i]->name;
			$pie[$i][] = $resultpie[$i]->inbound;
		}
        
        return view('site.dashboard', compact('inbound','outbound','failed','lost', 'rs', 'pie'));
    }
	
	public function getRiderDailyData(Request $request)
    {
		if(empty($request->input('date_'))){
            $date = date('d/m/Y', strtotime("-1 days"));
        }else{
			$date = $request->input('date_');
		}
		
		$rs = DB::select(DB::raw(
				"select
					id,
					branch_id,
					name,
					outbound,
					inbound,
					failed,
					lost,
					case when outbound = 0 then 0 else (inbound::float / outbound::float * 100) end as accuracy
				from
				(
					select 
						r.id,
						r.branch_id,
						r.name,
						case when p.outbound is null then 0 else p.outbound end as outbound,
						case when p.inbound is null then 0 else p.inbound end as inbound,
						case when p.failed is null then 0 else p.failed end as failed,
						case when p.lost is null then 0 else p.lost end as lost
					from
						riders r
					left join parcels p on p.rider_id = r.id and p.dateout between '".$date."'::date and '".$date."'::date and p.disable = 'ACTIVE'
					where r.disable = 'ACTIVE'
					order by r.name, p.branch_id
				) as stat"
			));

    	return DataTables::of($rs)
			->editColumn('name', function ($rs) use ($date){
				return 
					'<a href="'.url("parcelList").'?rider_id='.$rs->id.'&branch_id='.$rs->branch_id.'&type_id=&startdate='.$date.'&enddate='.$date.'">'
						.$rs->name.
					'</a>';
			})
			->editColumn('outbound', function ($rs) {
				return "<span class='label label-default text-lg'>".$rs->outbound."</span>";
			})
			->editColumn('inbound', function ($rs) {
				return "<span class='label label-primary text-lg'>".$rs->inbound."</span>";
			})
			->editColumn('failed', function ($rs) {
				return "<span class='label label-warning text-lg'>".$rs->failed."</span>";
			})
			->editColumn('lost', function ($rs) {
				return "<span class='label label-danger text-lg'>".$rs->lost."</span>";
			})
			->editColumn('accuracy', function ($rs) {
				return number_format($rs->accuracy, '2', '.', '');
			})
			->addColumn('action', function ($rs) use ($date){
                $action = 
                    '<a href="'.url("parcelList").'?rider_id='.$rs->id.'&branch_id='.$rs->branch_id.'&type_id=&startdate='.$date.'&enddate='.$date.'" class="btn btn-outline btn-sm btn-warning" title="Lihat Data">
						<i class="fa fa-search"></i>
					</a>';
                
				return $action;
			})
			->rawColumns(['name','outbound','inbound','failed','lost', 'action'])
			->toJson();
    }
	
	public function getRiderMonthlyData(Request $request)
    {
		if(empty($request->input('month_'))){
            $month = date('m');
        }else{
			$month = $request->input('month_');
		}
		
		if(empty($request->input('year_'))){
            $year = date('Y');
        }else{
			$year = $request->input('year_');
		}
		
		$prevYear = $year;
		$prevMonth = str_pad($month-1, 2, '0', STR_PAD_LEFT);
		
		if($prevMonth == 0){
			$prevMonth = 12;
			$prevYear = $year-1;
		}
		
		$rs = DB::select(DB::raw(
				"select
					id,
					branch_id,
					name,
					outbound,
					inbound,
					failed,
					lost,
					case when outbound = 0 then 0 else (inbound::float / outbound::float * 100) end as accuracy
				from
				(
					select 
						r.id,
						r.branch_id,
						r.name,
						sum(case when p.outbound is null then 0 else p.outbound end) as outbound,
						sum(case when p.inbound is null then 0 else p.inbound end) as inbound,
						sum(case when p.failed is null then 0 else p.failed end) as failed,
						sum(case when p.lost is null then 0 else p.lost end) as lost
					from
						riders r
					left join parcels p on p.rider_id = r.id and p.dateout between '".$prevYear."-".$prevMonth."-28'::date and '".$year."-".$month."-27'::date and p.disable = 'ACTIVE'
					where r.disable = 'ACTIVE'
					group by r.id, r.name, p.branch_id
					order by r.name, p.branch_id
				) as stat"
			));
		
		$startdate = "28/".$prevMonth."/".$prevYear;
		$enddate = "27/".$month."/".$year;

    	return DataTables::of($rs)
			->editColumn('name', function ($rs) use ($startdate, $enddate){
				return 
					'<a href="'.url("parcelList").'?rider_id='.$rs->id.'&branch_id='.$rs->branch_id.'&type_id=&startdate='.$startdate.'&enddate='.$enddate.'">'
						.$rs->name.
					'</a>';
			})
			->editColumn('outbound', function ($rs) {
				return "<span class='label label-default text-lg'>".$rs->outbound."</span>";
			})
			->editColumn('inbound', function ($rs) {
				return "<span class='label label-primary text-lg'>".$rs->inbound."</span>";
			})
			->editColumn('failed', function ($rs) {
				return "<span class='label label-warning text-lg'>".$rs->failed."</span>";
			})
			->editColumn('lost', function ($rs) {
				return "<span class='label label-danger text-lg'>".$rs->lost."</span>";
			})
			->editColumn('accuracy', function ($rs) {
				return number_format($rs->accuracy, '2', '.', '');
			})
			->addColumn('action', function ($rs) use ($startdate, $enddate) {
                $action = 
                    '<a href="'.url("parcelList").'?rider_id='.$rs->id.'&branch_id='.$rs->branch_id.'&type_id=&startdate='.$startdate.'&enddate='.$enddate.'"  class="btn btn-outline btn-sm btn-warning parcel-edit" title="Lihat Data"><i class="fa fa-search"></i></a>';
                
				return $action;
			})
			->rawColumns(['name','outbound','inbound','failed','lost','action'])
			->toJson();
    }
	
	public function getBranchDailyData(Request $request)
    {
		if(empty($request->input('date_'))){
            $date = date('d/m/Y', strtotime("-1 days"));
        }else{
			$date = $request->input('date_');
		}
		
		$rs = DB::select(DB::raw(
				"select
					id,
					name,
					outbound,
					inbound,
					failed,
					lost,
					case when outbound = 0 then 0 else (inbound::float / outbound::float * 100) end as accuracy
				from
				(
					select 
						b.id,
						b.name,
						sum(case when p.outbound is null then 0 else p.outbound end) as outbound,
						sum(case when p.inbound is null then 0 else p.inbound end) as inbound,
						sum(case when p.failed is null then 0 else p.failed end) as failed,
						sum(case when p.lost is null then 0 else p.lost end) as lost
					from
						branches b
					left join parcels p on p.branch_id = b.id and p.dateout between '".$date."'::date and '".$date."'::date and p.disable = 'ACTIVE'
					where b.disable = 'ACTIVE'
					group by b.name, b.id
					order by b.id
				) as stat"
			));

    	return DataTables::of($rs)
			->editColumn('name', function ($rs) use ($date){
				return 
					'<a href="'.url("parcelList").'?rider_id=&branch_id='.$rs->id.'&type_id=&startdate='.$date.'&enddate='.$date.'">'
						.$rs->name.
					'</a>';
			})
			->editColumn('outbound', function ($rs) {
				return "<span class='label label-default text-lg'>".$rs->outbound."</span>";
			})
			->editColumn('inbound', function ($rs) {
				return "<span class='label label-primary text-lg'>".$rs->inbound."</span>";
			})
			->editColumn('failed', function ($rs) {
				return "<span class='label label-warning text-lg'>".$rs->failed."</span>";
			})
			->editColumn('lost', function ($rs) {
				return "<span class='label label-danger text-lg'>".$rs->lost."</span>";
			})
			->editColumn('accuracy', function ($rs) {
				return number_format($rs->accuracy, '2', '.', '');
			})
			->addColumn('action', function ($rs) use ($date){
                $action = 
                    '<a href="'.url("parcelList").'?rider_id=&branch_id='.$rs->id.'&type_id=&startdate='.$date.'&enddate='.$date.'" class="btn btn-outline btn-sm btn-warning" title="Lihat Data">
						<i class="fa fa-search"></i>
					</a>';
                
				return $action;
			})
			->rawColumns(['name','outbound','inbound','failed','lost','action'])
			->toJson();
    }
	
	public function getBranchMonthlyData(Request $request)
    {
		if(empty($request->input('month_'))){
            $month = date('m');
        }else{
			$month = $request->input('month_');
		}
		
		if(empty($request->input('year_'))){
            $year = date('Y');
        }else{
			$year = $request->input('year_');
		}
		
		$prevYear = $year;
		$prevMonth = str_pad($month-1, 2, '0', STR_PAD_LEFT);
		
		if($prevMonth == 0){
			$prevMonth = 12;
			$prevYear = $year-1;
		}
		
		$rs = DB::select(DB::raw(
				"select
					id,
					name,
					outbound,
					inbound,
					failed,
					lost,
					case when outbound = 0 then 0 else (inbound::float / outbound::float * 100) end as accuracy
				from
				(
					select 
						b.id,
						b.name,
						sum(case when p.outbound is null then 0 else p.outbound end) as outbound,
						sum(case when p.inbound is null then 0 else p.inbound end) as inbound,
						sum(case when p.failed is null then 0 else p.failed end) as failed,
						sum(case when p.lost is null then 0 else p.lost end) as lost
					from
						branches b
					left join parcels p on p.branch_id = b.id and p.dateout between '".$prevYear."-".$prevMonth."-28'::date and '".$year."-".$month."-27'::date and p.disable = 'ACTIVE'
					where b.disable = 'ACTIVE'
					group by b.name, b.id
					order by b.id
				) as stat"
			));
		
		$startdate = "28/".$prevMonth."/".$prevYear;
		$enddate = "27/".$month."/".$year;

    	return DataTables::of($rs)
			->editColumn('name', function ($rs) use ($startdate, $enddate){
				return 
					'<a href="'.url("parcelList").'?rider_id=&branch_id='.$rs->id.'&type_id=&startdate='.$startdate.'&enddate='.$enddate.'">'
						.$rs->name.
					'</a>';
			})
			->editColumn('outbound', function ($rs) {
				return "<span class='label label-default text-lg'>".$rs->outbound."</span>";
			})
			->editColumn('inbound', function ($rs) {
				return "<span class='label label-primary text-lg'>".$rs->inbound."</span>";
			})
			->editColumn('failed', function ($rs) {
				return "<span class='label label-warning text-lg'>".$rs->failed."</span>";
			})
			->editColumn('lost', function ($rs) {
				return "<span class='label label-danger text-lg'>".$rs->lost."</span>";
			})
			->editColumn('accuracy', function ($rs) {
				return number_format($rs->accuracy, '2', '.', '');
			})
			->addColumn('action', function ($rs) use ($startdate, $enddate) {
                $action = 
                    '<a href="'.url("parcelList").'?rider_id=&branch_id='.$rs->id.'&type_id=&startdate='.$startdate.'&enddate='.$enddate.'"  class="btn btn-outline btn-sm btn-warning parcel-edit" title="Lihat Data"><i class="fa fa-search"></i></a>';
                
				return $action;
			})
			->rawColumns(['name','outbound','inbound','failed','lost','action'])
			->toJson();
    }
}