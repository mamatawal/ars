@extends('layouts/main/main')

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Formula {{ $branch }}</h2>
        <ol class="breadcrumb">
            <li>
                Pengurusan Premis
            </li>
            <li>
                Formula
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content">
	@if($id != 1)
	<div class="row">
        <div class="col-lg-6">
			<div class="">
				<button class="btn btn-primary btn-outline" id="sync">
					<i class="fa fa-refresh"></i>
				</button>
				<label for="refresh">
					Penyeragaman Data
				</label>
			</div>
        </div>
        <div class="col-lg-6">
			<div class="checkbox checkbox-primary checkbox-circle pull-right">
				<input id="checkbox8" type="checkbox" {{ $is_sa == 1 ? "checked" : ""}} >
				<label for="checkbox8">
					Follow Shah Alam Calculation
				</label>
			</div>
        </div>
    </div>
	@endif
	<div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Full Time</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover table-list-fdata">
                            <thead>
                                <tr>
                                    <th width="6%">#</th>
                                    <th>Min</th>
								  	<th>Max</th>
                                    <th>Kadar / Parcel</th>
                                    <th>Elaun</th>
                                    <th width="6%">Tindakan</th>
                                </tr>
                            </thead>
                            <tbody>
								<?php
									for($i = 1; $i < 4; $i++){ 
								?>
									<tr>
										<td>
											<?php echo $i ?>
										</td>
										<td>
											<?php echo empty(${"ft".$i}) ? '' : ${"ft".$i}->min ?>
										</td>
										<td>
											<?php echo empty(${"ft".$i}) ? '' : ${"ft".$i}->max ?>
										</td>
										<td>
											<?php echo empty(${"ft".$i}) ? '' : number_format(${"ft".$i}->price, 2, '.', '')  ?>
										</td>
										<td>
											<?php echo empty(${"ft".$i}) ? '' : number_format(${"ft".$i}->allowance, 2, '.', '') ?>
										</td>
										<td>
											<a class="btn btn-outline btn-sm btn-warning formula-edit" data-level="{{$i}}" data-type="1" data-branch="{{$id}}" data-toggle="tooltip" data-placement="top" title="Kemaskini Formula"><i class="fa fa-edit"></i></a>
										</td>
									</tr>
								<?php } ?>
                            </tbody>
                            <tfoot>
                                <tr>
									<th width="6%">#</th>
                                    <th>Min</th>
								  	<th>Max</th>
                                    <th>Kadar / Parcel</th>
                                    <th>Elaun</th>
                                    <th>Tindakan</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
	
	<div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Part Time</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover table-list-fdata">
                            <thead>
                                <tr>
                                    <th width="6%">#</th>
                                    <th>Min</th>
								  	<th>Max</th>
                                    <th>Kadar / Parcel</th>
                                    <th>Elaun</th>
                                    <th width="6%">Tindakan</th>
                                </tr>
                            </thead>
                            <tbody>
								<?php
									for($i = 1; $i < 4; $i++){ 
								?>
									<tr>
										<td>
											<?php echo $i ?>
										</td>
										<td>
											<?php echo empty(${"pt".$i}) ? '' : ${"pt".$i}->min ?>
										</td>
										<td>
											<?php echo empty(${"pt".$i}) ? '' : ${"pt".$i}->max ?>
										</td>
										<td>
											<?php echo empty(${"pt".$i}) ? '' : number_format(${"pt".$i}->price, 2, '.', '')  ?>
										</td>
										<td>
											<?php echo empty(${"pt".$i}) ? '' : number_format(${"pt".$i}->allowance, 2, '.', '') ?>
										</td>
										<td>
											<a class="btn btn-outline btn-sm btn-warning formula-edit" data-level="{{$i}}" data-type="2" data-branch="{{$id}}" data-toggle="tooltip" data-placement="top" title="Kemaskini Formula"><i class="fa fa-edit"></i></a>
										</td>
									</tr>
								<?php } ?>
                            </tbody>
                            <tfoot>
                                <tr>
									<th width="6%">#</th>
                                    <th>Min</th>
								  	<th>Max</th>
                                    <th>Kadar / Parcel</th>
                                    <th>Elaun</th>
                                    <th>Tindakan</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
	@if($id != 1)
	<div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Team Leader</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover table-list-fdata">
                            <thead>
                                <tr>
                                    <th width="6%">#</th>
                                    <th>Min</th>
								  	<th>Max</th>
                                    <th>Kadar / Parcel</th>
                                    <th>Elaun</th>
                                    <th width="6%">Tindakan</th>
                                </tr>
                            </thead>
                            <tbody>
								<?php
									for($i = 1; $i < 4; $i++){ 
								?>
									<tr>
										<td>
											<?php echo $i ?>
										</td>
										<td>
											<?php echo empty(${"tl".$i}) ? '' : ${"tl".$i}->min ?>
										</td>
										<td>
											<?php echo empty(${"tl".$i}) ? '' : ${"tl".$i}->max ?>
										</td>
										<td>
											<?php echo empty(${"tl".$i}) ? '' : number_format(${"tl".$i}->price, 2, '.', '') ?>
										</td>
										<td>
											<?php echo empty(${"tl".$i}) ? '' : number_format(${"tl".$i}->allowance, 2, '.', '') ?>
										</td>
										<td>
											<a class="btn btn-outline btn-sm btn-warning formula-edit" data-level="{{$i}}" data-type="3" data-branch="{{$id}}" data-toggle="tooltip" data-placement="top" title="Kemaskini Formula"><i class="fa fa-edit"></i></a>
										</td>
									</tr>
								<?php } ?>
                            </tbody>
                            <tfoot>
                                <tr>
									<th width="6%">#</th>
                                    <th>Min</th>
								  	<th>Max</th>
                                    <th>Kadar / Parcel</th>
                                    <th>Elaun</th>
                                    <th>Tindakan</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
	@endif
</div>

<div class="modal inmodal" id="modal-edit-formula">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Tutup</span></button>
                <h4 class="modal-title">Kemaskini Formula</h4>
            </div>
            <form id="form-edit-formula">
				{{ csrf_field() }}
                <div class="modal-body">
                    <div class="message-form"></div>
					<input type="hidden" name="level" id="level">
					<input type="hidden" name="type" id="type">
					<input type="hidden" name="branch" id="branch">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><strong>Dari</strong> <i class="fa fa-asterisk text-danger"></i></label>
                                <div class="input-group m-b">
                                    <span class="input-group-addon">
                                        <i class="fa fa-sort-amount-asc"></i>
                                    </span>
                                    <input type="number" name="min" class="form-control" placeholder="Min" data-required="true" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><strong>Hingga</strong></label>
                                <div class="input-group m-b">
                                    <span class="input-group-addon">
                                        <i class="fa fa-sort-amount-desc"></i>
                                    </span>
                                    <input type="number" name="max" class="form-control" placeholder="Max" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><strong>Kadar / Parcel</strong> <i class="fa fa-asterisk text-danger"></i></label>
                                <div class="input-group m-b">
                                    <span class="input-group-addon">
                                        <i class="fa fa-money"></i>
                                    </span>
                                    <input type="text" name="price" class="form-control" placeholder="Kadar / Parcel" data-required="true" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><strong>Elaun</strong></label>
                                <div class="input-group m-b">
                                    <span class="input-group-addon">
                                        <i class="fa fa-dollar"></i>
                                    </span>
                                    <input type="text" name="allowance" class="form-control" placeholder="Elaun" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a id="update-formula" class="btn btn-primary">Simpan</a>
                    <a class="btn btn-default" data-dismiss="modal">Tutup</a>
                </div>
            </form>    
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="{{ URL::asset('assets/parsley/parsley.min.js') }}"></script>
<script src="{{ URL::asset('assets/parsley/parsley.extend.js') }}"></script>
<script src="{{ URL::asset('inspinia/js/plugins/sweetalert/sweetalert.min.js') }}"></script>
<script>  
$(document).on('click','.formula-edit',function() {
	var level = $(this).data('level');
	var type = $(this).data('type');
	var branch = $(this).data('branch');
	var is_sa;
    $('#modal-edit-formula').modal('show');
    $('#form-edit-formula')[0].reset();
	$('.message-form').html('');
	
	$('#level').val(level);
	$('#type').val(type);
	$('#branch').val(branch);
	
	if($('#checkbox8').is(':checked')){
		is_sa = 1;
	}else{
		is_sa = 2;
	}
	
	$.ajax({
		url: '{{ url("ajaxViewFormula") }}' + '/' + level + '/' + type + '/' + branch,
		type: "GET",
		dataType: "JSON",
		success: function(data){
			var price = !data.price ? '0.00' : parseFloat(data.price).toFixed(2);
			var allowance = !data.allowance ? '0.00' : parseFloat(data.allowance).toFixed(2);
			$('#level').val(data.level);
			$('#type').val(data.type_id);
			$('#branch').val(data.branch_id);
			$('#is_sa').val(is_sa);
			$('[name="min"]').val(data.min);
			$('[name="max"]').val(data.max);
			$('[name="price"]').val(price);
			$('[name="allowance"]').val(allowance);
		}
	});
});
	
$('#update-formula').click(function(){
	var level = $('#level').val();
	var type = $('#type').val();
	var branch = $('#branch').val();

	if($('#form-edit-formula').parsley().validate()){
		$.ajax({
			url: '{{ url("ajaxUpdateFormula") }}' + '/' + level + '/' + type + '/' + branch,
			type: "POST",
			data: $('#form-edit-formula').serialize(),
			dataType: "JSON",
		 	success: function(data){
				if(data.status == 'success'){
					$('.message-form').html(data.success_form);
						setTimeout(function(){
							$('.message-form').remove();
							$('#modal-edit-formula').modal('hide');
							window.location.reload();
						}, 1000);
				}else{
					$('.message-form').html(data.error_form);
				}
			},
			error: function(jqXHR, textStatus, errorThrown){
				alert('Error get data from ajax');
			}
		});
	}
});
	
$('#checkbox8').click(function(){
	if($('#checkbox8').is(':checked')){
		$.ajax({
			type: 'GET',
			url : '{{ url("updateFormulaBased") }}',
			data : {
				is_sa : '1',
				branch : {{ $id }}
			},
			success:function(data){
				swal("Notifikasi", 'Data disimpan', "success");
			}
		});
	}else{
		$.ajax({
			type: 'GET',
			url : '{{ url("updateFormulaBased") }}',
			data : {
				is_sa : '2',
				branch : {{ $id }}
			},
			success:function(data){
				swal("Notifikasi", 'Data disimpan', "success");
			}
		});
	}
});
	
$('#sync').click(function(){
	$.ajax({
		type: 'GET',
		url : '{{ url("syncData") }}',
		data : {
			branch : {{ $id }}
		},
		success:function(data){
			swal("Notifikasi", 'Data harga parcel bulan terkini berjaya diseragamkan', "success");
		}
	});
});
</script>
@endsection