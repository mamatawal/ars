<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<link rel="manifest" href="manifest.json">

		<meta name="mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-capable" content="black">
		<meta name="application-name" content="ARS">
		<meta name="apple-mobile-web-app-title" content="ARS">
		<meta name="msapplication-starturl" content="/index.php">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="ARS Rider Management System">
		<meta name="theme-color" content="#058AF3" />

		<link rel="icon" href="/contact/images/logo152.png">
		<link rel="apple-touch-icon" href="/contact/images/logo152.png">

        <title>ARS Rider Management System</title>

        <link rel="shortcut icon" href="{{ URL::asset('contact/images/logo.png') }}" type="image/png" />
        <link rel="icon" href="{{ URL::asset('contact/images/logo.png') }}" type="image/png" />

        <link href="{{ URL::asset('inspinia/css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ URL::asset('inspinia/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
        <link href="{{ URL::asset('inspinia/css/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet">
        <link href="{{ URL::asset('inspinia/css/animate.css') }}" rel="stylesheet">
        <link href="{{ URL::asset('inspinia/css/style.css') }}" rel="stylesheet">
        <link href="{{ URL::asset('contact/css/style2.css') }}" rel="stylesheet">
    </head>
    <body class="gray-bg" style="background:url({{ URL::asset('contact/images/system.jpg') }}) fixed no-repeat;">
        <div class="loginColumns animated fadeInDown">
        @yield('content')
        <hr/>
            <div class="row">
                <div class="col-md-10 text-success">
                    Hakcipta Terpelihara <?php echo date('Y'); ?> © ARS Parcel Express Sdn Bhd
                </div>
                <div class="col-md-2 text-right text-white">
                   ARS
                </div>
            </div>
        </div>
    </body>
    @include('layouts.main.footer')
</html>