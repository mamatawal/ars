<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use Auth;
use Validator;
use App\Bank;
use App\Branch;
use App\Type;
use App\Rider;

class SettingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
	
	public function riderList(){
		$bank = Bank::orderBy('id')->pluck('name','id');
		$branch = Branch::orderBy('id')->pluck('name','id');
        
        return view('site.rider', compact('bank','branch'));
    }
	
	public function getRiderData(Request $request)
    {
		if(!empty($request->all())){
            $input = $request->all();
        }
		
		$rs = Rider::where(function($q) use ($input){
            if(!empty($input['bank_id'])){
                $q->where('bank_id', $input['bank_id']);
            }
			if(!empty($input['branch_id'])){
                $q->where('branch_id', $input['branch_id']);
            }
        });

    	return DataTables::of($rs)
			->addColumn('detail', function ($rs) {
				$email = empty($rs->email) ? 'Tiada' : $rs->email;
				$phone = empty($rs->phone) ? 'Tiada' : $rs->phone;
				
				return '<i class="fa fa-user"></i>&nbsp;&nbsp;'.$rs->name.'<br />'.
						'<i class="fa fa-id-card"></i>&nbsp;'.$rs->icno.'<br />'.
						'<i class="fa fa-envelope"></i>&nbsp;'.$email.'<br />'.
						'<i class="fa fa-phone"></i>&nbsp;&nbsp;'.$phone;
			})
			->filterColumn('detail', function ($query, $keyword) {
                $query->whereRaw("lower(name) like ?", ["%$keyword%"])
					->orWhereRaw("upper(name) like ?", ["%$keyword%"])
					->orWhereRaw("icno like ?", ["%$keyword%"])
					->orWhereRaw("email like ?", ["%$keyword%"])
					->orWhereRaw("phone like ?", ["%$keyword%"]);
            })
			->addColumn('emergency', function ($rs) {
				$e_name = empty($rs->emergencyname) ? '-' : $rs->emergencyname;
				$e_phone = empty($rs->emergencyphone) ? '-' : $rs->emergencyphone;
				$e_relation = empty($rs->emergencyrelation) ? '-' : $rs->emergencyrelation;
				
				return '<i class="fa fa-user"></i>&nbsp;&nbsp;'.$e_name.'<br />'.
						'<i class="fa fa-phone"></i>&nbsp;&nbsp;'.$e_phone.'<br />'.
						'<i class="fa fa-users"></i>&nbsp;'.$e_relation;
			})
			->filterColumn('emergency', function ($query, $keyword) {
                $query->whereRaw("lower(emergencyname) like ?", ["%$keyword%"])
					->orWhereRaw("upper(emergencyname) like ?", ["%$keyword%"])
					->orWhereRaw("lower(emergencyrelation) like ?", ["%$keyword%"])
					->orWhereRaw("upper(emergencyrelation) like ?", ["%$keyword%"])
					->orWhereRaw("emergencyphone like ?", ["%$keyword%"]);
            })
			->addColumn('bankaccount', function ($rs) {
                return '<i class="fa fa-font"></i>&nbsp;&nbsp;'.$rs->bank->code.'<br />'.
						'<i class="fa fa-university"></i> '.$rs->bank->name.'<br />'.
						'<i class="fa fa-credit-card"></i> '.$rs->accno;
			})
			->filterColumn('bankaccount', function ($query, $keyword) {
				$query->whereHas('bank', function ($q) use ($keyword) {
                    $q->whereRaw("lower(name) like ?", ["%$keyword%"])
						->orWhereRaw("upper(name) like ?", ["%$keyword%"])
						->orWhereRaw("lower(code) like ?", ["%$keyword%"])
						->orWhereRaw("upper(code) like ?", ["%$keyword%"])
						->orWhereRaw("accno like ?", ["%$keyword%"]);
                });
            })
			->addColumn('branch', function ($rs) {
				return $rs->branch->name;
			})
			->addColumn('action', function ($rs) {
                $action = 
                    '<a class="btn btn-outline btn-sm btn-warning rider-edit" data-id="'.$rs->id.'" data-toggle="tooltip" data-placement="top" title="Kemaskini Rider"><i class="fa fa-edit"></i></a>';
                
                if(Auth::user()->admin()){
                    $action .= '<a class="btn btn-outline btn-sm btn-danger rider-delete" data-id="'.$rs->id.'" data-toggle="tooltip" data-placement="top" title="Hapus Rider"><i class="fa fa-trash"></i></a>';
                }
                
				return $action;
			})
			->rawColumns(['detail','emergency','bankaccount','action'])
			->toJson();
    }
	
    public function ajaxViewRider($id){
        $rs = Rider::findOrFail($id);
		$rs->bank;
		$rs->branch;
		
        return $rs;
    }
	
    public function ajaxUpdateRider(Request $request, $id){
        $rs = Rider::findOrFail($id);
		$userid = Auth::user()->id;
		
		$validator = Validator::make($request->all(), [
			'e_icno'=> 'numeric|digits_between:12,14|unique_custom:users,icno,disable,ACTIVE',
            'e_email'=> 'sometimes|nullable|email',
        ],[
			'e_icno.numeric' => 'Nombor Kad Pengenalan mesti di dalam format nombor!',
			'e_icno.digits_between' => 'Nombor Kad Pengenalan mesti sekurang-kurangnya dua belas (12) aksara!',
			'e_icno.unique_custom' => 'Nombor Kad Pengenalan ini telah diambil!',
			'e_email.email' => 'Alamat emel mesti sah!',
		]);
		
		$rs->name = $request->input('e_name');
		$rs->icno = $request->input('e_icno');
        $rs->email = $request->input('e_email');
		$rs->phone = $request->input('e_phone');
		$rs->address = $request->input('e_address');
        $rs->bank_id = $request->input('e_bank');
        $rs->accno = $request->input('e_accno');
        $rs->branch_id = $request->input('e_branch');
		$rs->emergencyname = $request->input('e_emergencyname');
        $rs->emergencyphone = $request->input('e_emergencyphone');
        $rs->emergencyrelation = $request->input('e_emergencyrelation');
        $rs->updated_by = $userid;
		
		if(!$validator->fails()){
			$rs->save();
            $rs['status'] = 'success';
            $rs['success_form'] =   '<div class="row">
                                            <div class="col-md-12">
                                                <div class="alert alert-success alert-dismissable">
                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                    Rider telah berjaya dikemaskini.
                                                </div>
                                            </div>    
                                        </div>';
        } else {
            $rs['status'] = 'failure'; 
            $rs['error_form'] =   '<div class="row">
                                            <div class="col-md-12">
                                                <div class="alert alert-danger alert-dismissable">
                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.
													$validator->errors()->first().
                                                '</div>
                                            </div>    
                                        </div>';
        }
		
        return $rs;
    }
	
	public function ajaxDeleteRider($id){
        $rs = Rider::findOrFail($id);
		$userid = Auth::user()->id;
		
		$rs->disable = 'INACTIVE';
        $rs->updated_by = $userid;
		
		if($rs->save()){
            $rs['status'] = 'success';
            $rs['success_form'] =   '<div class="row">
                                            <div class="col-md-12">
                                                <div class="alert alert-success alert-dismissable">
                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                    Rider telah berjaya dihapus.
                                                </div>
                                            </div>    
                                        </div>';
        } else {
            $rs['status'] = 'failure'; 
            $rs['error_form'] =   '<div class="row">
                                            <div class="col-md-12">
                                                <div class="alert alert-danger alert-dismissable">
                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                    Rider tidak berjaya dihapus.
                                                </div>
                                            </div>    
                                        </div>';
        }
		
        return $rs;
    }
	
	public function ajaxRegisterRider(Request $request){
        $rs = new Rider();
		$userid = Auth::user()->id;
		
		$validator = Validator::make($request->all(), [
            'icno'=> 'numeric|digits_between:12,14|unique_custom:users,icno,disable,ACTIVE',
            'email'=> 'required|email',
        ],[
			'icno.numeric' => 'Nombor Kad Pengenalan mesti di dalam format nombor!',
			'icno.digits_between' => 'Nombor Kad Pengenalan mesti sekurang-kurangnya dua belas (12) aksara!',
			'icno.unique_custom' => 'Nombor Kad Pengenalan ini telah diambil!',
			'email.email' => 'Alamat emel mesti sah!',
		]);
		
		$rs->name = $request->input('name');
        $rs->icno = $request->input('icno');
        $rs->email = $request->input('email');
        $rs->phone = $request->input('phone');
        $rs->address = $request->input('address');
        $rs->bank_id = $request->input('bank');
        $rs->accno = $request->input('accno');
        $rs->branch_id = $request->input('branch');
        $rs->emergencyname = $request->input('emergencyname');
        $rs->emergencyphone = $request->input('emergencyphone');
        $rs->emergencyrelation = $request->input('emergencyrelation');
		$rs->disable = 'ACTIVE';
        $rs->created_by = $userid;
        $rs->updated_by = $userid;
		
		if(!$validator->fails()){
			$rs->save();
            $rs['status'] = 'success';
            $rs['success_form'] =   '<div class="row">
                                            <div class="col-md-12">
                                                <div class="alert alert-success alert-dismissable">
                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                    Rider telah berjaya didaftarkan.
                                                </div>
                                            </div>    
                                        </div>';
        } else {
            $rs['status'] = 'failure';
            $rs['error_form'] = $validator->errors();
        }
		
        return $rs;
    }
	
	public function mode(){
        return view('senggara.type');
    }
	
	public function getModeData()
    {
        $rs = Type::all();

    	return DataTables::of($rs)
			->addColumn('action', function ($rs) {
				return 
					'<a class="btn btn-outline btn-sm btn-warning mode-edit" data-id="'.$rs->id.'" data-toggle="tooltip" data-placement="top" title="Kemaskini Mode"><i class="fa fa-edit"></i></a>
					<a class="btn btn-outline btn-sm btn-danger mode-delete" data-id="'.$rs->id.'" data-toggle="tooltip" data-placement="top" title="Hapus Mode"><i class="fa fa-trash"></i></a>';
			})
			->rawColumns(['action'])
			->toJson();
    }
	
    public function ajaxViewMode($id){
        $rs = Type::findOrFail($id);
		
        return $rs;
    }
	
    public function ajaxUpdateMode(Request $request, $id){
        $rs = Type::findOrFail($id);
		$userid = Auth::user()->id;
		
		$validator = Validator::make($request->all(), [
            'e_name'=> 'unique_custom:types,name,disable,ACTIVE'
        ]);
		
		$rs->name = $request->input('e_name');
        $rs->updated_by = $userid;
		
		if(!$validator->fails()){
			$rs->save();
            $rs['status'] = 'success';
            $rs['success_form'] =   '<div class="row">
                                            <div class="col-md-12">
                                                <div class="alert alert-success alert-dismissable">
                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                    Mode telah berjaya dikemaskini.
                                                </div>
                                            </div>    
                                        </div>';
        } else {
            $rs['status'] = 'failure'; 
            $rs['error_form'] =   '<div class="row">
                                            <div class="col-md-12">
                                                <div class="alert alert-danger alert-dismissable">
                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                    Mode ini sudah wujud.
                                                </div>
                                            </div>    
                                        </div>';
        }
		
        return $rs;
    }
	
	public function ajaxDeleteMode($id){
        $rs = Type::findOrFail($id);
		$userid = Auth::user()->id;
		
		$rs->disable = 'INACTIVE';
        $rs->updated_by = $userid;
		
		if($rs->save()){
            $rs['status'] = 'success';
            $rs['success_form'] =   '<div class="row">
                                            <div class="col-md-12">
                                                <div class="alert alert-success alert-dismissable">
                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                    Mode telah berjaya dihapus.
                                                </div>
                                            </div>    
                                        </div>';
        } else {
            $rs['status'] = 'failure'; 
            $rs['error_form'] =   '<div class="row">
                                            <div class="col-md-12">
                                                <div class="alert alert-danger alert-dismissable">
                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                    Mode tidak berjaya dihapus.
                                                </div>
                                            </div>    
                                        </div>';
        }
		
        return $rs;
    }
	
	public function ajaxRegisterMode(Request $request){
        $rs = new Type();
		$userid = Auth::user()->id;
		
		$validator = Validator::make($request->all(), [
            'name'=> 'unique_custom:types,name,disable,ACTIVE'
        ],[
			'name.unique_custom' => 'Mode ini telah didaftar!'
		]);
		
		$rs->name = $request->input('name');
		$rs->disable = 'ACTIVE';
        $rs->created_by = $userid;
        $rs->updated_by = $userid;
		
		if(!$validator->fails()){
			$rs->save();
            $rs['status'] = 'success';
            $rs['success_form'] =   '<div class="row">
                                            <div class="col-md-12">
                                                <div class="alert alert-success alert-dismissable">
                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                    Mode telah berjaya didaftarkan.
                                                </div>
                                            </div>    
                                        </div>';
        } else {
            $rs['status'] = 'failure';
            $rs['error_form'] = $validator->errors();
        }
		
        return $rs;
    }
	
    public function branch(){
        return view('senggara.branch');
    }
	
	public function getBranchData()
    {
        $rs = Branch::all();

    	return DataTables::of($rs)
			->addColumn('formula', function ($rs) {
				return 
					'<center><a href="'.url("branch-formula/{$rs->id}").'" class="btn btn-outline btn-sm btn-success branch-formula" title="Formula"><i class="fa fa-money"></i></a></center>';
			})
			->addColumn('action', function ($rs) {
				return 
					'<a class="btn btn-outline btn-sm btn-warning branch-edit" data-id="'.$rs->id.'" data-toggle="tooltip" data-placement="top" title="Kemaskini Branch"><i class="fa fa-edit"></i></a>
					<a class="btn btn-outline btn-sm btn-danger branch-delete" data-id="'.$rs->id.'" data-toggle="tooltip" data-placement="top" title="Hapus Branch"><i class="fa fa-trash"></i></a>';
			})
			->rawColumns(['formula','action'])
			->toJson();
    }
	
    public function ajaxViewBranch($id){
        $rs = Branch::findOrFail($id);
		
        return $rs;
    }
	
    public function ajaxUpdateBranch(Request $request, $id){
        $rs = Branch::findOrFail($id);
		$userid = Auth::user()->id;
		
		$validator = Validator::make($request->all(), [
            'e_name'=> 'unique_custom:branches,name,disable,ACTIVE'
        ]);
		
		$rs->name = $request->input('e_name');
        $rs->updated_by = $userid;
		
		if(!$validator->fails()){
			$rs->save();
            $rs['status'] = 'success';
            $rs['success_form'] =   '<div class="row">
                                            <div class="col-md-12">
                                                <div class="alert alert-success alert-dismissable">
                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                    Branch telah berjaya dikemaskini.
                                                </div>
                                            </div>    
                                        </div>';
        } else {
            $rs['status'] = 'failure'; 
            $rs['error_form'] =   '<div class="row">
                                            <div class="col-md-12">
                                                <div class="alert alert-danger alert-dismissable">
                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                    Branch ini sudah wujud.
                                                </div>
                                            </div>    
                                        </div>';
        }
		
        return $rs;
    }
	
	public function ajaxDeleteBranch($id){
        $rs = Branch::findOrFail($id);
		$userid = Auth::user()->id;
		
		$rs->disable = 'INACTIVE';
        $rs->updated_by = $userid;
		
		if($rs->save()){
            $rs['status'] = 'success';
            $rs['success_form'] =   '<div class="row">
                                            <div class="col-md-12">
                                                <div class="alert alert-success alert-dismissable">
                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                    Branch telah berjaya dihapus.
                                                </div>
                                            </div>    
                                        </div>';
        } else {
            $rs['status'] = 'failure'; 
            $rs['error_form'] =   '<div class="row">
                                            <div class="col-md-12">
                                                <div class="alert alert-danger alert-dismissable">
                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                    Branch tidak berjaya dihapus.
                                                </div>
                                            </div>    
                                        </div>';
        }
		
        return $rs;
    }
	
	public function ajaxRegisterBranch(Request $request){
        $rs = new Branch();
		$userid = Auth::user()->id;
		
		$validator = Validator::make($request->all(), [
            'name'=> 'unique_custom:branches,name,disable,ACTIVE'
        ],[
			'name.unique_custom' => 'Branch ini telah didaftar!'
		]);
		
		$rs->name = $request->input('name');
		$rs->disable = 'ACTIVE';
        $rs->created_by = $userid;
        $rs->updated_by = $userid;
		
		if(!$validator->fails()){
			$rs->save();
            $rs['status'] = 'success';
            $rs['success_form'] =   '<div class="row">
                                            <div class="col-md-12">
                                                <div class="alert alert-success alert-dismissable">
                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                    Branch telah berjaya didaftarkan.
                                                </div>
                                            </div>    
                                        </div>';
        } else {
            $rs['status'] = 'failure';
            $rs['error_form'] = $validator->errors();
        }
		
        return $rs;
    }
	
	public function bank(){
        return view('senggara.bank');
    }
	
	public function getBankData()
    {
        $rs = Bank::all();

    	return DataTables::of($rs)
			->addColumn('action', function ($rs) {
				return 
					'<a class="btn btn-outline btn-sm btn-warning bank-edit" data-id="'.$rs->id.'" data-toggle="tooltip" data-placement="top" title="Kemaskini Bank"><i class="fa fa-edit"></i></a>
					<a class="btn btn-outline btn-sm btn-danger bank-delete" data-id="'.$rs->id.'" data-toggle="tooltip" data-placement="top" title="Hapus Bank"><i class="fa fa-trash"></i></a>';
			})
			->rawColumns(['action'])
			->toJson();
    }
	
    public function ajaxViewBank($id){
        $rs = Bank::findOrFail($id);
		
        return $rs;
    }
	
    public function ajaxUpdateBank(Request $request, $id){
        $rs = Bank::findOrFail($id);
		$userid = Auth::user()->id;
		
		$validator = Validator::make($request->all(), [
            'e_code'=> 'unique:banks,code,'.$id
        ]);
		
		$rs->code = $request->input('e_code');
		$rs->name = $request->input('e_name');
        $rs->updated_by = $userid;
		
		if(!$validator->fails()){
			$rs->save();
            $rs['status'] = 'success';
            $rs['success_form'] =   '<div class="row">
                                            <div class="col-md-12">
                                                <div class="alert alert-success alert-dismissable">
                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                    Bank telah berjaya dikemaskini.
                                                </div>
                                            </div>    
                                        </div>';
        } else {
            $rs['status'] = 'failure'; 
            $rs['error_form'] =   '<div class="row">
                                            <div class="col-md-12">
                                                <div class="alert alert-danger alert-dismissable">
                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                    Kod Bank ini sudah wujud.
                                                </div>
                                            </div>    
                                        </div>';
        }
		
        return $rs;
    }
	
	public function ajaxDeleteBank($id){
        $rs = Bank::findOrFail($id);
		$userid = Auth::user()->id;
		
		$rs->disable = 'INACTIVE';
        $rs->updated_by = $userid;
		
		if($rs->save()){
            $rs['status'] = 'success';
            $rs['success_form'] =   '<div class="row">
                                            <div class="col-md-12">
                                                <div class="alert alert-success alert-dismissable">
                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                    Bank telah berjaya dihapus.
                                                </div>
                                            </div>    
                                        </div>';
        } else {
            $rs['status'] = 'failure'; 
            $rs['error_form'] =   '<div class="row">
                                            <div class="col-md-12">
                                                <div class="alert alert-danger alert-dismissable">
                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                    Bank tidak berjaya dihapus.
                                                </div>
                                            </div>    
                                        </div>';
        }
		
        return $rs;
    }
	
	public function ajaxRegisterBank(Request $request){
        $rs = new Bank();
		$userid = Auth::user()->id;
		
		$validator = Validator::make($request->all(), [
            'code'=> 'unique_custom:banks,code,disable,ACTIVE'
        ],[
			'code.unique_custom' => 'Kod Bank ini telah didaftar!'
		]);
		
		$rs->code = $request->input('code');
		$rs->name = $request->input('name');
		$rs->disable = 'ACTIVE';
        $rs->created_by = $userid;
        $rs->updated_by = $userid;
		
		if(!$validator->fails()){
			$rs->save();
            $rs['status'] = 'success';
            $rs['success_form'] =   '<div class="row">
                                            <div class="col-md-12">
                                                <div class="alert alert-success alert-dismissable">
                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                    Bank telah berjaya didaftarkan.
                                                </div>
                                            </div>    
                                        </div>';
        } else {
            $rs['status'] = 'failure';
            $rs['error_form'] = $validator->errors();
        }
		
        return $rs;
    }
}
