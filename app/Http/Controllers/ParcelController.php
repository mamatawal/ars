<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use Carbon\Carbon;
use App\Parcel;
use App\Rate;
use App\Rider;
use App\Type;
use App\Branch;
use Auth;
use Validator;

class ParcelController extends Controller
{	
	public function parcelList(Request $request){
		$rider = Rider::orderBy('id')->pluck('name','id');
		$type = Type::orderBy('id')->pluck('name','id');
		$branch = Branch::orderBy('id')->pluck('name','id');
		$branchname = '';
		$ridername = '';
		$modename = '';
		
		if(!empty($request->all())){
            $input = $request->all();
        }else{
            $input = [
                'branch_id' => '',
                'rider_id' => '',
                'type_id' => '',
                'startdate' => date('d/m/Y'),
                'enddate' => date('d/m/Y'),
            ];
        }
		
		if($input["branch_id"]){
            $branchname = Branch::findOrFail($input["branch_id"])->name;
        }
        if($input["rider_id"]){
            $ridername = Rider::findOrFail($input["rider_id"])->name;
        }
        if($input["type_id"]){
            $modename = Type::findOrFail($input["mode_id"])->name;
        }
		
        return view('site.parcel', compact('rider','type','branch', 'ridername','branchname','modename'),
				   [
					   'startdate' => $input['startdate'],
					   'enddate' => $input['enddate']
				   ]);
    }
	
	public function getParcelData(Request $request)
    {
		if(!empty($request->all())){
            $input = $request->all();
        }
		
		$rs = Parcel::with('rider')->with('type')->with('branch')
			->where(function($q) use ($input){
			if(!empty($input['startdate']) && !empty($input['enddate'])){
                $q->whereRaw("dateout between '".$input['startdate']."'::date and '".$input['enddate']."'::date");
            }
            if(!empty($input['rider_id'])){
                $q->where('rider_id', $input['rider_id']);
            }
			if(!empty($input['type_id'])){
                $q->where('type_id', $input['type_id']);
            }
			if(!empty($input['branch_id'])){
                $q->where('branch_id', $input['branch_id']);
            }
        })->orderBy('dateout', 'desc');

    	return DataTables::of($rs)
			->addColumn('date', function ($rs) {
				return $rs->dateout ? with(new Carbon($rs->dateout))->format('d/m/Y') : '';
			})
			->filterColumn('date', function ($query, $keyword) {
                $query->whereRaw("to_char(dateout,'dd/MM/yyyy') like ?", ["%$keyword%"]);
            })
			->addColumn('rider', function ($rs) {
				return $rs->rider->name;
			})
			->editColumn('type', function ($rs) {
				$color = $rs->type_id == '1' ? 'label-success' : 'label-info';
				return "<span class='label ".$color." text-lg'>".$rs->type->name."</span>";
			})
			->editColumn('outbound', function ($rs) {
				return "<span class='label label-default text-lg'>".$rs->outbound."</span>";
			})
			->editColumn('inbound', function ($rs) {
				return "<span class='label label-primary text-lg'>".$rs->inbound."</span>";
			})
			->editColumn('failed', function ($rs) {
				return "<span class='label label-warning text-lg'>".$rs->failed."</span>";
			})
			->editColumn('lost', function ($rs) {
				return "<span class='label label-danger text-lg'>".$rs->lost."</span>";
			})
			->addColumn('branch', function ($rs) {
				return $rs->branch->name;
			})
			->addColumn('action', function ($rs) {
				$action = '';
				if(!in_array(Auth::user()->id, ['3','4'])){
					$action = 
						'<a class="btn btn-outline btn-sm btn-warning parcel-edit" data-id="'.$rs->id.'" data-toggle="tooltip" data-placement="top" title="Kemaskini Parcel"><i class="fa fa-edit"></i></a>';

					if(Auth::user()->admin()){
						$action .= '<a class="btn btn-outline btn-sm btn-danger parcel-delete" data-id="'.$rs->id.'" data-toggle="tooltip" data-placement="top" title="Hapus Parcel"><i class="fa fa-trash"></i></a>';
					}
				}
                
				return $action;
			})
			->rawColumns(['type','outbound','inbound','failed','lost','action'])
			->toJson();
    }
	
    public function ajaxViewParcel($id){
        $rs = Parcel::findOrFail($id);
		$rs->rider;
		$rs->type;
		$rs->branch;
		$rs['tarikh'] = $rs->dateout ? with(new Carbon($rs->dateout))->format('d/m/Y') : '';
		
        return $rs;
    }
	
    public function ajaxUpdateParcel(Request $request, $id){
        $rs = Parcel::findOrFail($id);
		$userid = Auth::user()->id;
		
		$validator = Validator::make($request->all(), [
			'e_outbound'=> 'numeric',
			'e_inbound'=> 'numeric',
			'e_failed'=> 'numeric',
			'e_lost'=> 'numeric',
			'e_dateout' => 'unique:parcels,dateout,'.$id.',id,disable,ACTIVE,rider_id,'.$request->input('e_rider')
        ],[
			'e_outbound.numeric' => 'Data mesti di dalam format nombor!',
			'e_inbound.numeric' => 'Data mesti di dalam format nombor!',
			'e_failed.numeric' => 'Data mesti di dalam format nombor!',
			'e_lost.numeric' => 'Data mesti di dalam format nombor!',
			'e_dateout.unique' => 'Data Rider pada hari yang dipilih ini sudah didaftarkan!',
		]);
		
		$rs->dateout = date_create_from_format('d/m/Y', $request->input('e_dateout'))->format('Y-m-d');
		$rs->outbound = $request->input('e_outbound');
		$rs->inbound = $request->input('e_inbound');
        $rs->failed = $request->input('e_failed');
		$rs->lost = $request->input('e_lost');
		$rs->rider_id = $request->input('e_rider');
        $rs->type_id = $request->input('e_type');
        $rs->branch_id = $request->input('e_branch');
        $rs->updated_by = $userid;
		
		$rate = Rate::where('type_id', $request->input('e_type'))
			->where('branch_id', $request->input('e_branch'))
			->whereRaw('(('.$request->input('e_inbound').' between min and max and max notnull) or ('.$request->input('e_inbound').' >= min and max isnull))')
			->first();
		
		$level1 = Rate::where('type_id', $request->input('e_type'))
				->where('branch_id', $request->input('e_branch'))
				->where('level', '1')->first()->id;
		
		$rs->level1_id = $level1;
		
		if($rate->level == '3'){
			$level2 = Rate::where('type_id', $request->input('e_type'))
				->where('branch_id', $request->input('e_branch'))
				->where('level', '2')->first()->id;
			
			$rs->level2_id = $level2;
			$rs->level3_id = $rate->id;
		}else if($rate->level == '2'){
			$rs->level2_id = $rate->id;
		}
		
		if(!$validator->fails()){
			$rs->save();
			
            $rs['status'] = 'success';
            $rs['success_form'] =   '<div class="row">
                                            <div class="col-md-12">
                                                <div class="alert alert-success alert-dismissable">
                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                    Parcel telah berjaya dikemaskini.
                                                </div>
                                            </div>    
                                        </div>';
        } else {
            $rs['status'] = 'failure'; 
            $rs['error_form'] =   '<div class="row">
                                            <div class="col-md-12">
                                                <div class="alert alert-danger alert-dismissable">
                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.
													$validator->errors()->first().
                                                '</div>
                                            </div>    
                                        </div>';
        }
		
        return $rs;
    }
	
	public function ajaxDeleteParcel($id){
        $rs = Parcel::findOrFail($id);
		$userid = Auth::user()->id;
		
		$rs->disable = 'INACTIVE';
        $rs->updated_by = $userid;
		
		if($rs->save()){
            $rs['status'] = 'success';
            $rs['success_form'] =   '<div class="row">
                                            <div class="col-md-12">
                                                <div class="alert alert-success alert-dismissable">
                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                    Parcel telah berjaya dihapus.
                                                </div>
                                            </div>    
                                        </div>';
        } else {
            $rs['status'] = 'failure'; 
            $rs['error_form'] =   '<div class="row">
                                            <div class="col-md-12">
                                                <div class="alert alert-danger alert-dismissable">
                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                    Parcel tidak berjaya dihapus.
                                                </div>
                                            </div>    
                                        </div>';
        }
		
        return $rs;
    }
	
	public function ajaxRegisterParcel(Request $request){
        $rs = new Parcel();
		$userid = Auth::user()->id;
		
		$validator = Validator::make($request->all(), [
            'outbound'=> 'numeric',
			'inbound'=> 'numeric',
			'failed'=> 'numeric',
			'lost'=> 'numeric',
			'dateout' => 'unique:parcels,dateout,NULL,id,disable,ACTIVE,rider_id,'.$request->input('rider')
        ],[
			'outbound.numeric' => 'Data mesti di dalam format nombor!',
			'inbound.numeric' => 'Data mesti di dalam format nombor!',
			'failed.numeric' => 'Data mesti di dalam format nombor!',
			'lost.numeric' => 'Data mesti di dalam format nombor!',
			'dateout.unique' => 'Data Rider pada hari yang dipilih ini sudah didaftarkan!',
		]);
		
		$rs->dateout = date_create_from_format('d/m/Y', $request->input('dateout'))->format('Y-m-d');
		$rs->outbound = $request->input('outbound');
		$rs->inbound = $request->input('inbound');
        $rs->failed = $request->input('failed');
		$rs->lost = $request->input('lost');
		$rs->rider_id = $request->input('rider');
        $rs->type_id = $request->input('type');
        $rs->branch_id = $request->input('branch');
		$rs->disable = 'ACTIVE';
        $rs->created_by = $userid;
        $rs->updated_by = $userid;
		
		$rate = Rate::where('type_id', $request->input('type'))
			->where('branch_id', $request->input('branch'))
			->whereRaw('(('.$request->input('inbound').' between min and max and max notnull) or ('.$request->input('inbound').' >= min and max isnull))')
			->first();
		
		$level1 = Rate::where('type_id', $request->input('type'))
				->where('branch_id', $request->input('branch'))
				->where('level', '1')->first()->id;
		
		$rs->level1_id = $level1;
		
		if($rate->level == '3'){
			$level2 = Rate::where('type_id', $request->input('type'))
				->where('branch_id', $request->input('branch'))
				->where('level', '2')->first()->id;
			
			$rs->level2_id = $level2;
			$rs->level3_id = $rate->id;
		}else if($rate->level == '2'){
			$rs->level2_id = $rate->id;
		}
		
		if(!$validator->fails()){
			$rs->save();
            $rs['status'] = 'success';
            $rs['success_form'] =   '<div class="row">
                                            <div class="col-md-12">
                                                <div class="alert alert-success alert-dismissable">
                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                    Parcel telah berjaya didaftarkan.
                                                </div>
                                            </div>    
                                        </div>';
        } else {
            $rs['status'] = 'failure';
            $rs['error_form'] = $validator->errors();
        }
		
        return $rs;
    }
	
	public function populateRider(Request $request){
        if($request->id != ''){
            $rs = Rider::where('branch_id', $request->id)->get();
        }else{
            $rs = Rider::all();
        }
        return $rs;
    }
	
	public function populateBranch(Request $request){
		$rs = Rider::findOrFail($request->id)->branch_id;
		
        return $rs;
    }
}