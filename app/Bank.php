<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Scopes\ActiveScope;
use OwenIt\Auditing\Contracts\Auditable;

class Bank extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    
    protected $auditInclude = [
        'id','code','name','created_by','updated_by','disable',
    ];
    
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new ActiveScope);
    }
	
	public function creator()
    {
        return $this->belongsTo('App\User', 'created_by');
    }
	
	public function updater()
    {
        return $this->belongsTo('App\User', 'updated_by');
    }
}
