@extends('layouts/main/print')

@section('content')
<div class="wrapper wrapper-content p-xl">
	<div class="row">
		<div class="col-sm-12 text-center">
			<img alt="image" src="{{ URL::asset('contact/images/logo.png') }}" width="150" />
		</div>
		<div class="col-sm-12 text-center">
			<strong>ARS PARCEL EXPRESS (M) SDN BHD</strong><br />
			03-41444120
		</div>
	</div>
	
	<div class="table-responsive m-t">
		<table class="table invoice-table">
			<thead>
				<tr>
					<td colspan="4" class="text-center">
					</td>
				</tr>
				<tr>
					<td colspan="4" class="text-center">SALARY SLIP</td>
				</tr>
				<tr>
					<td class="font-bold" style="width:25%">
						Name :<br />
						IC :<br /><br />
					</td>
					<td style="width:35%">
						{{ $rider->name }}<br />
						{{ $rider->icno }}<br /><br />
					</td>
					<td class="font-bold">
						Month :<br />
						PPL :<br /><br />
					</td>
					<td>
						{{ $monthname }}<br />
						{{ $rider->branch->name }}<br /><br />
					</td>
				</tr>
			</thead>
			<tbody id="tbody">
				<tr>
					<th colspan="2">Earnings</th>
					<th colspan="2">Deductions</th>
				</tr>
				<tr>
					<td>Basic Salary</td>
					<td class="text-right">{{ number_format($rs[0]->basic, '2', '.', '') }}</td>
					<td style="text-align:left !important">Advance</td>
					<td class="text-right">{{ !$salary ? '0.00' : number_format($salary->advance, '2', '.', '') }}</td>
				</tr>
				<tr>
					<td>Parcel Allowance</td>
					<td class="text-right">{{ number_format($rs[0]->allowance, '2', '.', '') }}</td>
					<td style="text-align:left !important" style="text-align:left !important">SOCSO</td>
					<td class="text-right">{{ !$salary ? '0.00' : number_format($salary->socso, 2, '.', '') }}</td>
				</tr>
				<tr>
					<td>Supervision Allowance</td>
					<td class="text-right">{{ !$salary ? '0.00' : number_format($salary->supervision, 2, '.', '') }}</td>
					<td style="text-align:left !important">Other Deduction</td>
					<td class="text-right">{{ !$salary ? '0.00' : number_format($salary->otherdeduction, 2, '.', '') }}</td>
				</tr>
				<tr>
					<td>Other Addition</td>
					<td class="text-right">{{ !$salary ? '0.00' : number_format($salary->otheraddition, 2, '.', '') }}</td>
					<td colspan="2"></td>
				</tr>
				<tr>
					<td>Attendance Incentive</td>
					<td class="text-right">{{ !$salary ? '0.00' : number_format($salary->attendance, 2, '.', '') }}</td>
					<td colspan="2"></td>
				</tr>
				<tr>
					<td>Performance Incentive</td>
					<td class="text-right">{{ !$salary ? '0.00' : number_format($salary->performance, 2, '.', '') }}</td>
					<td colspan="2"></td>
				</tr>
			</tbody>
			<?php
				$totalearning = $rs[0]->basic + $rs[0]->allowance + (!$salary ? 0 : $salary->supervision) + (!$salary ? 0 : $salary->otheraddition) + (!$salary ? 0 : $salary->attendance) + (!$salary ? 0 : $salary->performance);
				
				$totaldeduction = (!$salary ? 0 : $salary->advance) + (!$salary ? 0 : $salary->socso) + (!$salary ? 0 : $salary->otherdeduction);
			
				$totalsalary = $totalearning - $totaldeduction;
			?>
			<tfoot>
				<th>Total Earnings</th>
				<td class="text-right">{{ number_format($totalearning, 2, '.', '') }}</td>
				<th>Total Deduction</th>
				<td class="text-right">{{ number_format($totaldeduction, 2, '.', '') }}</td>
			</tfoot>
		</table>
	</div>
	
	<table class="table invoice-total">
		<tbody>
			<tr>
				<td></td>
			</tr>
			<tr>
				<td class="font-bold">{{ number_format($totalsalary, 2, '.', '') }}</td>
			</tr>
		</tbody>
	</table>
	<div class="well m-t">
		<center>*THIS IS COMPUTER GENERATED, NO SIGNATURE NEEDED*</center>
	</div>
</div>
@endsection