$(document).ready(function() {
    $('.chosen-type, .chosen-e-type, .chosen-type-filter, .chosen-branch, .chosen-e-branch, .chosen-branch-filter, .chosen-rider, .chosen-e-rider, .chosen-rider-filter').chosen({
        width: "100%"
    });
    
    $('[data-toggle="tooltip"]').tooltip();
});

$(document).on('click','.parcel-new',function() {
    $('#modal-new-parcel').modal('show');
    $('#form-new-parcel')[0].reset();
	$('[name="rider"]').val(null).trigger('chosen:updated');
	$('[name="type"]').val(null).trigger('chosen:updated');
	$('[name="branch"]').val(null).trigger('chosen:updated');
	$('.message-form').html('');
});

$('#register-parcel').click(function(){
	if($('#form-new-parcel').parsley().validate()){
		$.ajax({
			url:"ajaxRegisterParcel",
			type: "POST",
			data: $('#form-new-parcel').serialize(),
			dataType: "JSON",
		 	success: function(data){
				if(data.status == 'success'){
					$('.message-form').html(data.success_form);
						setTimeout(function(){
							$('.message-form').remove();
							$('#modal-new-parcel').modal('hide');
							window.location.reload();
						}, 500);
				}else{
					var error = '';
					$.each(data.error_form, function(k,v){
						error += v[0]+'<br/>';
					});
					$('.message-form').html('<div class="row">'+
												'<div class="col-md-12">'+
													'<div class="alert alert-danger alert-dismissable">'+
														'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+
														error +
													'</div>'+
												'</div>'+    
											'</div>');
				}
			},
			error: function(jqXHR, textStatus, errorThrown){
				alert('Error get data from ajax');
			}
		});
	}
});

$('#update-parcel').click(function(){
	var id = $('[name="e_id"]').val();

	if($('#form-edit-parcel').parsley().validate()){
		$.ajax({
			url:"ajaxUpdateParcel&id=" + id,
			type: "POST",
			data: $('#form-edit-parcel').serialize(),
			dataType: "JSON",
		 	success: function(data){
				if(data.status == 'success'){
					$('.message-form').html(data.success_form);
						setTimeout(function(){
							$('.message-form').remove();
							$('#modal-edit-parcel').modal('hide');
							window.location.reload();
						}, 500);
				}else{
					$('.message-form').html(data.error_form);
				}
			},
			error: function(jqXHR, textStatus, errorThrown){
				alert('Error get data from ajax');
			}
		});
	}
});

$(document).on('click','.parcel-delete',function() {
	var id = $(this).data('id');
    $('#modal-delete-parcel').modal('show');
    $('#form-delete-parcel')[0].reset();
	$('.message-form').html('');
	
	$.ajax({
		url: "ajaxViewParcel&id=" + id,
		type: "GET",
		dataType: "JSON",
		success: function(data){
			$('[name="d_id"]').val(data.id);
			$('[name="d_dateout"]').val(data.tarikh);
			$('[name="d_rider"]').val(data.rider.name);
			$('[name="d_branch"]').val(data.branch.name);
			$('[name="d_type"]').val(data.type.name);
			$('[name="d_outbound"]').val(data.outbound);
			$('[name="d_inbound"]').val(data.inbound);
			$('[name="d_failed"]').val(data.failed);
			$('[name="d_lost"]').val(data.lost);
		},
		error: function(jqXHR, textStatus, errorThrown){
			alert('Error get data from ajax');
		}
	});
});

$('#delete-parcel').click(function(){
	var id = $('[name="d_id"]').val();
	
	$.ajax({
		url:"ajaxDeleteParcel&id=" + id,
		type: "POST",
		data: $('#form-delete-parcel').serialize(),
		dataType: "JSON",
		success: function(data){
			window.location.reload();
		},
		error: function(jqXHR, textStatus, errorThrown){
			alert('Error get data from ajax');
		}
	});
});