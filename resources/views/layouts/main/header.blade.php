    <nav class="navbar navbar-fixed-top white-bg" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-success " href="#"><i class="fa fa-bars"></i> </a>
        </div>
        <ul class="nav navbar-top-links navbar-right">
            <li>
            	<a href="{{url('userprofile')}}"><button class="label label-primary text-lg" >{{ Auth::user()->fullname }}</button></a>
        	</li>
			|
            <li>
				<a href="{{ route('logout') }}"
					onclick="event.preventDefault();
							 document.getElementById('logout-form').submit();">
					<i class="fa fa-sign-out"></i> Log Keluar
				</a>

				<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
					{{ csrf_field() }}
				</form>
            </li>
        </ul>
    </nav>