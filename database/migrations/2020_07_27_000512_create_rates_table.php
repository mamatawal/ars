<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rates', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('level');
			$table->integer('min');
			$table->integer('max')->nullable();
			$table->float('price');
			$table->float('allowance')->nullable();
			$table->integer('type_id');
			$table->integer('branch_id');
			$table->integer('created_by');
            $table->integer('updated_by');
            $table->string('disable');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rates');
    }
}
