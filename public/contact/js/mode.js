$(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip();
});

$(document).on('click','.mode-new',function() {
    $('#modal-new-mode').modal('show');
    $('#form-new-mode')[0].reset();
	$('.message-form').html('');
});

$('#register-mode').click(function(){
	if($('#form-new-mode').parsley().validate()){
		$.ajax({
			url:"ajaxRegisterMode",
			type: "POST",
			data: $('#form-new-mode').serialize(),
			dataType: "JSON",
		 	success: function(data){
				if(data.status == 'success'){
					$('.message-form').html(data.success_form);
						setTimeout(function(){
							$('.message-form').remove();
							$('#modal-new-mode').modal('hide');
							$(location).attr('href','mode');
						}, 1000);
				}else{
					var error = '';
					$.each(data.error_form, function(k,v){
						error += v[0]+'<br/>';
					});
					$('.message-form').html('<div class="row">'+
												'<div class="col-md-12">'+
													'<div class="alert alert-danger alert-dismissable">'+
														'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+
														error +
													'</div>'+
												'</div>'+    
											'</div>');
				}
			},
			error: function(jqXHR, textStatus, errorThrown){
				alert('Error get data from ajax');
			}
		});
	}
});

$(document).on('click','.mode-edit',function() {
	var id = $(this).data('id');
    $('#modal-edit-mode').modal('show');
    $('#form-edit-mode')[0].reset();
	$('.message-form').html('');
	
	$.ajax({
		url: "ajaxViewMode&id=" + id,
		type: "GET",
		dataType: "JSON",
		success: function(data){
			$('[name="e_id"]').val(data.id);
			$('[name="e_name"]').val(data.name);
		},
		error: function(jqXHR, textStatus, errorThrown){
			alert('Error get data from ajax');
		}
	});
});

$('#update-mode').click(function(){
	var id = $('[name="e_id"]').val();

	if($('#form-edit-mode').parsley().validate()){
		$.ajax({
			url:"ajaxUpdateMode&id=" + id,
			type: "POST",
			data: $('#form-edit-mode').serialize(),
			dataType: "JSON",
		 	success: function(data){
				if(data.status == 'success'){
					$('.message-form').html(data.success_form);
						setTimeout(function(){
							$('.message-form').remove();
							$('#modal-edit-mode').modal('hide');
							$(location).attr('href','mode');
						}, 1000);
				}else{
					$('.message-form').html(data.error_form);
				}
			},
			error: function(jqXHR, textStatus, errorThrown){
				alert('Error get data from ajax');
			}
		});
	}
});

$(document).on('click','.mode-delete',function() {
	var id = $(this).data('id');
    $('#modal-delete-mode').modal('show');
    $('#form-delete-mode')[0].reset();
	$('.message-form').html('');
	
	$.ajax({
		url: "ajaxViewMode&id=" + id,
		type: "GET",
		dataType: "JSON",
		success: function(data){
			$('[name="d_id"]').val(data.id);
			$('[name="d_name"]').val(data.name);
		},
		error: function(jqXHR, textStatus, errorThrown){
			alert('Error get data from ajax');
		}
	});
});

$('#delete-mode').click(function(){
	var id = $('[name="d_id"]').val();
	
	$.ajax({
		url:"ajaxDeleteMode&id=" + id,
		type: "POST",
		data: $('#form-delete-mode').serialize(),
		dataType: "JSON",
		success: function(data){
			$(location).attr('href','mode');
		},
		error: function(jqXHR, textStatus, errorThrown){
			alert('Error get data from ajax');
		}
	});
});