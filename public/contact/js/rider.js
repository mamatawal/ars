$(document).ready(function() {
    $('.chosen-bank, .chosen-e-bank, .chosen-branch, .chosen-e-branch').chosen({
        width: "100%"
    });
    
    $('[data-toggle="tooltip"]').tooltip();
});

$(document).on('click','.rider-new',function() {
    $('#modal-new-rider').modal('show');
    $('#form-new-rider')[0].reset();
	$('[name="bank"]').val(null).trigger('chosen:updated');
	$('[name="branch"]').val(null).trigger('chosen:updated');
	$('.message-form').html('');
});

$('#register-rider').click(function(){
	if($('#form-new-rider').parsley().validate()){
		$.ajax({
			url:"ajaxRegisterRider",
			type: "POST",
			data: $('#form-new-rider').serialize(),
			dataType: "JSON",
		 	success: function(data){
				if(data.status == 'success'){
					$('.message-form').html(data.success_form);
						setTimeout(function(){
							$('.message-form').remove();
							$('#modal-new-rider').modal('hide');
							$(location).attr('href','riderList');
						}, 1000);
				}else{
					var error = '';
					$.each(data.error_form, function(k,v){
						error += v[0]+'<br/>';
					});
					$('.message-form').html('<div class="row">'+
												'<div class="col-md-12">'+
													'<div class="alert alert-danger alert-dismissable">'+
														'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+
														error +
													'</div>'+
												'</div>'+    
											'</div>');
				}
			},
			error: function(jqXHR, textStatus, errorThrown){
				alert('Error get data from ajax');
			}
		});
	}
});

$('#update-rider').click(function(){
	var id = $('[name="e_id"]').val();

	if($('#form-edit-rider').parsley().validate()){
		$.ajax({
			url:"ajaxUpdateRider&id=" + id,
			type: "POST",
			data: $('#form-edit-rider').serialize(),
			dataType: "JSON",
		 	success: function(data){
				if(data.status == 'success'){
					$('.message-form').html(data.success_form);
						setTimeout(function(){
							$('.message-form').remove();
							$('#modal-edit-rider').modal('hide');
							$(location).attr('href','riderList');
						}, 1000);
				}else{
					$('.message-form').html(data.error_form);
				}
			},
			error: function(jqXHR, textStatus, errorThrown){
				alert('Error get data from ajax');
			}
		});
	}
});

$(document).on('click','.rider-delete',function() {
	var id = $(this).data('id');
    $('#modal-delete-rider').modal('show');
    $('#form-delete-rider')[0].reset();
	$('.message-form').html('');
	
	$.ajax({
		url: "ajaxViewRider&id=" + id,
		type: "GET",
		dataType: "JSON",
		success: function(data){
			$('[name="d_id"]').val(data.id);
			$('[name="d_name"]').val(data.name);
			$('[name="d_icno"]').val(data.icno);
			$('[name="d_email"]').val(data.email);
			$('[name="d_phone"]').val(data.phone);
			$('[name="d_address"]').val(data.address);
			$('[name="d_bank"]').val(data.bank.name);
			$('[name="d_accno"]').val(data.accno);
			$('[name="d_branch"]').val(data.branch.name);
		},
		error: function(jqXHR, textStatus, errorThrown){
			alert('Error get data from ajax');
		}
	});
});

$('#delete-rider').click(function(){
	var id = $('[name="d_id"]').val();
	
	$.ajax({
		url:"ajaxDeleteRider&id=" + id,
		type: "POST",
		data: $('#form-delete-rider').serialize(),
		dataType: "JSON",
		success: function(data){
			$(location).attr('href','riderList');
		},
		error: function(jqXHR, textStatus, errorThrown){
			alert('Error get data from ajax');
		}
	});
});