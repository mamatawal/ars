$(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip();
});

$(document).on('click','.branch-new',function() {
    $('#modal-new-branch').modal('show');
    $('#form-new-branch')[0].reset();
	$('.message-form').html('');
});

$('#register-branch').click(function(){
	if($('#form-new-branch').parsley().validate()){
		$.ajax({
			url:"ajaxRegisterBranch",
			type: "POST",
			data: $('#form-new-branch').serialize(),
			dataType: "JSON",
		 	success: function(data){
				if(data.status == 'success'){
					$('.message-form').html(data.success_form);
						setTimeout(function(){
							$('.message-form').remove();
							$('#modal-new-branch').modal('hide');
							$(location).attr('href','branch');
						}, 1000);
				}else{
					var error = '';
					$.each(data.error_form, function(k,v){
						error += v[0]+'<br/>';
					});
					$('.message-form').html('<div class="row">'+
												'<div class="col-md-12">'+
													'<div class="alert alert-danger alert-dismissable">'+
														'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+
														error +
													'</div>'+
												'</div>'+    
											'</div>');
				}
			},
			error: function(jqXHR, textStatus, errorThrown){
				alert('Error get data from ajax');
			}
		});
	}
});

$(document).on('click','.branch-edit',function() {
	var id = $(this).data('id');
    $('#modal-edit-branch').modal('show');
    $('#form-edit-branch')[0].reset();
	$('.message-form').html('');
	
	$.ajax({
		url: "ajaxViewBranch&id=" + id,
		type: "GET",
		dataType: "JSON",
		success: function(data){
			$('[name="e_id"]').val(data.id);
			$('[name="e_name"]').val(data.name);
		},
		error: function(jqXHR, textStatus, errorThrown){
			alert('Error get data from ajax');
		}
	});
});

$('#update-branch').click(function(){
	var id = $('[name="e_id"]').val();

	if($('#form-edit-branch').parsley().validate()){
		$.ajax({
			url:"ajaxUpdateBranch&id=" + id,
			type: "POST",
			data: $('#form-edit-branch').serialize(),
			dataType: "JSON",
		 	success: function(data){
				if(data.status == 'success'){
					$('.message-form').html(data.success_form);
						setTimeout(function(){
							$('.message-form').remove();
							$('#modal-edit-branch').modal('hide');
							$(location).attr('href','branch');
						}, 1000);
				}else{
					$('.message-form').html(data.error_form);
				}
			},
			error: function(jqXHR, textStatus, errorThrown){
				alert('Error get data from ajax');
			}
		});
	}
});

$(document).on('click','.branch-delete',function() {
	var id = $(this).data('id');
    $('#modal-delete-branch').modal('show');
    $('#form-delete-branch')[0].reset();
	$('.message-form').html('');
	
	$.ajax({
		url: "ajaxViewBranch&id=" + id,
		type: "GET",
		dataType: "JSON",
		success: function(data){
			$('[name="d_id"]').val(data.id);
			$('[name="d_name"]').val(data.name);
		},
		error: function(jqXHR, textStatus, errorThrown){
			alert('Error get data from ajax');
		}
	});
});

$('#delete-branch').click(function(){
	var id = $('[name="d_id"]').val();
	
	$.ajax({
		url:"ajaxDeleteBranch&id=" + id,
		type: "POST",
		data: $('#form-delete-branch').serialize(),
		dataType: "JSON",
		success: function(data){
			$(location).attr('href','branch');
		},
		error: function(jqXHR, textStatus, errorThrown){
			alert('Error get data from ajax');
		}
	});
});