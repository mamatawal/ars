@extends('layouts/main/main')

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Rider</h2>
        <ol class="breadcrumb">
            <li>
                Pengurusan Rider
            </li>
            <li>
                Rider
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content">
    <div class="row">
            <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Senarai Rider</h5>
                    <div class="ibox-tools">
						<a class="btn btn-outline btn-success rider-filter"><i class="fa fa-search"></i> Tapisan</a>
                        <a class="btn btn-outline btn-success rider-new">
                            Daftar Rider
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover table-list-rider">
                            <thead>
                                <tr>
                                    <th width="6%">#</th>
                                    <th width="30%">Nama Penuh<br />No. Kad Pengenalan<br />Email<br />No. Telefon</th>
                                    <th width="20%">Alamat</th>
                                    <th>Kod Bank<br />Nama Bank<br />No. Akaun</th>
                                    <th>Branch</th>
                                    <th>Kecemasan</th>
                                    <th width="10%">Tindakan</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>#</th>
                                    <th>Nama Penuh<br />No. Kad Pengenalan<br />Email<br />No. Telefon</th>
                                    <th>Alamat</th>
                                    <th>Kod Bank<br />Nama Bank<br />No. Akaun</th>
                                    <th>Branch</th>
                                    <th>Kecemasan</th>
                                    <th>Tindakan</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal inmodal" id="modal-new-rider">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Tutup</span></button>
                <h4 class="modal-title">Daftar Maklumat Rider Baru</h4>
            </div>
            <form id="form-new-rider">
				{{ csrf_field() }}
                <div class="modal-body">
                    <div class="message-form"></div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label><strong>Nama Penuh</strong> <i class="fa fa-asterisk text-danger"></i></label>
                                <div class="input-group m-b">
                                    <span class="input-group-addon">
                                        <i class="fa fa-user-circle-o"></i>
                                    </span>
                                    <input type="text" name="name" class="form-control" placeholder="Nama Penuh" data-required="true" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><strong>Nombor Kad Pengenalan</strong> <i class="fa fa-asterisk text-danger"></i></label>
                                <div class="input-group m-b">
                                    <span class="input-group-addon">
                                        <i class="fa fa-drivers-license-o"></i>
                                    </span>
                                    <input type="text" name="icno" class="form-control" placeholder="Nombor Kad Pengenalan" data-required="true" />
                                </div>
                            </div>
                        </div>
						<div class="col-md-6">
                            <div class="form-group">
                                <label><strong>Alamat Emel</strong></label>
                                <div class="input-group m-b">
                                    <span class="input-group-addon">
                                        <i class="fa fa-envelope-o"></i>
                                    </span>
                                    <input type="text" name="email" class="form-control" placeholder="Alamat Emel" />
                                </div>
                            </div>
                        </div>
                    </div>
					<div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><strong>Nombor Telefon</strong></label>
                                <div class="input-group m-b">
                                    <span class="input-group-addon">
                                        <i class="fa fa-phone"></i>
                                    </span>
                                    <input type="text" name="phone" class="form-control" placeholder="Nombor Telefon" />
                                </div>
                            </div>
                        </div>
                    </div>
					<div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label><strong>Alamat</strong></label>
                                <div class="input-group m-b">
                                    <span class="input-group-addon">
                                        <i class="fa fa-home"></i>
                                    </span>
									<textarea type="text" name="address" class="form-control" placeholder="Alamat" rows="3"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><strong>Bank</strong> <i class="fa fa-asterisk text-danger"></i></label>
                                <div class="input-group m-b">
                                    <span class="input-group-addon">
                                        <i class="fa fa-university"></i>
                                    </span>
									{!! Form::select('bank', $bank, old('bank_id'), ['class' => 'form-control m-b chosen-e-bank', 'data-required' => 'true', 'placeholder' => 'Pilih Bank']) !!}
                                </div>
                            </div>
                        </div>
						<div class="col-md-6">
                            <div class="form-group">
                                <label><strong>Nombor Akaun</strong> <i class="fa fa-asterisk text-danger"></i></label>
                                <div class="input-group m-b">
                                    <span class="input-group-addon">
                                        <i class="fa fa-credit-card"></i>
                                    </span>
                                    <input type="text" name="accno" class="form-control" placeholder="Nombor Akaun" data-required="true" />
                                </div>
                            </div>
                        </div>
                    </div>
					<div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><strong>Branch</strong> <i class="fa fa-asterisk text-danger"></i></label>
                                <div class="input-group m-b">
                                    <span class="input-group-addon">
                                        <i class="fa fa-building"></i>
                                    </span>
									{!! Form::select('branch', $branch, old('branch_id'), ['class' => 'form-control m-b chosen-e-branch', 'data-required' => 'true', 'placeholder' => 'Pilih Branch']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
					<fieldset class="scheduler-border">
						<legend class="scheduler-border">Talian Kecemasan</legend>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label><strong>Nama</strong></label>
									<div class="input-group m-b">
										<span class="input-group-addon">
											<i class="fa fa-user-circle-o"></i>
										</span>
										<input type="text" name="emergencyname" class="form-control" placeholder="Nama Penuh" />
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label><strong>Nombor Telefon</strong></label>
									<div class="input-group m-b">
										<span class="input-group-addon">
											<i class="fa fa-phone"></i>
										</span>
										<input type="text" name="emergencyphone" class="form-control" placeholder="Nombor Telefon" />
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label><strong>Hubungan</strong></label>
									<div class="input-group m-b">
										<span class="input-group-addon">
											<i class="fa fa-users"></i>
										</span>
										<input type="text" name="emergencyrelation" class="form-control" placeholder="Hubungan" />
									</div>
								</div>
							</div>
						</div>
					</fieldset>
                </div>
                <div class="modal-footer">
                    <a id="register-rider" class="btn btn-primary">Daftar</a>
                    <a class="btn btn-default" data-dismiss="modal">Tutup</a>
                </div>
            </form>    
        </div>
    </div>
</div>

<div class="modal inmodal" id="modal-edit-rider">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Tutup</span></button>
                <h4 class="modal-title">Kemaskini Maklumat Rider</h4>
            </div>
            <form id="form-edit-rider">
				{{ csrf_field() }}
                <div class="modal-body">
                    <div class="message-form"></div>
					<input type="hidden" name="e_id">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label><strong>Nama Penuh</strong> <i class="fa fa-asterisk text-danger"></i></label>
                                <div class="input-group m-b">
                                    <span class="input-group-addon">
                                        <i class="fa fa-user-circle-o"></i>
                                    </span>
                                    <input type="text" name="e_name" class="form-control" placeholder="Nama Penuh" data-required="true" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><strong>Nombor Kad Pengenalan</strong> <i class="fa fa-asterisk text-danger"></i></label>
                                <div class="input-group m-b">
                                    <span class="input-group-addon">
                                        <i class="fa fa-drivers-license-o"></i>
                                    </span>
                                    <input type="text" name="e_icno" class="form-control" placeholder="Nombor Kad Pengenalan" data-required="true" />
                                </div>
                            </div>
                        </div>
						<div class="col-md-6">
                            <div class="form-group">
                                <label><strong>Alamat Emel</strong></label>
                                <div class="input-group m-b">
                                    <span class="input-group-addon">
                                        <i class="fa fa-envelope-o"></i>
                                    </span>
                                    <input type="text" name="e_email" class="form-control" placeholder="Alamat Emel" />
                                </div>
                            </div>
                        </div>
                    </div>
					<div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><strong>Nombor Telefon</strong></label>
                                <div class="input-group m-b">
                                    <span class="input-group-addon">
                                        <i class="fa fa-phone"></i>
                                    </span>
                                    <input type="text" name="e_phone" class="form-control" placeholder="Nombor Telefon" />
                                </div>
                            </div>
                        </div>
                    </div>
					<div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label><strong>Alamat</strong></label>
                                <div class="input-group m-b">
                                    <span class="input-group-addon">
                                        <i class="fa fa-home"></i>
                                    </span>
									<textarea type="text" name="e_address" class="form-control" placeholder="Alamat" rows="3"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><strong>Bank</strong> <i class="fa fa-asterisk text-danger"></i></label>
                                <div class="input-group m-b">
                                    <span class="input-group-addon">
                                        <i class="fa fa-university"></i>
                                    </span>
									{!! Form::select('e_bank', $bank, old('bank_id'), ['class' => 'form-control m-b chosen-e-bank', 'data-required' => 'true', 'placeholder' => 'Pilih Bank']) !!}
                                </div>
                            </div>
                        </div>
						<div class="col-md-6">
                            <div class="form-group">
                                <label><strong>Nombor Akaun</strong> <i class="fa fa-asterisk text-danger"></i></label>
                                <div class="input-group m-b">
                                    <span class="input-group-addon">
                                        <i class="fa fa-credit-card"></i>
                                    </span>
                                    <input type="text" name="e_accno" class="form-control" placeholder="Nombor Akaun" data-required="true" />
                                </div>
                            </div>
                        </div>
                    </div>
					<div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><strong>Branch</strong> <i class="fa fa-asterisk text-danger"></i></label>
                                <div class="input-group m-b">
                                    <span class="input-group-addon">
                                        <i class="fa fa-building"></i>
                                    </span>
									{!! Form::select('e_branch', $branch, old('branch_id'), ['class' => 'form-control m-b chosen-e-branch', 'data-required' => 'true', 'placeholder' => 'Pilih Branch']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
					<fieldset class="scheduler-border">
						<legend class="scheduler-border">Talian Kecemasan</legend>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label><strong>Nama</strong></label>
									<div class="input-group m-b">
										<span class="input-group-addon">
											<i class="fa fa-user-circle-o"></i>
										</span>
										<input type="text" name="e_emergencyname" class="form-control" placeholder="Nama Penuh" />
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label><strong>Nombor Telefon</strong></label>
									<div class="input-group m-b">
										<span class="input-group-addon">
											<i class="fa fa-phone"></i>
										</span>
										<input type="text" name="e_emergencyphone" class="form-control" placeholder="Nombor Telefon" />
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label><strong>Hubungan</strong></label>
									<div class="input-group m-b">
										<span class="input-group-addon">
											<i class="fa fa-users"></i>
										</span>
										<input type="text" name="e_emergencyrelation" class="form-control" placeholder="Hubungan" />
									</div>
								</div>
							</div>
						</div>
					</fieldset>
                </div>
                <div class="modal-footer">
                    <a id="update-rider" class="btn btn-primary">Simpan</a>
                    <a class="btn btn-default" data-dismiss="modal">Tutup</a>
                </div>
            </form>    
        </div>
    </div>
</div>

<div class="modal inmodal" id="modal-delete-rider">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Hapus Maklumat Rider
                    <br /><small>Anda pasti untuk menghapuskan maklumat rider ini?</small>
                </h4>
            </div>
            <form id="form-delete-rider">
				{{ csrf_field() }}
                <div class="modal-body">
                    <div class="message-form"></div>
					<input type="hidden" name="d_id">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label><strong>Nama Penuh</strong></label>
                                <div class="input-group m-b">
                                    <span class="input-group-addon">
                                        <i class="fa fa-user-circle-o"></i>
                                    </span>
                                    <input type="text" name="d_name" class="form-control" placeholder="Nama Penuh" disabled="true" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><strong>Nombor Kad Pengenalan</strong></label>
                                <div class="input-group m-b">
                                    <span class="input-group-addon">
                                        <i class="fa fa-drivers-license-o"></i>
                                    </span>
                                    <input type="text" name="d_icno" class="form-control" placeholder="Nombor Kad Pengenalan" disabled="true" />
                                </div>
                            </div>
                        </div>
						<div class="col-md-6">
                            <div class="form-group">
                                <label><strong>Alamat Emel</strong></label>
                                <div class="input-group m-b">
                                    <span class="input-group-addon">
                                        <i class="fa fa-envelope-o"></i>
                                    </span>
                                    <input type="text" name="d_email" class="form-control" placeholder="Alamat Emel" disabled="true" />
                                </div>
                            </div>
                        </div>
                    </div>
					<div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><strong>Nombor Telefon</strong></label>
                                <div class="input-group m-b">
                                    <span class="input-group-addon">
                                        <i class="fa fa-phone"></i>
                                    </span>
                                    <input type="text" name="d_phone" class="form-control" placeholder="Nombor Telefon" disabled="true" />
                                </div>
                            </div>
                        </div>
                    </div>
					<div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label><strong>Alamat</strong></label>
                                <div class="input-group m-b">
                                    <span class="input-group-addon">
                                        <i class="fa fa-home"></i>
                                    </span>
									<textarea type="text" name="d_address" class="form-control" placeholder="Alamat" rows="3" disabled="true"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><strong>Bank</strong></label>
                                <div class="input-group m-b">
                                    <span class="input-group-addon">
                                        <i class="fa fa-university"></i>
                                    </span>
                                    <input type="text" name="d_bank" class="form-control" placeholder="Bank" disabled="true" />
                                </div>
                            </div>
                        </div>
						<div class="col-md-6">
                            <div class="form-group">
                                <label><strong>Nombor Akaun</strong></label>
                                <div class="input-group m-b">
                                    <span class="input-group-addon">
                                        <i class="fa fa-credit-card"></i>
                                    </span>
                                    <input type="text" name="d_accno" class="form-control" placeholder="Nombor Akaun" disabled="true" />
                                </div>
                            </div>
                        </div>
                    </div>
					<div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><strong>Branch</strong></label>
                                <div class="input-group m-b">
                                    <span class="input-group-addon">
                                        <i class="fa fa-building"></i>
                                    </span>
                                    <input type="text" name="d_branch" class="form-control" placeholder="Branch" disabled="true" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a id="delete-rider" class="btn btn-primary">Hapus</a>
                    <a class="btn btn-default" data-dismiss="modal">Tutup</a>
                </div>
            </form>    
        </div>
    </div>
</div>

<div class="modal inmodal" id="modal-filter-fdata">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Tutup</span></button>
                <h4 class="modal-title">Tapisan Data Rider</h4>
            </div>
            <form id="form-filter-fdata" method="POST" action="{{url('/riderList')}}">
                {{ csrf_field() }}
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><strong>Branch</strong></label>
                                <div class="input-group m-b">
                                    <span class="input-group-addon">
                                        <i class="fa fa-building"></i>
                                    </span>
                                    {!! Form::select('branch_id', $branch, old('branch_id'), ['class' => 'form-control m-b chosen-branch', 'placeholder' => 'Pilih Branch', 'id' => 'branch_id']) !!}
                                </div>
                            </div>
                        </div>
						<div class="col-md-6">
                            <div class="form-group">
                                <label><strong>Bank</strong></label>
                                <div class="input-group m-b">
                                    <span class="input-group-addon">
                                        <i class="fa fa-university"></i>
                                    </span>
                                    {!! Form::select('bank_id', $bank, old('bank_id'), ['class' => 'form-control m-b chosen-bank', 'placeholder' => 'Pilih Bank', 'id' => 'bank_id']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="filter-fdata" class="btn btn-primary" type="submit">Tapis</button>
                    <a class="btn btn-default" data-dismiss="modal">Tutup</a>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="{{ URL::asset('inspinia/js/plugins/dataTables/datatables.min.js') }}"></script>
<script src="{{ URL::asset('assets/parsley/parsley.min.js') }}"></script>
<script src="{{ URL::asset('assets/parsley/parsley.extend.js') }}"></script>
<script src="{{ URL::asset('inspinia/js/plugins/chosen/chosen.jquery.js') }}"></script>
<script src="{{ URL::asset('inspinia/js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>
<script src="{{ URL::asset('contact/js/rider.js') }}"></script>
<script>  
$('.table-list-rider').DataTable({
	processing: true,
	serverSide: true,
	ajax: '{{ route('getRiderData') }}?bank_id='+$('#bank_id').val()+'&branch_id='+$('#branch_id').val(),
	columns: [
		{data: 'id', name: 'id'},
		{data: 'detail', name: 'detail'},
		{data: 'address', name: 'address'},
		{data: 'bankaccount', name: 'bankaccount'},
		{data: 'branch', name: 'branch.name'},
		{data: 'emergency', name: 'emergency'},
		{data: 'action', name: 'action'},
	],
	pageLength: 25,
	responsive: true,
	dom: 'lTfgitp',
	language: {
		decimal:        "",
		emptyTable:     "Tiada data",
		info:           "Paparan _START_ sehingga _END_ daripada _TOTAL_ rekod",
		infoEmpty:      "Paparan 0 sehingga 0 daripada 0 rekod",
		infoFiltered:   "(Tapisan daripada _MAX_ jumlah rekod)",
		infoPostFix:    "",
		thousands:      ",",
		lengthMenu:     "Paparan _MENU_ rekod",
		loadingRecords: "Sedang memuatkan...",
		processing:     "Sedang diproses...",
		search:         "Carian:",
		zeroRecords:    "Tiada rekod yang dijumpai",
		paginate: {
			first:      "Pertama",
			last:       "Terakhir",
			next:       "Berikut",
			previous:   "Terdahulu"
		},
		aria: {
			sortAscending:  ": aktif untuk susunan jaluran menaik",
			sortDescending: ": aktif untuk susunan jaluran menurun"
		}
	}
});
	
$(document).on('click','.rider-filter',function() {
    $('#modal-filter-fdata').modal('show');
    $('#form-filter-fdata')[0].reset();
});
        
$(document).on('click','.rider-edit',function() {
	var id = $(this).data('id');
    $('#modal-edit-rider').modal('show');
    $('#form-edit-rider')[0].reset();
	$('.message-form').html('');
	
	$.ajax({
		url: "ajaxViewRider&id=" + id,
		type: "GET",
		dataType: "JSON",
		success: function(data){
			$('[name="e_id"]').val(data.id);
			$('[name="e_name"]').val(data.name);
			$('[name="e_icno"]').val(data.icno);
			$('[name="e_email"]').val(data.email);
			$('[name="e_phone"]').val(data.phone);
			$('[name="e_address"]').val(data.address);
			$('[name="e_bank"]').val(data.bank_id).trigger('chosen:updated');
			$('[name="e_accno"]').val(data.accno);
			$('[name="e_branch"]').val(data.branch_id).trigger('chosen:updated');
            $('.chosen-e-bank, .chosen-e-branch').change();
			$('[name="e_emergencyname"]').val(data.emergencyname);
			$('[name="e_emergencyphone"]').val(data.emergencyphone);
			$('[name="e_emergencyrelation"]').val(data.emergencyrelation);
		},
		error: function(jqXHR, textStatus, errorThrown){
			alert('Error get data from ajax');
		}
	});
});
</script>
@endsection