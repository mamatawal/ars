<?php

namespace App\Http\Controllers;

class SiteController extends Controller
{
	public function offline(){
        return view('site.offline');
    }
}