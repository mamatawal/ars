<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ARS Rider Management System</title>
    <link href="{{ URL::asset('inspinia/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('inspinia/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('inspinia/css/animate.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('inspinia/css/style.css') }}" rel="stylesheet">
</head>

<body class="white-bg">
	@yield('content')
</body>

<!-- Mainly scripts -->
<script src="{{ URL::asset('inspinia/js/jquery-3.1.1.min.js') }}"></script>
<script src="{{ URL::asset('inspinia/js/bootstrap.min.js') }}"></script>
<script src="{{ URL::asset('inspinia/js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>

<!-- Custom and plugin javascript -->
<script src="{{ URL::asset('inspinia/js/inspinia.js') }}"></script>

<script type="text/javascript">
	window.print();
</script>
</html>