@extends('layouts/main/main')

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Dashboard</h2>
        <ol class="breadcrumb">
            <li>
                Dashboard
            </li>
            <li>
                Paparan Data
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content">
    <div class="row">
		@if (session('status'))
			<div class="alert alert-success">
				{{ session('status') }}
			</div>
		@endif
<!--
        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5 class="m-b-md">Jumlah Bawa</h5>
					<span class="label label-primary pull-right">Today</span>
                </div>
                <div class="ibox-content">
                    <h2 class="text-success">
                    	{{ $outbound }}
                    </h2>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5 class="m-b-md">Jumlah Serah</h5>
					<span class="label label-primary pull-right">Today</span>
                </div>
                <div class="ibox-content">
                    <h2 class="text-info">
                        {{ $inbound }}
                    </h2>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5 class="m-b-md">Jumlah Gagal Serah</h5>
					<span class="label label-primary pull-right">Today</span>
                </div>
                <div class="ibox-content">
                    <h2 class="text-warning">
                        {{ $failed }}
                    </h2>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5 class="m-b-md">Jumlah Hilang</h5>
					<span class="label label-primary pull-right">Today</span>
                </div>
                <div class="ibox-content">
                    <h2 class="text-danger">
                        {{ $lost }}
                    </h2>
                </div>
            </div>
        </div>
-->
    </div>
	
	<div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Carta Data Parcel <small>(Jumlah Serah) bagi</small> Tahun {{ date('Y') }}</h5>   
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="row">
                                <div>
                                    <div id="barChart"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="row">
                                <div>
                                    <div id="lineChart"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="row">
                                <div>
                                    <div id="pieChart"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	
	<div class="row">
		<div class="col-lg-6">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Statistik Bulanan mengikut Branch</h5>
					<ul class="navbar-top-links navbar-right navbar-right">
                        <li class="dropdown">
                            <a class="dropdown-toggle monthlybranch-filter">
                                <i class="fa fa-calendar"></i> <span id="monthlybranch">{{date('M Y')}}</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover table-monthly-branch">
                            <thead>
                                <tr>
                                    <th width="20%">Branch</th>
                                    <th>Accuracy (%)</th>
                                    <th>Jumlah Bawa</th>
                                    <th>Jumlah Serah</th>
                                    <th>Jumlah Gagal Serah</th>
                                    <th>Jumlah Hilang</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
		
        <div class="col-lg-6">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Statistik Harian mengikut Branch</h5>
					<ul class="navbar-top-links navbar-right navbar-right">
                        <li class="dropdown">
                            <a class="dropdown-toggle dailybranch-filter">
                                <i class="fa fa-calendar"></i> <span id="dailybranch">{{date('d/m/Y', strtotime("-1 days"))}}</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover table-daily-branch">
                            <thead>
                                <tr>
                                    <th width="20%">Branch</th>
                                    <th>Accuracy (%)</th>
                                    <th>Jumlah Bawa</th>
                                    <th>Jumlah Serah</th>
                                    <th>Jumlah Gagal Serah</th>
                                    <th>Jumlah Hilang</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
	
	<div class="row">
		<div class="col-lg-6">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Statistik Bulanan mengikut Rider</h5>
					<ul class="navbar-top-links navbar-right navbar-right">
                        <li class="dropdown">
                            <a class="dropdown-toggle monthlyrider-filter">
                                <i class="fa fa-calendar"></i> <span id="monthlyrider">{{date('M Y')}}</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover table-monthly-rider">
                            <thead>
                                <tr>
                                    <th width="20%">Rider</th>
                                    <th>Accuracy (%)</th>
                                    <th>Jumlah Bawa</th>
                                    <th>Jumlah Serah</th>
                                    <th>Jumlah Gagal Serah</th>
                                    <th>Jumlah Hilang</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
		
        <div class="col-lg-6">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Statistik Harian mengikut Rider</h5>
					<ul class="navbar-top-links navbar-right navbar-right">
                        <li class="dropdown">
                            <a class="dropdown-toggle dailyrider-filter">
                                <i class="fa fa-calendar"></i> <span id="dailyrider">{{date('d/m/Y', strtotime("-1 days"))}}</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover table-daily-rider">
                            <thead>
                                <tr>
                                    <th width="20%">Rider</th>
                                    <th>Accuracy (%)</th>
                                    <th>Jumlah Bawa</th>
                                    <th>Jumlah Serah</th>
                                    <th>Jumlah Gagal Serah</th>
                                    <th>Jumlah Hilang</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
	
</div>

<div class="modal inmodal" id="modal-filter-dailyrider">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Tutup</span></button>
                <h4 class="modal-title">Tapisan Data</h4>
            </div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label><strong>Tarikh</strong></label>
							<div class="input-group date">
								<span class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</span>
								{!! Form::text('dailyriderdate', date('d/m/Y', strtotime("-1 days")), array('class' => 'form-control', 'onkeydown' => 'return false', 'readonly')) !!}
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button id="save-dailyrider" class="btn btn-primary">Tapis</button>
				<a class="btn btn-default" data-dismiss="modal">Tutup</a>
			</div>
        </div>
    </div>
</div>

<div class="modal inmodal" id="modal-filter-monthlyrider">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Tutup</span></button>
                <h4 class="modal-title">Tapisan Data</h4>
            </div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label><strong>Bulan</strong></label>
							<div class="input-group">
								<span class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</span>
								{!! Form::text('monthlyrider', date('m'), array('class' => 'form-control', 'onkeydown' => 'return false', 'readonly')) !!}
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label><strong>Tahun</strong></label>
							<div class="input-group">
								<span class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</span>
								{!! Form::text('yearlyrider', date('Y'), array('class' => 'form-control', 'onkeydown' => 'return false', 'readonly')) !!}
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button id="save-monthlyrider" class="btn btn-primary">Tapis</button>
				<a class="btn btn-default" data-dismiss="modal">Tutup</a>
			</div>
        </div>
    </div>
</div>

<div class="modal inmodal" id="modal-filter-dailybranch">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Tutup</span></button>
                <h4 class="modal-title">Tapisan Data</h4>
            </div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label><strong>Tarikh</strong></label>
							<div class="input-group date">
								<span class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</span>
								{!! Form::text('dailybranchdate', date('d/m/Y', strtotime("-1 days")), array('class' => 'form-control', 'onkeydown' => 'return false', 'readonly')) !!}
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button id="save-dailybranch" class="btn btn-primary">Tapis</button>
				<a class="btn btn-default" data-dismiss="modal">Tutup</a>
			</div>
        </div>
    </div>
</div>

<div class="modal inmodal" id="modal-filter-monthlybranch">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Tutup</span></button>
                <h4 class="modal-title">Tapisan Data</h4>
            </div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label><strong>Bulan</strong></label>
							<div class="input-group">
								<span class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</span>
								{!! Form::text('monthlybranch', date('m'), array('class' => 'form-control', 'onkeydown' => 'return false', 'readonly')) !!}
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label><strong>Tahun</strong></label>
							<div class="input-group">
								<span class="input-group-addon">
									<i class="fa fa-calendar"></i>
								</span>
								{!! Form::text('yearlybranch', date('Y'), array('class' => 'form-control', 'onkeydown' => 'return false', 'readonly')) !!}
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button id="save-monthlybranch" class="btn btn-primary">Tapis</button>
				<a class="btn btn-default" data-dismiss="modal">Tutup</a>
			</div>
        </div>
    </div>
</div>
@endsection

@section('js')
<!--
<script src="{{ URL::asset('inspinia/js/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ URL::asset('inspinia/js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
-->

<script src="{{ URL::asset('inspinia/js/plugins/d3/d3.min.js') }}"></script>
<script src="{{ URL::asset('inspinia/js/plugins/c3/c3.min.js') }}"></script>>
<script src="{{ URL::asset('assets/parsley/parsley.min.js') }}"></script>
<script src="{{ URL::asset('assets/parsley/parsley.extend.js') }}"></script>
<script src="{{ URL::asset('inspinia/js/plugins/chosen/chosen.jquery.js') }}"></script>
<script src="{{ URL::asset('inspinia/js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>
<script src="{{ URL::asset('inspinia/js/plugins/slick/slick.min.js') }}"></script>
<script src="{{ URL::asset('inspinia/js/plugins/dataTables/datatables.min.js') }}"></script>
<script type="text/javascript" language="javascript" src="{{ URL::asset('js/Chart.bundle.min.js') }}"></script>

<script type="text/javascript">
c3.generate({
	bindto: '#barChart',
	data:{
		columns: <?php echo json_encode($rs) ?>,
		colors:{
			'Shah Alam': '#000000',
			'Banting': '#e42238',
			'Terengganu': '#1ab394',
			'Kuching': '#3e80c5'
		},
		type: 'bar',
	},
	axis: {
		x: {
			type: 'category',
			categories: ['Jan', 'Feb', 'Mac', 'Apr', 'Mei', 'Jun', 'Jul', 'Ogos', 'Sep', 'Okt', 'Nov', 'Dis']
		}
	}
});

c3.generate({
	bindto: '#lineChart',
	data:{
		columns: <?php echo json_encode($rs) ?>,
		colors:{
			'Shah Alam': '#000000',
			'Banting': '#e42238',
			'Terengganu': '#1ab394',
			'Kuching': '#3e80c5'
		},
		type: 'spline'
	},
	axis: {
		x: {
			type: 'category',
			categories: ['Jan', 'Feb', 'Mac', 'Apr', 'Mei', 'Jun', 'Jul', 'Ogos', 'Sep', 'Okt', 'Nov', 'Dis']
		}
	}
});

c3.generate({
	bindto: '#pieChart',
	data:{
		columns: <?php echo json_encode($pie) ?>,
		colors:{
			'Shah Alam': '#000000',
			'Banting': '#e42238',
			'Terengganu': '#1ab394',
			'Kuching': '#3e80c5'
		},
		type : 'pie'
	},
	title: {
	  text: 'Carta Bulanan Terkini'
	}
});	

$('[name="monthlyrider"], [name="monthlybranch"]').datepicker({
	todayBtn: "linked",
	autoclose: true,
	minViewMode: "months",
	format: "mm"
});

$('[name="yearlyrider"], [name="yearlybranch"]').datepicker({
	todayBtn: "linked",
	minViewMode: "years",
	autoclose: true,
	format: "yyyy"
});

$('[name="dailyriderdate"], [name="dailybranchdate"]').datepicker({
	todayBtn: "linked",
	keyboardNavigation: false,
	forceParse: false,
	calendarWeeks: false,
	autoclose: true,
	format: "dd/mm/yyyy"
});

$('.table-daily-rider').DataTable({
	aaSorting: [],
	processing: true,
	serverSide: true,
	ajax: '{{ url('getRiderDailyData') }}',
	columns: [
		{data: 'name', name: 'name'},
		{data: 'accuracy', name: 'accuracy'},
		{data: 'outbound', name: 'outbound'},
		{data: 'inbound', name: 'inbound'},
		{data: 'failed', name: 'failed'},
		{data: 'lost', name: 'lost'},
	],
	pageLength: 25,
	responsive: true,
	dom: 'lTfgitp',
	language: {
		decimal:        "",
		emptyTable:     "Tiada data",
		info:           "Paparan _START_ sehingga _END_ daripada _TOTAL_ rekod",
		infoEmpty:      "Paparan 0 sehingga 0 daripada 0 rekod",
		infoFiltered:   "(Tapisan daripada _MAX_ jumlah rekod)",
		infoPostFix:    "",
		thousands:      ",",
		lengthMenu:     "Paparan _MENU_ rekod",
		loadingRecords: "Sedang memuatkan...",
		processing:     "Sedang diproses...",
		search:         "Carian:",
		zeroRecords:    "Tiada rekod yang dijumpai",
		paginate: {
			first:      "Pertama",
			last:       "Terakhir",
			next:       "Berikut",
			previous:   "Terdahulu"
		},
		aria: {
			sortAscending:  ": aktif untuk susunan jaluran menaik",
			sortDescending: ": aktif untuk susunan jaluran menurun"
		}
	}
});
	
$('.table-monthly-rider').DataTable({
	aaSorting: [],
	processing: true,
	serverSide: true,
	ajax: '{{ url('getRiderMonthlyData') }}',
	columns: [
		{data: 'name', name: 'name'},
		{data: 'accuracy', name: 'accuracy'},
		{data: 'outbound', name: 'outbound'},
		{data: 'inbound', name: 'inbound'},
		{data: 'failed', name: 'failed'},
		{data: 'lost', name: 'lost'},
	],
	pageLength: 25,
	responsive: true,
	dom: 'lTfgitp',
	language: {
		decimal:        "",
		emptyTable:     "Tiada data",
		info:           "Paparan _START_ sehingga _END_ daripada _TOTAL_ rekod",
		infoEmpty:      "Paparan 0 sehingga 0 daripada 0 rekod",
		infoFiltered:   "(Tapisan daripada _MAX_ jumlah rekod)",
		infoPostFix:    "",
		thousands:      ",",
		lengthMenu:     "Paparan _MENU_ rekod",
		loadingRecords: "Sedang memuatkan...",
		processing:     "Sedang diproses...",
		search:         "Carian:",
		zeroRecords:    "Tiada rekod yang dijumpai",
		paginate: {
			first:      "Pertama",
			last:       "Terakhir",
			next:       "Berikut",
			previous:   "Terdahulu"
		},
		aria: {
			sortAscending:  ": aktif untuk susunan jaluran menaik",
			sortDescending: ": aktif untuk susunan jaluran menurun"
		}
	}
});
	
$('.table-daily-branch').DataTable({
	aaSorting: [],
	processing: true,
	serverSide: true,
	ajax: '{{ url('getBranchDailyData') }}',
	columns: [
		{data: 'name', name: 'name'},			   
		{data: 'accuracy', name: 'accuracy'},
		{data: 'outbound', name: 'outbound'},
		{data: 'inbound', name: 'inbound'},
		{data: 'failed', name: 'failed'},
		{data: 'lost', name: 'lost'},
	],
	pageLength: 25,
	responsive: true,
	dom: 'lTfgitp',
	language: {
		decimal:        "",
		emptyTable:     "Tiada data",
		info:           "Paparan _START_ sehingga _END_ daripada _TOTAL_ rekod",
		infoEmpty:      "Paparan 0 sehingga 0 daripada 0 rekod",
		infoFiltered:   "(Tapisan daripada _MAX_ jumlah rekod)",
		infoPostFix:    "",
		thousands:      ",",
		lengthMenu:     "Paparan _MENU_ rekod",
		loadingRecords: "Sedang memuatkan...",
		processing:     "Sedang diproses...",
		search:         "Carian:",
		zeroRecords:    "Tiada rekod yang dijumpai",
		paginate: {
			first:      "Pertama",
			last:       "Terakhir",
			next:       "Berikut",
			previous:   "Terdahulu"
		},
		aria: {
			sortAscending:  ": aktif untuk susunan jaluran menaik",
			sortDescending: ": aktif untuk susunan jaluran menurun"
		}
	}
});
	
$('.table-monthly-branch').DataTable({
	fixedColumns: {
        leftColumns: 2
    },
	aaSorting: [],
	processing: true,
	serverSide: true,
	ajax: '{{ url('getBranchMonthlyData') }}',
	columns: [
		{data: 'name', name: 'name'},
		{data: 'accuracy', name: 'accuracy'},
		{data: 'outbound', name: 'outbound'},
		{data: 'inbound', name: 'inbound'},
		{data: 'failed', name: 'failed'},
		{data: 'lost', name: 'lost'},
	],
	pageLength: 25,
	responsive: true,
	dom: 'lTfgitp',
	language: {
		decimal:        "",
		emptyTable:     "Tiada data",
		info:           "Paparan _START_ sehingga _END_ daripada _TOTAL_ rekod",
		infoEmpty:      "Paparan 0 sehingga 0 daripada 0 rekod",
		infoFiltered:   "(Tapisan daripada _MAX_ jumlah rekod)",
		infoPostFix:    "",
		thousands:      ",",
		lengthMenu:     "Paparan _MENU_ rekod",
		loadingRecords: "Sedang memuatkan...",
		processing:     "Sedang diproses...",
		search:         "Carian:",
		zeroRecords:    "Tiada rekod yang dijumpai",
		paginate: {
			first:      "Pertama",
			last:       "Terakhir",
			next:       "Berikut",
			previous:   "Terdahulu"
		},
		aria: {
			sortAscending:  ": aktif untuk susunan jaluran menaik",
			sortDescending: ": aktif untuk susunan jaluran menurun"
		}
	}
});
	
$(document).on('click','.dailyrider-filter',function() {
    $('#modal-filter-dailyrider').modal('show');
});
	
$(document).on('click','.monthlyrider-filter',function() {
    $('#modal-filter-monthlyrider').modal('show');
});
	
$(document).on('click','.dailybranch-filter',function() {
    $('#modal-filter-dailybranch').modal('show');
});
	
$(document).on('click','.monthlybranch-filter',function() {
    $('#modal-filter-monthlybranch').modal('show');
});
	
$('#save-dailyrider').click(function () {
	var newdate = $('[name="dailyriderdate"]').val();
	$('#modal-filter-dailyrider').modal('hide');
	$('#dailyrider').html(newdate);
	
	var listTable = $('.table-daily-rider').DataTable();
	var newUrl = '{{ url("getRiderDailyData") }}' + '?date_=' + newdate;
	listTable.ajax.url(newUrl).load();
});
	
$('#save-monthlyrider').click(function () {
	var newmonth = $('[name="monthlyrider"]').val();
	var newyear = $('[name="yearlyrider"]').val();
	var monthname = $('[name="monthlyrider"]').datepicker('getDate').toLocaleString('default', { month: 'short' });
	
	$('#modal-filter-monthlyrider').modal('hide');
	$('#monthlyrider').html(monthname + ' ' + newyear);
	
	var listTable = $('.table-monthly-rider').DataTable();
	var newUrl = '{{ url("getRiderMonthlyData") }}' + '?month_=' + newmonth + '&year_=' + newyear;
	listTable.ajax.url(newUrl).load();
});
	
$('#save-dailybranch').click(function () {
	var newdate = $('[name="dailybranchdate"]').val();
	$('#modal-filter-dailybranch').modal('hide');
	$('#dailybranch').html(newdate);
	
	var listTable = $('.table-daily-branch').DataTable();
	var newUrl = '{{ url("getBranchDailyData") }}' + '?date_=' + newdate;
	listTable.ajax.url(newUrl).load();
});
	
$('#save-monthlybranch').click(function () {
	var newmonth = $('[name="monthlybranch"]').val();
	var newyear = $('[name="yearlybranch"]').val();
	var monthname = $('[name="monthlybranch"]').datepicker('getDate').toLocaleString('default', { month: 'short' });
	
	$('#modal-filter-monthlybranch').modal('hide');
	$('#monthlybranch').html(monthname + ' ' + newyear);
	
	var listTable = $('.table-monthly-branch').DataTable();
	var newUrl = '{{ url("getBranchMonthlyData") }}' + '?month_=' + newmonth + '&year_=' + newyear;
	listTable.ajax.url(newUrl).load();
});
</script>
@endsection