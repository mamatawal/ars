<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\MailResetPasswordNotification as MailResetPasswordNotification;
use App\Scopes\ActiveScope;
use OwenIt\Auditing\Contracts\Auditable;

class User extends Authenticatable implements Auditable
{
    use Notifiable;
    use \OwenIt\Auditing\Auditable;
    
    protected $auditInclude = [
        'id','icno','fullname','email','role_id','sekatan','lastlogin','lastlogout','created_by','updated_by','disable',
    ];
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'icno', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
	
	protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new ActiveScope);
    }
	
	public function role()
    {
        return $this->belongsTo('App\Role');
    }
	
	public function sendPasswordResetNotification($token)
	{
		$this->notify(new MailResetPasswordNotification($token));
	}
    
    public function admin() {
        return $this->role()->whereIn('name', ['ADMIN'])->exists();
    }
    
    public function pengguna() {
        return $this->role()->whereIn('name', ['SUPERVISOR'])->exists();
    }
	
	public function creator()
    {
        return $this->belongsTo('App\User', 'created_by');
    }
	
	public function updater()
    {
        return $this->belongsTo('App\User', 'updated_by');
    }
}
