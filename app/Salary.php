<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Scopes\ActiveScope;
use OwenIt\Auditing\Contracts\Auditable;

class Salary extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    
    protected $auditInclude = [
        'id','salarymonth','supervision','otheraddition','attendance','performance','advance','socso','otherdeduction','rider_id','created_by','updated_by','disable',
    ];
    
    protected static function boot()
    {
        parent::boot();
		
		static::addGlobalScope(new ActiveScope);
    }
	
	public function rider()
    {
        return $this->belongsTo('App\Rider');
    }
	
	public function creator()
    {
        return $this->belongsTo('App\User', 'created_by');
    }
	
	public function updater()
    {
        return $this->belongsTo('App\User', 'updated_by');
    }
}
