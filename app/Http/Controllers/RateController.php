<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Rate;
use App\Rider;
use App\Branch;
use App\Salary;
use App\Parcel;
use Auth;
use DB;
use Validator;

class RateController extends Controller
{	
	public function salary(Request $request){
		$branch = Branch::orderBy('id')->pluck('name','id');
		$rider = Rider::orderBy('id')->pluck('name','id');
		
        return view('site.salary', compact('branch','rider'));
    }
	
	public function populateMode(Request $request){
		$prevMonth = $request->input('month_')-1;
		$prevYear = $request->input('year_');
		
		if($prevMonth == 0){
			$prevMonth = 12;
			$prevYear = $prevYear-1;
		}
		
		if($request->input('rider')){
			$rs = DB::select(DB::raw(
				"select 
					string_agg(distinct t.name, ',') as modes 
				from parcels p 
				inner join types t on t.id = p.type_id
				where dateout between '".$prevYear."-".$prevMonth."-28'::date and '".$request->input('year_')."-".$request->input('month_')."-27'::date
				and p.disable = 'ACTIVE' and p.rider_id = ".$request->input('rider')
			));
		}
		
        return $rs;
    }
	
	public function populateSalary(Request $request){
		$prevMonth = $request->input('month_')-1;
		$prevYear = $request->input('year_');
		
		if($prevMonth == 0){
			$prevMonth = 12;
			$prevYear = $prevYear-1;
		}
		
		if($request->input('rider')){
			$rs = DB::select(DB::raw(
				"select
					date_field,
					case when inbound is null then 0 else inbound end as inbound,
					case when basic is null then 0 else basic end as basic,
					allowance,
					case when basic is null then 0 else basic end + allowance as total
				from (
					select 
						to_char(i, 'dd/mm/yyyy') as date_field,
						p.inbound as inbound,
						(case 
								when p.level2_id is null then
									case when r1.is_sa = 1 and p.type_id = 1 then p.inbound * r1.price
									else
										r1.price
									end
								else
									case
										when r2.is_sa = 1 then r2.min + (case when r2.allowance is null then 0 else r2.allowance end)
										else 
											r1.price + (case when r2.allowance is null then 0 else r2.allowance end) 
									end
								end
						) as basic,
						(case 
								when p.level2_id is null then 0
								else
									case
										when r2.is_sa = 1 then 
											case when p.level3_id is null then r2.price * (p.inbound - r2.min)
											else
												(r2.price * (r2.max - r2.min)) + (r3.price * (p.inbound - r2.max))
											end
										else 
											r2.price * (p.inbound - r1.price)
									end
								end
						) as allowance
					from (
						select i::date from generate_series('".$prevYear."-".$prevMonth."-28', '".$request->input('year_')."-".$request->input('month_')."-27', '1 day'::interval) i
						) i
					left join parcels p on date(p.dateout) = i.i and p.disable= 'ACTIVE' and p.rider_id = ".$request->input('rider')."
					left join rates r1 on p.level1_id = r1.id
					left join rates r2 on p.level2_id = r2.id
					left join rates r3 on p.level3_id = r3.id
				) as salary"
			));
		}
		
        return $rs;
    }
	
	public function populateTotalSalary(Request $request){
		$salarymonth = $request->input('year_').'-'.$request->input('month_').'-01';
		
		if($request->input('rider')){
			$rs = Salary::where('rider_id', $request->input('rider'))
				->whereDate('salarymonth', date($salarymonth))->first();
		}
		
        return $rs;
    }
	
	public function ajaxUpdateSalary(Request $request){
		$salarymonth = $request->input('year_').'-'.$request->input('month_').'-01';
        $rs = Salary::where('rider_id', $request->input('rider'))
				->whereDate('salarymonth', date($salarymonth))->first();
		$userid = Auth::user()->id;
		
		if(!$rs){
			$rs = new Salary();
			$rs->rider_id = $request->input('rider');
			$rs->created_by = $userid;
			$rs->disable = 'ACTIVE';
		}
		
		$validator = Validator::make(array_filter($request->all()), [
			'supervision'=> 'regex:/^\d+\.\d{2}?$/',
			'otheraddition'=> 'regex:/^\d+\.\d{2}?$/',
			'attendance'=> 'regex:/^\d+\.\d{2}?$/',
			'performance'=> 'regex:/^\d+\.\d{2}?$/',
			'advance' => 'regex:/^\d+\.\d{2}?$/',
			'socso' => 'regex:/^\d+\.\d{2}?$/',
			'otherdeduction' => 'regex:/^\d+\.\d{2}?$/'
        ],[
			'supervision.regex' => 'Data ruangan SUPERVISION ALLOWANCE mesti di dalam format harga yang betul! (Cth: 10.00)',
			'otheraddition.regex' => 'Data ruangan OTHER ADDITION mesti di dalam format harga yang betul! (Cth: 10.00)',
			'attendance.regex' => 'Data ruangan ATTENDANCE INCENTIVE mesti di dalam format harga yang betul! (Cth: 10.00)',
			'performance.regex' => 'Data ruangan PERFORMANCE INCENTIVE mesti di dalam format harga yang betul! (Cth: 10.00)',
			'advance.regex' => 'Data ruangan ADVANCE mesti di dalam format harga yang betul! (Cth: 10.00)',
			'socso.regex' => 'Data ruangan SOCSO mesti di dalam format harga yang betul! (Cth: 10.00)',
			'otherdeduction.regex' => 'Data ruangan OTHER DEDUCTION mesti di dalam format harga yang betul! (Cth: 10.00)'
		]);
		
		$rs->salarymonth = $salarymonth;
		$rs->supervision = $request->input('supervision');
		$rs->otheraddition = $request->input('otheraddition');
        $rs->attendance = $request->input('attendance');
		$rs->performance = $request->input('performance');
		$rs->advance = $request->input('advance');
		$rs->socso = $request->input('socso');
		$rs->otherdeduction = $request->input('otherdeduction');		
        $rs->updated_by = $userid;
		
		if(!$validator->fails()){
			$rs->save();
			
            $rs['status'] = 'success';
            $rs['message'] = 'Data berjaya disimpan';
        } else {
            $rs['status'] = 'failure'; 
            $rs['message'] = $validator->errors()->first();
        }
		
        return $rs;
    }
	
	public function salaryPrint(Request $request){
		$prevMonth = $request->input('month_')-1;
		$prevYear = $request->input('year_');
		$salarymonth = $request->input('year_').'-'.$request->input('month_').'-01';
		
		$monthname = date('M Y',strtotime($salarymonth));
		
		if($prevMonth == 0){
			$prevMonth = 12;
			$prevYear = $prevYear-1;
		}
		
		$rs = DB::select(DB::raw(
			"select
				date_field,
				case when inbound is null then 0 else inbound end as inbound,
				case when basic is null then 0 else basic end as basic,
				allowance,
				case when basic is null then 0 else basic end + allowance as total
			from (
				select 
					to_char(i, 'dd/mm/yyyy') as date_field,
					p.inbound as inbound,
					(case 
							when p.level2_id is null then
								case when r1.is_sa = 1 and p.type_id = 1 then p.inbound * r1.price
								else
									r1.price
								end
							else
								case
									when r2.is_sa = 1 then r2.min + (case when r2.allowance is null then 0 else r2.allowance end)
									else 
										r1.price + (case when r2.allowance is null then 0 else r2.allowance end) 
								end
							end
					) as basic,
					(case 
							when p.level2_id is null then 0
							else
								case
									when r2.is_sa = 1 then 
										case when p.level3_id is null then r2.price * (p.inbound - r2.min)
										else
											(r2.price * (r2.max - r2.min)) + (r3.price * (p.inbound - r2.max))
										end
									else 
										r2.price * (p.inbound - r1.price)
								end
							end
					) as allowance
				from (
					select i::date from generate_series('".$prevYear."-".$prevMonth."-28', '".$request->input('year_')."-".$request->input('month_')."-27', '1 day'::interval) i
					) i
				left join parcels p on date(p.dateout) = i.i and p.disable= 'ACTIVE' and p.rider_id = ".$request->input('rider')."
				left join rates r1 on p.level1_id = r1.id
				left join rates r2 on p.level2_id = r2.id
				left join rates r3 on p.level3_id = r3.id
			) as salary"
		));
		
		$rider = Rider::find($request->input('rider'));
		
		$mode = DB::select(DB::raw(
				"select 
					string_agg(distinct t.name, ',') as modes 
				from parcels p 
				inner join types t on t.id = p.type_id
				where dateout between '".$prevYear."-".$prevMonth."-28'::date and '".$request->input('year_')."-".$request->input('month_')."-27'::date
				and p.disable = 'ACTIVE' and p.rider_id = ".$request->input('rider')
			));
		
		$modes = $mode[0]->modes;
		
		$salary = Salary::where('rider_id', $request->input('rider'))
				->whereDate('salarymonth', date($salarymonth))->first();
		
		if(!$salary){
			$salary = (object) ['supervision'=> 0, 'otheraddition' => 0, 'attendance' => 0, 'performance' => 0, 'advance' => 0, 'socso' => 0, 'otherdeduction' => 0];
		}
		
        return view('site.printsalary', compact('rs','rider','monthname','modes','salary'));
    }
	
	public function salaryPayslip(Request $request){
		$prevMonth = $request->input('month_')-1;
		$prevYear = $request->input('year_');
		$salarymonth = $request->input('year_').'-'.$request->input('month_').'-01';
		
		$monthname = date('M Y',strtotime($salarymonth));
		
		if($prevMonth == 0){
			$prevMonth = 12;
			$prevYear = $prevYear-1;
		}
		
		$rs = DB::select(DB::raw(
			"select 
				p.rider_id,
				sum(case 
						when p.level2_id is null then
							case when r1.is_sa = 1 and p.type_id = 1 then p.inbound * r1.price
							else
								r1.price
							end
						else
							case
								when r2.is_sa = 1 then r2.min + (case when r2.allowance is null then 0 else r2.allowance end)
								else 
									r1.price + (case when r2.allowance is null then 0 else r2.allowance end) 
							end
						end
				) as basic,
				sum(case 
						when p.level2_id is null then 0
						else
							case
								when r2.is_sa = 1 then 
									case when p.level3_id is null then r2.price * (p.inbound - r2.min)
									else
										(r2.price * (r2.max - r2.min)) + (r3.price * (p.inbound - r2.max))
									end
								else 
									r2.price * (p.inbound - r1.price)
							end
						end
				) as allowance
			from parcels p
			left join rates r1 on p.level1_id = r1.id
			left join rates r2 on p.level2_id = r2.id
			left join rates r3 on p.level3_id = r3.id
			where p.rider_id = ".$request->input('rider')." and dateout between '".$prevYear."-".$prevMonth."-28'::date and '".$request->input('year_')."-".$request->input('month_')."-27'::date
			and p.disable= 'ACTIVE'
			group by rider_id"
		));
		
		if(!$rs){
			$rs[] = (object) ['basic'=> 0, 'allowance'=> 0];
		}
		
		$rider = Rider::find($request->input('rider'));
		
		$mode = DB::select(DB::raw(
				"select 
					string_agg(distinct t.name, ',') as modes 
				from parcels p 
				inner join types t on t.id = p.type_id
				where dateout between '".$prevYear."-".$prevMonth."-28'::date and '".$request->input('year_')."-".$request->input('month_')."-27'::date
				and p.disable = 'ACTIVE' and p.rider_id = ".$request->input('rider')
			));
		
		$modes = $mode[0]->modes;
		
		$salary = Salary::where('rider_id', $request->input('rider'))
				->whereDate('salarymonth', date($salarymonth))->first();
		
        return view('site.payslip', compact('rs','rider','monthname','modes','salary'));
    }
	
	public function formula($id){
		$ft1 = Rate::where('type_id', 1)->where('branch_id', $id)->where('level', '1')->first();
		$ft2 = Rate::where('type_id', 1)->where('branch_id', $id)->where('level', '2')->first();
		$ft3 = Rate::where('type_id', 1)->where('branch_id', $id)->where('level', '3')->first();
		$pt1 = Rate::where('type_id', 2)->where('branch_id', $id)->where('level', '1')->first();
		$pt2 = Rate::where('type_id', 2)->where('branch_id', $id)->where('level', '2')->first();
		$pt3 = Rate::where('type_id', 2)->where('branch_id', $id)->where('level', '3')->first();
		$tl1 = Rate::where('type_id', 3)->where('branch_id', $id)->where('level', '1')->first();
		$tl2 = Rate::where('type_id', 3)->where('branch_id', $id)->where('level', '2')->first();
		$tl3 = Rate::where('type_id', 3)->where('branch_id', $id)->where('level', '3')->first();
		$is_sa = Rate::where('type_id', 1)->where('branch_id', $id)->first()->is_sa;
		$branch = Branch::find($id)->name;
		
        return view('site.formula', compact('branch','ft1','ft2','ft3','pt1','pt2','pt3','tl1','tl2','tl3','id','is_sa'));
    }
	
	public function ajaxViewFormula($level, $type, $branch){
		$rs = Rate::where('level', $level)->where('type_id', $type)->where('branch_id', $branch)->first();
		
        return $rs;
    }
	
    public function ajaxUpdateFormula(Request $request, $level, $type, $branch){
        $rs = Rate::where('level', $level)->where('type_id', $type)->where('branch_id', $branch)->first();
		$userid = Auth::user()->id;
		
		$rec = new Rate();
		
		$validator = Validator::make(array_filter($request->all()), [
			'min'=> 'required|numeric',
			'max'=> 'numeric',
			'price'=> 'required|regex:/^\d+\.\d{2}?$/',
			'allowance'=> 'sometimes|regex:/^\d+\.\d{2}?$/'
        ],[
			'min.numeric' => 'Data ruangan Min mesti di dalam format nombor!',
			'max.numeric' => 'Data ruangan Max mesti di dalam format nombor!',
			'price.required' => 'Ruangan Kadar / Parcel wajib diisi!',
			'price.regex' => 'Data ruangan Kadar / Parcel mesti di dalam format harga yang betul! (Cth: 10.00)',
			'allowance.regex' => 'Data ruangan Elaun mesti di dalam format harga yang betul! (Cth: 10.00)',
		]);
		
		$rec->level = $request->input('level');
		$rec->min = $request->input('min');
		$rec->max = $request->input('max');
		$rec->price = $request->input('price');
        $rec->allowance = $request->input('allowance');
        $rec->type_id = $request->input('type');
        $rec->branch_id = $request->input('branch');
        $rec->disable = 'ACTIVE';
        $rec->created_by = $userid;
        $rec->updated_by = $userid;
        $rec->is_sa = $request->input('is_sa');
		
		if(!$validator->fails()){
			$rec->save();
			
			if($rs){
				$rs->updated_by = $userid;
				$rs->disable = 'INACTIVE';
				$rs->save();
			}
			
            $rs['status'] = 'success';
            $rs['success_form'] =   '<div class="row">
                                            <div class="col-md-12">
                                                <div class="alert alert-success alert-dismissable">
                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                    Formula telah berjaya dikemaskini.
                                                </div>
                                            </div>    
                                        </div>';
        } else {
            $rs['status'] = 'failure'; 
            $rs['error_form'] =   '<div class="row">
                                            <div class="col-md-12">
                                                <div class="alert alert-danger alert-dismissable">
                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.
													$validator->errors()->first().
                                                '</div>
                                            </div>    
                                        </div>';
        }
		
        return $rs;
    }
	
	public function updateFormulaBased(Request $request){
        $rs = Rate::where('branch_id', $request->input('branch'))->where('type_id', '!=', 3);
		$userid = Auth::user()->id;
		
		$rs->update(['is_sa' => $request->input('is_sa'), 'updated_by' => $userid]);
    }
	
	public function syncData(Request $request){
		$userid = Auth::user()->id;
		$dt = date('d');
		$month = date('m');
		$year = date('Y');
		$prevYear = $year;
		
		if($dt <= 27){
			$prevMonth = str_pad($month-1, 2, '0', STR_PAD_LEFT);

			if($prevMonth == 0){
				$prevMonth = 12;
				$prevYear = $year-1;
			}
		}else{
			$prevMonth = $month;

			$month = str_pad($prevMonth+1, 2, '0', STR_PAD_LEFT);

			if($month == 13){
				$month = '01';
				$year = $prevYear+1;
			}
		}
		
        $rs = Parcel::where('branch_id', $request->input('branch'))->whereRaw("dateout between '".$prevYear."-".$prevMonth."-28'::date and '".$year."-".$month."-27'::date")->orderBy('id')->get();
		
		foreach($rs as $r){
			$rate = Rate::where('type_id', $r->type_id)
				->where('branch_id', $r->branch_id)
				->whereRaw('(('.$r->inbound.' between min and max and max notnull) or ('.$r->inbound.' >= min and max isnull))')
				->first();

			$level1 = Rate::where('type_id', $r->type_id)
					->where('branch_id', $r->branch_id)
					->where('level', '1')->first()->id;

			$r->level1_id = $level1;

			if($rate->level == '3'){
				$level2 = Rate::where('type_id', $r->type_id)
					->where('branch_id', $r->branch_id)
					->where('level', '2')->first()->id;

				$r->level2_id = $level2;
				$r->level3_id = $rate->id;
			}else if($rate->level == '2'){
				$r->level2_id = $rate->id;
			}
			
			$r->updated_by = $userid;
			$r->save();
		}
    }
}