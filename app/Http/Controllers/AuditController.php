<?php

namespace App\Http\Controllers;

use App\Audit;
use DataTables;

class AuditController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
	
    public function audit(){
	   return view('site.audit');
    }
    
    public function getAuditData()
    {
        $rs = Audit::orderBy('created_at', 'DESC');

    	return DataTables::eloquent($rs)
        ->orderColumn('created_at', '-created_at $1')
        ->toJson();
    }
}
