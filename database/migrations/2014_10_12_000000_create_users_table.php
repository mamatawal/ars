<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('icno');
            $table->string('fullname');
            $table->string('password');
            $table->string('email');
            $table->integer('role_id');
            $table->string('sekatan');
            $table->timestamp('lastlogin')->nullable();
            $table->timestamp('lastlogout')->nullable();
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->string('disable');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
