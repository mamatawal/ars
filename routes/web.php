<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//offline page
Route::get('/offline', 'SiteController@offline');

Auth::routes();
Route::get('/', function () {
	return view('auth.login');
});

//all role can access
Route::get('/dashboard', 'DashboardController@dashboard');
Route::get('/getRiderDailyData', 'DashboardController@getRiderDailyData');
Route::get('/getRiderMonthlyData', 'DashboardController@getRiderMonthlyData');
Route::get('/getBranchDailyData', 'DashboardController@getBranchDailyData');
Route::get('/getBranchMonthlyData', 'DashboardController@getBranchMonthlyData');

Route::get('/parcelList', 'ParcelController@parcelList');
Route::post('/parcelList', 'ParcelController@parcelList');
Route::get('/getParcelData', 'ParcelController@getParcelData')->name('getParcelData');
Route::get('/ajaxViewParcel&id={id}', 'ParcelController@ajaxViewParcel');
Route::post('/ajaxUpdateParcel&id={id}', 'ParcelController@ajaxUpdateParcel');
Route::post('/ajaxRegisterParcel', 'ParcelController@ajaxRegisterParcel');
Route::get('/populateRider', 'ParcelController@populateRider')->name('populateRider');
Route::get('/populateBranch', 'ParcelController@populateBranch')->name('populateBranch');

Route::get('/riderList', 'SettingController@riderList');
Route::post('/riderList', 'SettingController@riderList');
Route::get('/getRiderData', 'SettingController@getRiderData')->name('getRiderData');
Route::get('/ajaxViewRider&id={id}', 'SettingController@ajaxViewRider');
Route::post('/ajaxUpdateRider&id={id}', 'SettingController@ajaxUpdateRider');
Route::post('/ajaxRegisterRider', 'SettingController@ajaxRegisterRider');

Route::get('/userprofile', 'UserController@userProfile');
Route::get('/getUserProfile', 'UserController@getUserProfile')->name('getUserProfile');
Route::post('/ajaxUpdatePassword&id={id}', 'UserController@ajaxUpdatePassword');
Route::get('/ajaxViewUser&id={id}', 'UserController@ajaxViewUser');

Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    Artisan::call('view:clear');
    Artisan::call('config:clear');
    return "Cache is cleared";
});

//role except PENGGUNA SISTEM
Route::group(['middleware' => 'Editor'], function() {
    
});

//role system admin
Route::group(['middleware' => 'Admin'], function() {
	Route::post('/ajaxDeleteParcel&id={id}', 'ParcelController@ajaxDeleteParcel');
	
	Route::get('/salary', 'RateController@salary');
	Route::get('/populateMode', 'RateController@populateMode');
	Route::get('/populateSalary', 'RateController@populateSalary');
	Route::get('/populateTotalSalary', 'RateController@populateTotalSalary');
	Route::get('/ajaxUpdateSalary', 'RateController@ajaxUpdateSalary');
	Route::get('/salary-print', 'RateController@salaryPrint');
	Route::get('/salary-payslip', 'RateController@salaryPayslip');
	Route::get('/syncData', 'RateController@syncData');
	
    Route::post('/ajaxDeleteRider&id={id}', 'SettingController@ajaxDeleteRider');
	
	Route::get('/riderMode', 'SettingController@mode');
    Route::get('/getModeData', 'SettingController@getModeData')->name('getModeData');
    Route::get('/ajaxViewMode&id={id}', 'SettingController@ajaxViewMode');
    Route::post('/ajaxUpdateMode&id={id}', 'SettingController@ajaxUpdateMode');
    Route::post('/ajaxDeleteMode&id={id}', 'SettingController@ajaxDeleteMode');
    Route::post('/ajaxRegisterMode', 'SettingController@ajaxRegisterMode');
	
    Route::get('/branch', 'SettingController@branch');
    Route::get('/getBranchData', 'SettingController@getBranchData')->name('getBranchData');
    Route::get('/ajaxViewBranch&id={id}', 'SettingController@ajaxViewBranch');
    Route::post('/ajaxUpdateBranch&id={id}', 'SettingController@ajaxUpdateBranch');
    Route::post('/ajaxDeleteBranch&id={id}', 'SettingController@ajaxDeleteBranch');
    Route::post('/ajaxRegisterBranch', 'SettingController@ajaxRegisterBranch');
	
	Route::get('/branch-formula/{id}', 'RateController@formula');
	Route::get('/ajaxViewFormula/{level}/{type}/{branch}', 'RateController@ajaxViewFormula');
	Route::post('/ajaxUpdateFormula/{level}/{type}/{branch}', 'RateController@ajaxUpdateFormula');
	Route::get('/updateFormulaBased', 'RateController@updateFormulaBased');
    
    Route::get('/bank', 'SettingController@bank');
    Route::get('/getBankData', 'SettingController@getBankData')->name('getBankData');
    Route::get('/ajaxViewBank&id={id}', 'SettingController@ajaxViewBank');
    Route::post('/ajaxUpdateBank&id={id}', 'SettingController@ajaxUpdateBank');
    Route::post('/ajaxDeleteBank&id={id}', 'SettingController@ajaxDeleteBank');
    Route::post('/ajaxRegisterBank', 'SettingController@ajaxRegisterBank');
	
	Route::get('/userUser', 'UserController@userUser');
    Route::get('/getUserData', 'UserController@getUserData')->name('getUserData');
    Route::post('/ajaxUpdateUser&id={id}', 'UserController@ajaxUpdateUser');
    Route::post('/ajaxRegisterUser', 'UserController@ajaxRegisterUser');
    Route::post('/ajaxDeleteUser&id={id}', 'UserController@ajaxDeleteUser');
    
    Route::get('/userRole', 'UserController@userRole');
    Route::get('/getRoleData', 'UserController@getRoleData')->name('getRoleData');
    Route::get('/ajaxViewRole&id={id}', 'UserController@ajaxViewRole');
    Route::post('/ajaxUpdateRole&id={id}', 'UserController@ajaxUpdateRole');
    Route::post('/ajaxDeleteRole&id={id}', 'UserController@ajaxDeleteRole');
    Route::post('/ajaxRegisterRole', 'UserController@ajaxRegisterRole');

    Route::get('/audit', 'AuditController@audit');
    Route::get('/getAuditData', 'AuditController@getAuditData')->name('getAuditData');
});