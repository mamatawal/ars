$(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip();
});

$(document).on('click','.bank-new',function() {
    $('#modal-new-bank').modal('show');
    $('#form-new-bank')[0].reset();
	$('.message-form').html('');
});

$('#register-bank').click(function(){
	if($('#form-new-bank').parsley().validate()){
		$.ajax({
			url:"ajaxRegisterBank",
			type: "POST",
			data: $('#form-new-bank').serialize(),
			dataType: "JSON",
		 	success: function(data){
				if(data.status == 'success'){
					$('.message-form').html(data.success_form);
						setTimeout(function(){
							$('.message-form').remove();
							$('#modal-new-bank').modal('hide');
							$(location).attr('href','bank');
						}, 1000);
				}else{
					var error = '';
					$.each(data.error_form, function(k,v){
						error += v[0]+'<br/>';
					});
					$('.message-form').html('<div class="row">'+
												'<div class="col-md-12">'+
													'<div class="alert alert-danger alert-dismissable">'+
														'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'+
														error +
													'</div>'+
												'</div>'+    
											'</div>');
				}
			},
			error: function(jqXHR, textStatus, errorThrown){
				alert('Error get data from ajax');
			}
		});
	}
});

$(document).on('click','.bank-edit',function() {
	var id = $(this).data('id');
    $('#modal-edit-bank').modal('show');
    $('#form-edit-bank')[0].reset();
	$('.message-form').html('');
	
	$.ajax({
		url: "ajaxViewBank&id=" + id,
		type: "GET",
		dataType: "JSON",
		success: function(data){
			$('[name="e_id"]').val(data.id);
			$('[name="e_code"]').val(data.code);
			$('[name="e_name"]').val(data.name);
		},
		error: function(jqXHR, textStatus, errorThrown){
			alert('Error get data from ajax');
		}
	});
});

$('#update-bank').click(function(){
	var id = $('[name="e_id"]').val();

	if($('#form-edit-bank').parsley().validate()){
		$.ajax({
			url:"ajaxUpdateBank&id=" + id,
			type: "POST",
			data: $('#form-edit-bank').serialize(),
			dataType: "JSON",
		 	success: function(data){
				if(data.status == 'success'){
					$('.message-form').html(data.success_form);
						setTimeout(function(){
							$('.message-form').remove();
							$('#modal-edit-bank').modal('hide');
							$(location).attr('href','bank');
						}, 1000);
				}else{
					$('.message-form').html(data.error_form);
				}
			},
			error: function(jqXHR, textStatus, errorThrown){
				alert('Error get data from ajax');
			}
		});
	}
});

$(document).on('click','.bank-delete',function() {
	var id = $(this).data('id');
    $('#modal-delete-bank').modal('show');
    $('#form-delete-bank')[0].reset();
	$('.message-form').html('');
	
	$.ajax({
		url: "ajaxViewBank&id=" + id,
		type: "GET",
		dataType: "JSON",
		success: function(data){
			$('[name="d_id"]').val(data.id);
			$('[name="d_code"]').val(data.code);
			$('[name="d_name"]').val(data.name);
		},
		error: function(jqXHR, textStatus, errorThrown){
			alert('Error get data from ajax');
		}
	});
});

$('#delete-bank').click(function(){
	var id = $('[name="d_id"]').val();
	
	$.ajax({
		url:"ajaxDeleteBank&id=" + id,
		type: "POST",
		data: $('#form-delete-bank').serialize(),
		dataType: "JSON",
		success: function(data){
			$(location).attr('href','bank');
		},
		error: function(jqXHR, textStatus, errorThrown){
			alert('Error get data from ajax');
		}
	});
});