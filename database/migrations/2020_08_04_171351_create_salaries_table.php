<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salaries', function (Blueprint $table) {
            $table->increments('id');
			$table->timestamp('salarymonth');
			$table->float('supervision')->nullable();
			$table->float('otheraddition')->nullable();
			$table->float('attendance')->nullable();
			$table->float('performance')->nullable();
			$table->float('advance')->nullable();
			$table->float('socso')->nullable();
			$table->float('otherdeduction')->nullable();
			$table->integer('rider_id');
			$table->integer('created_by');
            $table->integer('updated_by');
            $table->string('disable');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salaries');
    }
}
