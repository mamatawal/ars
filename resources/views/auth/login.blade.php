@extends('layouts.login.main')

@section('content')
<div class="row p-sm login-overlay">
    <div class="row">
        <div class="col-md-6">
            <p class="text-white">
                <small>ARS Rider Management System</small>
            </p>
			<center><img src="{{ URL::asset('contact/images/logo.png') }}" width="300"/></center>
			<center><button type="button" class="btn btn-danger block" id="butInstall"><i class="fa fa-download"></i></button></center>
            <p>&nbsp;</p>
        </div>
        <div class="col-md-6">
            <div class="ibox-content">
                <form class="m-t" name="formlogin" role="form" action="{{ route('login') }}" method="POST">
					{{ csrf_field() }}
                    @if ($errors->has('icno') || $errors->has('password') || $errors->has('disable'))
                        <div class="alert alert-danger contact-alert">
                            <strong>
								{{ $errors->first('icno') }} {{ $errors->first('password') }} {{ $errors->first('disable') }}
                            </strong>
                        </div>
                    @endif
                    <div class="form-group">
						<input type="text" class="form-control" name="icno" placeholder="IC Pengguna" value="{{ old('icno') }}" id="icno" required autofocus>
                    </div>
                    <div class="form-group">
                        <div class="input-group m-b">
                            <input id="password" type="password" placeholder="Kata Laluan" class="form-control masked" name="password" required autocomplete="off">
                            <span class="input-group-addon" id="eye">
                                    <i class="fa fa-eye"></i>
                            </span>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-success block full-width m-b">Log Masuk</button>
<!--                    <a href="{{ route('password.request') }}"><small>Lupa Kata Laluan?</small></a>-->
                </form>
                <p class="m-t">
                    <small>
                        ARS Rider Management System <a class="block-login"><span class="badge badge-success">ARS</span></a>
                    </small>
                </p>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="{{ URL::asset('inspinia/js/plugins/sweetalert/sweetalert.min.js') }}"></script>
<script src="{{ URL::asset('contact/js/login.js') }}"></script>
<script type="text/javascript">
    function show() {
        var p = document.getElementById('password');
        p.setAttribute('type', 'text');
    }

    function hide() {
        var p = document.getElementById('password');
        p.setAttribute('type', 'password');
    }

    var pwShown = 0;

    document.getElementById("eye").addEventListener("click", function () {
        if (pwShown == 0) {
            pwShown = 1;
            show();
        } else {
            pwShown = 0;
            hide();
        }
    }, false);
</script>
@endsection