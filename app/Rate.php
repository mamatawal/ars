<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Scopes\ActiveScope;
use OwenIt\Auditing\Contracts\Auditable;

class Rate extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    
    protected $auditInclude = [
        'id','level','min','max','price','allowance','type_id','branch_id','created_by','updated_by','disable',
    ];
    
    protected static function boot()
    {
        parent::boot();
		
		static::addGlobalScope(new ActiveScope);
    }
	
	public function branch()
    {
        return $this->belongsTo('App\Branch');
    }
	
	public function type()
    {
        return $this->belongsTo('App\Type');
    }
	
	public function creator()
    {
        return $this->belongsTo('App\User', 'created_by');
    }
	
	public function updater()
    {
        return $this->belongsTo('App\User', 'updated_by');
    }
}
