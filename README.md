This is my very first project using PWA. This application was built on Laravel framework and database postgreSQL. 
These are the features/module on this system:-
1. Branch Management - Admin can manage all the branch in their company. Every branch will have different formula on parcel calculation
2. Rider Management - Admin can manage all rider (part time/full time/leader)
3. Payslip - Generate payslip for all rider