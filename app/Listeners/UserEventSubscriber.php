<?php

namespace App\Listeners;

use App\Events\Event;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Carbon\Carbon;

class UserEventSubscriber
{
	/**
     * Handle user login events.
     */
	public function onUserLogin($event) {
		$event->user->lastlogin = Carbon::now();
    	$event->user->save();
	}

    /**
     * Handle user logout events.
     */
    public function onUserLogout($event) {
		$event->user->lastlogout = Carbon::now();
    	$event->user->save();
	}

    /**
     * Register the listeners for the subscriber.
     *
     * @param  Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        $events->listen(
            'Illuminate\Auth\Events\Login',
            'App\Listeners\UserEventSubscriber@onUserLogin'
        );

        $events->listen(
            'Illuminate\Auth\Events\Logout',
            'App\Listeners\UserEventSubscriber@onUserLogout'
        );
    }
}
