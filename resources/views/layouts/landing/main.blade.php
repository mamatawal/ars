<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <title>ARS Rider Management System</title>
        
        <!-- Favicon Icon -->
        <link rel="shortcut icon" href="{{ URL::asset('contact/images/logo.png') }}" type="image/png" />
        <link rel="icon" href="{{ URL::asset('contact/images/logo.png') }}" type="image/png" />
        <!-- Favicon Icon -->
        
        <!-- ALL CSS -->
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('bespoke/css/bootstrap.css') }}">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('bespoke/css/style.css') }}" >
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('bespoke/css/preset.css') }}" >
		<style>
			@media print {
				body * {
					visibility: hidden;
				}
				#printarea, #printarea * {
					visibility: visible;
				}
				#printarea { display:block; }
			}
		</style>
    </head>
    <body>
        @yield('content')
    
    </body>
    @include('layouts.landing.footer')
</html>
