<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('riders', function (Blueprint $table) {
            $table->increments('id');
			$table->string('name');
			$table->string('icno');
			$table->string('email')->nullable();
			$table->string('phone')->nullable();
			$table->text('address')->nullable();
			$table->string('accno')->nullable();
			$table->integer('bank_id');
			$table->integer('branch_id');
			$table->string('emergencyname')->nullable();
			$table->string('emergencyphone')->nullable();
			$table->string('emergencyrelation')->nullable();
			$table->integer('created_by');
            $table->integer('updated_by');
            $table->string('disable');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('riders');
    }
}
