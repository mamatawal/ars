@extends('layouts/main/main')

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Summary of Salary</h2>
        <ol class="breadcrumb">
            <li>
                Pengurusan Gaji
            </li>
            <li>
                Summary of Salary
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content">
	<div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><strong>Branch</strong></label>
                                <div class="input-group date">
                                    <span class="input-group-addon">
                                        <i class="fa fa-university"></i>
                                    </span>
									{!! Form::select('branch', $branch, old('branch_id'), ['class' => 'form-control m-b chosen-branch', 'data-required' => 'true', 'placeholder' => 'Pilih Branch']) !!}
                                </div>
                            </div>
                        </div>
						<div class="col-md-6">
                            <div class="form-group">
                                <label><strong>Rider</strong></label>
                                <div class="input-group m-b">
                                    <span class="input-group-addon">
                                        <i class="fa fa-motorcycle"></i>
                                    </span>
                                    {!! Form::select('rider', $rider, old('rider_id'), ['class' => 'form-control m-b chosen-rider', 'data-required' => 'true', 'placeholder' => 'Pilih Rider']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
					<div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><strong>Bulan</strong></label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
									{!! Form::text('month_', date('m'), array('class' => 'form-control', 'onkeydown' => 'return false', 'readonly')) !!}
                                </div>
                            </div>
                        </div>
						<div class="col-md-6">
                            <div class="form-group">
                                <label><strong>Tahun</strong></label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
									{!! Form::text('year_', date('Y'), array('class' => 'form-control', 'onkeydown' => 'return false', 'readonly')) !!}
                                </div>
                            </div>
                        </div>
                    </div>
					<div class="row">
						<div class="col-md-12">
							<a id="filter-salary" class="btn btn-success pull-right">Jana</a>
						</div>
					</div>
                </div>
            </div>
        </div>
    </div>
	
	<div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Summary of Rider's Salary</h5>
					<ul class="navbar-top-links navbar-right navbar-right save" style="display:none" id="sectionprint">
                        <li class="dropdown">
                            <a id="printsummary" href="" target="_blank">
                                <i class="fa fa-print"></i> Print Summary
                            </a>
                        </li>
						<li class="dropdown">
                            <a id="printpayslip" href="" target="_blank">
                                <i class="fa fa-print"></i> Print Payslip
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="ibox-content">
					<div class="row">
						<div class="col-lg-6">
							<div class="row">
								<div class="col-lg-3">Nama</div>
								<div class="col-lg-1">:</div>
								<div class="col-lg-8"><span id="name"></span></div>
							</div>
							<div class="row">
								<div class="col-lg-3">No IC</div>
								<div class="col-lg-1">:</div>
								<div class="col-lg-8"><span id="icno"></span></div>
							</div>
							<div class="row">
								<div class="col-lg-3"><span id="bankcode"></span></div>
								<div class="col-lg-1">:</div>
								<div class="col-lg-8"><span id="bankacc"></span></div>
							</div>
							<div class="row">
								<div class="col-lg-3">Branch</div>
								<div class="col-lg-1">:</div>
								<div class="col-lg-8"><span id="branch"></span></div>
							</div>
							<div class="row">
								<div class="col-lg-3">Bulan</div>
								<div class="col-lg-1">:</div>
								<div class="col-lg-8"><span id="monthyear"></span></div>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="row">
								<div class="col-lg-3">Mode</div>
								<div class="col-lg-1">:</div>
								<div class="col-lg-8"><span id="mode"></span></div>
							</div>
						</div>
					</div>
					<hr>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover table-list-fdata">
                            <thead>
                                <tr>
                                    <th width="6%">Tarikh</th>
                                    <th>Parcel Dihantar</th>
								  	<th>Gaji Pokok Harian</th>
                                    <th>Elaun Parcel</th>
                                    <th>Jumlah Komisyen</th>
                                </tr>
                            </thead>
                            <tbody id="tbody">
								<tr><td colspan="5"><center>Tiada Data</center></td></tr>
                            </tbody>
                            <tfoot>
                                <tr>
									<th>Jumlah</th>
                                    <th class="text-right"><span id="totalinbound"></span></th>
								  	<th class="text-right"><span id="totalbasic"></span></th>
                                    <th class="text-right"><span id="totalallowance"></span></th>
                                    <th class="text-right"><span id="totalall"></span></th>
                                </tr>
								<tr>
									<th colspan="3" rowspan="10"></th>
									<th colspan="2">ADDITION</th>
								</tr>
								<tr>
									<td>SUPERVISION ALLOWANCE</td>
									<td><input type="text" class="form-control text-right kira" id="supervision" /></td>
								</tr>
								<tr>
									<td>OTHER ADDITION</td>
									<td><input type="text" class="form-control text-right kira" id="otheraddition" /></td>
								</tr>
								<tr>
									<td>ATTENDANCE INCENTIVE</td>
									<td><input type="text" class="form-control text-right kira" id="attendance" /></td>
								</tr>
								<tr>
									<td>PERFORMANCE INCENTIVE</td>
									<td><input type="text" class="form-control text-right kira" id="performance" /></td>
								</tr>
								<tr>
									<th colspan="2">DEDUCTION</th>
								</tr>
								<tr>
									<td>ADVANCE</td>
									<td><input type="text" class="form-control text-right kira" id="advance" /></td>
								</tr>
								<tr>
									<td>SOCSO</td>
									<td><input type="text" class="form-control text-right kira" id="socso" /></td>
								</tr>
								<tr>
									<td>OTHER DEDUCTION</td>
									<td><input type="text" class="form-control text-right kira" id="otherdeduction" /></td>
								</tr>
								<tr>
									<th>TOTAL SALARY</th>
									<th class="text-right"><span id="totalsalary"></span></th>
								</tr>
                            </tfoot>
                        </table>
                    </div>
					<div class="row">
						<div class="col-md-12">
							<a id="save" class="btn btn-success pull-right save" style="display:none">Simpan</a>
						</div>
					</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="{{ URL::asset('assets/parsley/parsley.min.js') }}"></script>
<script src="{{ URL::asset('assets/parsley/parsley.extend.js') }}"></script>
<script src="{{ URL::asset('inspinia/js/plugins/chosen/chosen.jquery.js') }}"></script>
<script src="{{ URL::asset('inspinia/js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>
<script src="{{ URL::asset('inspinia/js/plugins/sweetalert/sweetalert.min.js') }}"></script>
<script>
$(document).ready(function() {
    $('.chosen-branch, .chosen-rider').chosen({
        width: "100%"
    });
	
	$('[name="month_"]').datepicker({
		todayBtn: "linked",
		autoclose: true,
		minViewMode: "months",
		format: "mm"
	});

	$('[name="year_"]').datepicker({
		todayBtn: "linked",
		minViewMode: "years",
		autoclose: true,
		format: "yyyy"
	});
});
	
$('.chosen-branch').change(function () {
	$('.chosen-rider').html('').trigger("chosen:updated");
	
	$.ajax({
		type: 'GET',
		url : '{{ route("populateRider") }}',
		data : {
			id : $(this).val()
		},
		success:function(data){
			$('.chosen-rider').append('<option value="">Pilih Rider</option>').trigger("chosen:updated");;
			$.each(data, function(k,v){
				$('.chosen-rider').append('<option value="'+v.id+'">'+v.name+'</option>').trigger("chosen:updated");
			})
		}
	});
});

$('.chosen-rider').change(function () {
	if($('.chosen-branch').val() == ''){
		$.ajax({
			type: 'GET',
			url : '{{ route("populateBranch") }}',
			data : {
				id : $(this).val()
			},
			success:function(data){
				$('.chosen-branch').val(data).trigger('chosen:updated');
			}
		});
	}
});
	
$('#filter-salary').click(function () {
	if($('.chosen-rider').val() != ""){
		$('.save').show();
		
		var rider = $('.chosen-rider').val();
		var month_ = $('[name="month_"]').val();
		var year_ = $('[name="year_"]').val();
		var monthname = $('[name="month_"]').datepicker('getDate').toLocaleString('default', { month: 'short' });
		
		$("#printsummary").attr('href', '{{ url("salary-print") }}' + '?rider=' + rider + '&month_=' + month_ + '&year_=' + year_);
		
		$("#printpayslip").attr('href', '{{ url("salary-payslip") }}' + '?rider=' + rider + '&month_=' + month_ + '&year_=' + year_);

		$.ajax({
			url: '{{ url("ajaxViewRider") }}' + "&id=" + rider,
			type: "GET",
			dataType: "JSON",
			success: function(data){
				$('#name').html(data.name);
				$('#icno').html(data.icno);
				$('#bankcode').html(data.bank.code);
				$('#bankacc').html(data.accno);
				$('#branch').html(data.branch.name);
				$('#monthyear').html(monthname + ' ' + year_);
			},
			error: function(jqXHR, textStatus, errorThrown){
				alert('Error get data from ajax');
			}
		});
		
		$.ajax({
			type: 'GET',
			url : '{{ url("populateMode") }}',
			data : {
				rider : rider,
				month_ : month_,
				year_ : year_
			},
			success:function(data){
				$('#mode').html(data[0].modes);
			}
		});

		$.ajax({
			type: 'GET',
			url : '{{ url("populateSalary") }}',
			data : {
				rider : rider,
				month_ : month_,
				year_ : year_
			},
			success:function(data){
				$('#tbody').html("");
				$('#totalinbound').html("");
				$('#totalbasic').html("");
				$('#totalallowance').html("");
				$('#totalall').html("");
				
				var totalinbound = 0, totalbasic = 0, totalallowance = 0, totalall = 0;
				
				$.each(data, function(k, v){
					totalinbound += v['inbound'];
					totalbasic += parseFloat(v['basic']);
					totalallowance += parseFloat(v['allowance']);
					totalall += parseFloat(v['total']);
					
					$('#tbody').append(
						'<tr><td>'+v['date_field']+'</td><td class="text-right">'+v['inbound']+'</td><td class="text-right">'+parseFloat(v['basic']).toFixed(2)+'</td><td class="text-right">'+parseFloat(v['allowance']).toFixed(2)+'</td><td class="text-right">'+parseFloat(v['total']).toFixed(2)+'</td></tr>'
					);
				});
				
				$('#totalinbound').html(totalinbound);
				$('#totalbasic').html(totalbasic.toFixed(2));
				$('#totalallowance').html(totalallowance.toFixed(2));
				$('#totalall').html(totalall.toFixed(2));
			}
		});
		
		$.ajax({
			type: 'GET',
			url : '{{ url("populateTotalSalary") }}',
			data : {
				rider : rider,
				month_ : month_,
				year_ : year_
			},
			success:function(data){
				var supervision = data.supervision ? parseFloat(data.supervision).toFixed(2) : '';
				var otheraddition = data.otheraddition ? parseFloat(data.otheraddition).toFixed(2) : '';
				var attendance = data.attendance ? parseFloat(data.attendance).toFixed(2) : '';
				var performance = data.performance ? parseFloat(data.performance).toFixed(2) : '';
				var advance = data.advance ? parseFloat(data.advance).toFixed(2) : '';
				var socso = data.socso ? parseFloat(data.socso).toFixed(2) : '';
				var otherdeduction = data.otherdeduction ? parseFloat(data.otherdeduction).toFixed(2) : '';
				
				$('#supervision').val(supervision);
				$('#otheraddition').val(otheraddition);
				$('#attendance').val(attendance);
				$('#performance').val(performance);
				$('#advance').val(advance);
				$('#socso').val(socso);
				$('#otherdeduction').val(otherdeduction);
				
				kira();
			}
		});
	}else{
		swal("Amaran", "Sila pilih rider terlebih dahulu!", "error");
	}
});
	
$('#save').click(function () {
	$.ajax({
		type: 'GET',
		url : '{{ url("ajaxUpdateSalary") }}',
		data : {
			rider : $('.chosen-rider').val(),
			month_ : $('[name="month_"]').val(),
			year_ : $('[name="year_"]').val(),
			supervision : $('#supervision').val(),
			otheraddition : $('#otheraddition').val(),
			attendance : $('#attendance').val(),
			performance : $('#performance').val(),
			advance : $('#advance').val(),
			socso : $('#socso').val(),
			otherdeduction : $('#otherdeduction').val()
		},
		success:function(data){
			if(data.status == 'success'){
				kira();
				swal("Notifikasi", data.message, "success");
			}else{
				swal("Amaran", data.message, "error");
			}
		}
	});
});
	
$('.kira').on('change', function() {
	kira();
});
	
function kira() {
	no1 = $('#totalall').html() != '' ? parseFloat($('#totalall').html()) : 0.00;
	no2 = $('#supervision').val() != '' ? parseFloat($('#supervision').val()) : 0.00;
	no3 = $('#otheraddition').val() != '' ? parseFloat($('#otheraddition').val()) : 0.00;
	no4 = $('#attendance').val() != '' ? parseFloat($('#attendance').val()) : 0.00;
	no5 = $('#performance').val() != '' ? parseFloat($('#performance').val()) : 0.00;

	no6 = $('#advance').val() != '' ? parseFloat($('#advance').val()) : 0.00;
	no7 = $('#socso').val() != '' ? parseFloat($('#socso').val()) : 0.00;
	no8 = $('#otherdeduction').val() != '' ? parseFloat($('#otherdeduction').val()) : 0.00;

	result = no1 + no2 + no3 + no4 + no5 - no6 - no7 - no8;

	$('#totalsalary').html(result.toFixed(2));
}
</script>
@endsection