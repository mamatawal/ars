<?php

namespace App\Http\Middleware;

use Closure;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $admin = ['ADMIN'];
        
        if (!in_array($request->user()->role->name, $admin)) {
            return redirect('/dashboard')->with('statusdanger', 'Capaian akses anda tidak dibenarkan!');
        }
        return $next($request);
    }
}
