@extends('layouts/main/main')

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Parcel</h2>
        <ol class="breadcrumb">
            <li>
                Pengurusan Parcel
            </li>
            <li>
                Parcel
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content">
	<div class="row">
        <div class="col-lg-5">
            <div class="ibox float-e-margins">
				<div class="ibox-title">
                    <h5>Tapisan Data</h5>
                </div>
                <div class="ibox-content">
                    <div class="file-manager">
                        <div class="scroll_content">
                            <ul class="folder-list m-b-md" style="padding: 0">
                                <li>
                                    <a><i class="fa fa-calendar"></i> Tarikh Mula
                                    <span class="label label-warning pull-right">{{$startdate}}</span></a>
                                </li>
                                <li>
                                    <a><i class="fa fa-calendar"></i> Tarikh Akhir
                                    <span class="label label-warning pull-right">{{$enddate}}</span></a>
                                </li>
                                <li>
                                    <a><i class="fa fa-building"></i> Branch
                                    <span class="label label-warning pull-right">
                                        @if(!empty($branchname))
                                            {{$branchname}}
                                        @else
                                            Semua
                                        @endif
                                    </span></a>
                                </li>
                                <li>
                                    <a><i class="fa fa-motorcycle"></i> Rider
                                    <span class="label label-warning pull-right">
                                        @if(!empty($ridername))
                                            {{$ridername}}
                                        @else
                                            Semua
                                        @endif
                                    </span></a>
                                </li>
                                <li>
                                    <a><i class="fa fa-sliders"></i> Mode
                                    <span class="label label-warning pull-right">
                                        @if(!empty($modename))
                                            {{$modename}}
                                        @else
                                            Semua
                                        @endif
                                    </span></a>
                                </li>
                            </ul>
                        </div>
                        <div class="clearfix"></div>
                        <div class="space-25"></div>
						<a class="btn btn-block btn-success parcel-filter">Carian</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
	
    <div class="row">
            <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Senarai Parcel</h5>
					@if(!in_array(Auth::user()->id, ['3','4']))
                    <div class="ibox-tools">
                        <a class="btn btn-outline btn-success parcel-new">
                            Daftar Parcel
                        </a>
                    </div>
					@endif
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover table-list-parcel">
                            <thead>
                                <tr>
                                    <th>Tarikh</th>
                                    <th width="20%">Rider</th>
                                    <th>Mode</th>
                                    <th>Jumlah Bawa</th>
                                    <th>Jumlah Serah</th>
                                    <th>Jumlah Gagal Serah</th>
                                    <th>Jumlah Hilang</th>
                                    <th>Branch</th>
                                    <th width="10%">Tindakan</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>Tarikh</th>
                                    <th>Rider</th>
                                    <th>Mode</th>
                                    <th>Jumlah Bawa</th>
                                    <th>Jumlah Serah</th>
                                    <th>Jumlah Gagal Serah</th>
                                    <th>Jumlah Hilang</th>
                                    <th>Branch</th>
                                    <th>Tindakan</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal inmodal" id="modal-new-parcel">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Tutup</span></button>
                <h4 class="modal-title">Daftar Maklumat Parcel Baru</h4>
            </div>
            <form id="form-new-parcel">
				{{ csrf_field() }}
                <div class="modal-body">
                    <div class="message-form"></div>
					<div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><strong>Tarikh</strong> <i class="fa fa-asterisk text-danger"></i></label>
                                <div class="input-group date">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
									{!! Form::text('dateout', date('d/m/Y'), array('class' => 'form-control', 'onkeydown' => 'return false', 'readonly')) !!}
                                </div>
                            </div>
                        </div>
						<div class="col-md-6">
                            <div class="form-group">
                                <label><strong>Branch</strong> <i class="fa fa-asterisk text-danger"></i></label>
                                <div class="input-group m-b">
                                    <span class="input-group-addon">
                                        <i class="fa fa-university"></i>
                                    </span>
                                    {!! Form::select('branch', $branch, old('branch_id'), ['class' => 'form-control m-b chosen-branch', 'data-required' => 'true', 'placeholder' => 'Pilih Branch']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label><strong>Rider</strong> <i class="fa fa-asterisk text-danger"></i></label>
                                <div class="input-group m-b">
                                    <span class="input-group-addon">
                                        <i class="fa fa-motorcycle"></i>
                                    </span>
                                    {!! Form::select('rider', $rider, old('rider_id'), ['class' => 'form-control m-b chosen-rider', 'data-required' => 'true', 'placeholder' => 'Pilih Rider']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
						<div class="col-md-6">
                            <div class="form-group">
                                <label><strong>Mode</strong> <i class="fa fa-asterisk text-danger"></i></label>
                                <div class="input-group m-b">
                                    <span class="input-group-addon">
                                        <i class="fa fa-sliders"></i>
                                    </span>
                                    {!! Form::select('type', $type, old('type_id'), ['class' => 'form-control m-b chosen-type', 'data-required' => 'true', 'placeholder' => 'Pilih Mode']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
					<div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><strong>Jumlah Bawa</strong> <i class="fa fa-asterisk text-danger"></i></label>
                                <div class="input-group m-b">
                                    <span class="input-group-addon">
                                        <i class="fa fa-arrow-right"></i>
                                    </span>
                                    <input type="number" name="outbound" class="form-control kira" placeholder="Jumlah Bawa" data-required="true" />
                                </div>
                            </div>
                        </div>
						<div class="col-md-6">
                            <div class="form-group">
                                <label><strong>Jumlah Serah</strong> <i class="fa fa-asterisk text-danger"></i></label>
                                <div class="input-group m-b">
                                    <span class="input-group-addon">
                                        <i class="fa fa-arrow-left"></i>
                                    </span>
									<input type="number" name="inbound" class="form-control kira" placeholder="Jumlah Serah" data-required="true" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><strong>Jumlah Gagal Serah</strong> <i class="fa fa-asterisk text-danger"></i></label>
                                <div class="input-group m-b">
                                    <span class="input-group-addon">
                                        <i class="fa fa-exchange"></i>
                                    </span>
									<input type="number" name="failed" class="form-control" placeholder="Jumlah Gagal Serah" data-required="true" />
                                </div>
                            </div>
                        </div>
						<div class="col-md-6">
                            <div class="form-group">
                                <label><strong>Jumlah Hilang</strong> <i class="fa fa-asterisk text-danger"></i></label>
                                <div class="input-group m-b">
                                    <span class="input-group-addon">
                                        <i class="fa fa-arrows-alt"></i>
                                    </span>
                                    <input type="number" name="lost" class="form-control" placeholder="Jumlah Hilang" data-required="true" value="0"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a id="register-parcel" class="btn btn-primary">Daftar</a>
                    <a class="btn btn-default" data-dismiss="modal">Tutup</a>
                </div>
            </form>    
        </div>
    </div>
</div>

<div class="modal inmodal" id="modal-edit-parcel">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Tutup</span></button>
                <h4 class="modal-title">Kemaskini Maklumat Parcel</h4>
            </div>
            <form id="form-edit-parcel">
				{{ csrf_field() }}
                <div class="modal-body">
                    <div class="message-form"></div>
					<input type="hidden" name="e_id">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><strong>Tarikh</strong> <i class="fa fa-asterisk text-danger"></i></label>
                                <div class="input-group date">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
									{!! Form::text('e_dateout', null, array('class' => 'form-control', 'onkeydown' => 'return false', 'readonly')) !!}
                                </div>
                            </div>
                        </div>
						<div class="col-md-6">
                            <div class="form-group">
                                <label><strong>Branch</strong> <i class="fa fa-asterisk text-danger"></i></label>
                                <div class="input-group m-b">
                                    <span class="input-group-addon">
                                        <i class="fa fa-university"></i>
                                    </span>
									{!! Form::select('e_branch', $branch, old('branch_id'), ['class' => 'form-control m-b chosen-e-branch', 'data-required' => 'true', 'placeholder' => 'Pilih Branch']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label><strong>Rider</strong> <i class="fa fa-asterisk text-danger"></i></label>
                                <div class="input-group m-b">
                                    <span class="input-group-addon">
                                        <i class="fa fa-motorcycle"></i>
                                    </span>
                                    {!! Form::select('e_rider', $rider, old('rider_id'), ['class' => 'form-control m-b chosen-e-rider', 'data-required' => 'true', 'placeholder' => 'Pilih Rider']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
						<div class="col-md-6">
                            <div class="form-group">
                                <label><strong>Mode</strong> <i class="fa fa-asterisk text-danger"></i></label>
                                <div class="input-group m-b">
                                    <span class="input-group-addon">
                                        <i class="fa fa-sliders"></i>
                                    </span>
                                    {!! Form::select('e_type', $type, old('type_id'), ['class' => 'form-control m-b chosen-e-type', 'data-required' => 'true', 'placeholder' => 'Pilih Mode']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
					<div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><strong>Jumlah Bawa</strong> <i class="fa fa-asterisk text-danger"></i></label>
                                <div class="input-group m-b">
                                    <span class="input-group-addon">
                                        <i class="fa fa-arrow-right"></i>
                                    </span>
                                    <input type="number" name="e_outbound" class="form-control kiraedit" placeholder="Jumlah Bawa" data-required="true" />
                                </div>
                            </div>
                        </div>
						<div class="col-md-6">
                            <div class="form-group">
                                <label><strong>Jumlah Serah</strong> <i class="fa fa-asterisk text-danger"></i></label>
                                <div class="input-group m-b">
                                    <span class="input-group-addon">
                                        <i class="fa fa-arrow-left"></i>
                                    </span>
									<input type="number" name="e_inbound" class="form-control kiraedit" placeholder="Jumlah Serah" data-required="true" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><strong>Jumlah Gagal Serah</strong> <i class="fa fa-asterisk text-danger"></i></label>
                                <div class="input-group m-b">
                                    <span class="input-group-addon">
                                        <i class="fa fa-exchange"></i>
                                    </span>
									<input type="number" name="e_failed" class="form-control" placeholder="Jumlah Gagal Serah" data-required="true" />
                                </div>
                            </div>
                        </div>
						<div class="col-md-6">
                            <div class="form-group">
                                <label><strong>Jumlah Hilang</strong> <i class="fa fa-asterisk text-danger"></i></label>
                                <div class="input-group m-b">
                                    <span class="input-group-addon">
                                        <i class="fa fa-arrows-alt"></i>
                                    </span>
                                    <input type="number" name="e_lost" class="form-control" placeholder="Jumlah Hilang" data-required="true" value="0"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a id="update-parcel" class="btn btn-primary">Simpan</a>
                    <a class="btn btn-default" data-dismiss="modal">Tutup</a>
                </div>
            </form>    
        </div>
    </div>
</div>

<div class="modal inmodal" id="modal-delete-parcel">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Hapus Maklumat Parcel
                    <br /><small>Anda pasti untuk menghapuskan maklumat parcel ini?</small>
                </h4>
            </div>
            <form id="form-delete-parcel">
				{{ csrf_field() }}
                <div class="modal-body">
                    <div class="message-form"></div>
					<input type="hidden" name="d_id">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><strong>Tarikh</strong></label>
                                <div class="input-group m-b">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
									<input type="text" name="d_dateout" class="form-control" placeholder="Tarikh" disabled="true" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><strong>Branch</strong></label>
                                <div class="input-group m-b">
                                    <span class="input-group-addon">
                                        <i class="fa fa-university"></i>
                                    </span>
									<input type="text" name="d_branch" class="form-control" placeholder="Branch" disabled="true" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label><strong>Rider</strong></label>
                                <div class="input-group m-b">
                                    <span class="input-group-addon">
                                        <i class="fa fa-motorcycle"></i>
                                    </span>
									<input type="text" name="d_rider" class="form-control" placeholder="Rider" disabled="true" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
						<div class="col-md-6">
                            <div class="form-group">
                                <label><strong>Mode</strong></label>
                                <div class="input-group m-b">
                                    <span class="input-group-addon">
                                        <i class="fa fa-sliders"></i>
                                    </span>
									<input type="text" name="d_type" class="form-control" placeholder="Mode" disabled="true" />
                                </div>
                            </div>
                        </div>
                    </div>
					<div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><strong>Jumlah Bawa</strong></label>
                                <div class="input-group m-b">
                                    <span class="input-group-addon">
                                        <i class="fa fa-arrow-right"></i>
                                    </span>
                                    <input type="text" name="d_outbound" class="form-control" placeholder="Jumlah Bawa" disabled="true" />
                                </div>
                            </div>
                        </div>
						<div class="col-md-6">
                            <div class="form-group">
                                <label><strong>Jumlah Serah</strong></label>
                                <div class="input-group m-b">
                                    <span class="input-group-addon">
                                        <i class="fa fa-arrow-left"></i>
                                    </span>
									<input type="text" name="d_inbound" class="form-control" placeholder="Jumlah Serah" disabled="true" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><strong>Jumlah Gagal Serah</strong></label>
                                <div class="input-group m-b">
                                    <span class="input-group-addon">
                                        <i class="fa fa-exchange"></i>
                                    </span>
									<input type="text" name="d_failed" class="form-control" placeholder="Jumlah Gagal Serah" disabled="true" />
                                </div>
                            </div>
                        </div>
						<div class="col-md-6">
                            <div class="form-group">
                                <label><strong>Jumlah Hilang</strong></label>
                                <div class="input-group m-b">
                                    <span class="input-group-addon">
                                        <i class="fa fa-arrows-alt"></i>
                                    </span>
                                    <input type="text" name="d_lost" class="form-control" placeholder="Jumlah Hilang" disabled="true" value="0"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a id="delete-parcel" class="btn btn-primary">Hapus</a>
                    <a class="btn btn-default" data-dismiss="modal">Tutup</a>
                </div>
            </form>    
        </div>
    </div>
</div>

<div class="modal inmodal" id="modal-filter-fdata">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Tutup</span></button>
                <h4 class="modal-title">Tapisan Data Parcel</h4>
            </div>
            <form id="form-filter-fdata" method="POST" action="{{url('/parcelList')}}">
                {{ csrf_field() }}
                <div class="modal-body">
					<div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><strong>Tarikh Mula</strong></label>
                                <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <?php echo Form::text('startdate',$startdate,array('class' => 'form-control', 'onkeydown' => 'return false', 'readonly', 'id' => 'startdate'));?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><strong>Tarikh Akhir</strong></label>
                                <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <?php echo Form::text('enddate',$enddate,array('class' => 'form-control','onkeydown' => 'return false', 'readonly', 'id' => 'enddate'));?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
						<div class="col-md-6">
                            <div class="form-group">
                                <label><strong>Branch</strong></label>
                                <div class="input-group m-b">
                                    <span class="input-group-addon">
                                        <i class="fa fa-university"></i>
                                    </span>
									{!! Form::select('branch_id', $branch, old('branch_id'), ['class' => 'form-control m-b chosen-branch-filter', 'placeholder' => 'Semua', 'id' => 'branch_id']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label><strong>Rider</strong></label>
                                <div class="input-group m-b">
                                    <span class="input-group-addon">
                                        <i class="fa fa-motorcycle"></i>
                                    </span>
                                    {!! Form::select('rider_id', $rider, old('rider_id'), ['class' => 'form-control m-b chosen-rider-filter', 'placeholder' => 'Semua', 'id' => 'rider_id']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
						<div class="col-md-6">
                            <div class="form-group">
                                <label><strong>Mode</strong></label>
                                <div class="input-group m-b">
                                    <span class="input-group-addon">
                                        <i class="fa fa-sliders"></i>
                                    </span>
                                    {!! Form::select('type_id', $type, old('type_id'), ['class' => 'form-control m-b chosen-type-filter', 'placeholder' => 'Semua', 'id' => 'type_id']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="filter-fdata" class="btn btn-primary" type="submit">Tapis</button>
                    <a class="btn btn-default" data-dismiss="modal">Tutup</a>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="{{ URL::asset('inspinia/js/plugins/dataTables/datatables.min.js') }}"></script>
<script src="{{ URL::asset('assets/parsley/parsley.min.js') }}"></script>
<script src="{{ URL::asset('assets/parsley/parsley.extend.js') }}"></script>
<script src="{{ URL::asset('inspinia/js/plugins/chosen/chosen.jquery.js') }}"></script>
<script src="{{ URL::asset('inspinia/js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>
<script src="{{ URL::asset('contact/js/parcel.js') }}"></script>
<script>  
$('.table-list-parcel').DataTable({
	aaSorting: [],
	processing: true,
	serverSide: true,
	ajax: '{{ route('getParcelData') }}?startdate='+$('#startdate').val()+'&enddate='+$('#enddate').val()+'&rider_id='+$('#rider_id').val()+'&type_id='+$('#type_id').val()+'&branch_id='+$('#branch_id').val(),
	columns: [
		{data: 'date', name: 'date'},
		{data: 'rider', name: 'rider.name'},
		{data: 'type', name: 'type.name'},
		{data: 'outbound', name: 'outbound'},
		{data: 'inbound', name: 'inbound'},
		{data: 'failed', name: 'failed'},
		{data: 'lost', name: 'lost'},
		{data: 'branch', name: 'branch.name'},
		{data: 'action', name: 'action'},
	],
	pageLength: 25,
	responsive: true,
	dom: 'lTfgitp',
	language: {
		decimal:        "",
		emptyTable:     "Tiada data",
		info:           "Paparan _START_ sehingga _END_ daripada _TOTAL_ rekod",
		infoEmpty:      "Paparan 0 sehingga 0 daripada 0 rekod",
		infoFiltered:   "(Tapisan daripada _MAX_ jumlah rekod)",
		infoPostFix:    "",
		thousands:      ",",
		lengthMenu:     "Paparan _MENU_ rekod",
		loadingRecords: "Sedang memuatkan...",
		processing:     "Sedang diproses...",
		search:         "Carian:",
		zeroRecords:    "Tiada rekod yang dijumpai",
		paginate: {
			first:      "Pertama",
			last:       "Terakhir",
			next:       "Berikut",
			previous:   "Terdahulu"
		},
		aria: {
			sortAscending:  ": aktif untuk susunan jaluran menaik",
			sortDescending: ": aktif untuk susunan jaluran menurun"
		}
	}
});
	
$(document).on('click','.parcel-filter',function() {
    $('#modal-filter-fdata').modal('show');
    $('#form-filter-fdata')[0].reset();
});
        
$(document).on('click','.parcel-edit',function() {
	var id = $(this).data('id');
    $('#modal-edit-parcel').modal('show');
    $('#form-edit-parcel')[0].reset();
	$('.message-form').html('');
	
	$.ajax({
		url: "ajaxViewParcel&id=" + id,
		type: "GET",
		dataType: "JSON",
		success: function(data){
			$('[name="e_id"]').val(data.id);
			$('[name="e_dateout"]').val(data.tarikh);
			$('[name="e_branch"]').val(data.branch_id).trigger('chosen:updated');
			$('[name="e_rider"]').val(data.rider_id).trigger('chosen:updated');
			$('[name="e_type"]').val(data.type_id).trigger('chosen:updated');
			$('[name="e_outbound"]').val(data.outbound);
			$('[name="e_inbound"]').val(data.inbound);
			$('[name="e_failed"]').val(data.failed);
			$('[name="e_lost"]').val(data.lost);
		},
		error: function(jqXHR, textStatus, errorThrown){
			alert('Error get data from ajax');
		}
	});
});
	
$('.input-group.date').datepicker({
	todayBtn: "linked",
	keyboardNavigation: false,
	forceParse: false,
	calendarWeeks: false,
	autoclose: true,
	format: "dd/mm/yyyy"
});
	
$('.chosen-branch').change(function () {
	$('.chosen-rider').html('').trigger("chosen:updated");
	
	$.ajax({
		type: 'GET',
		url : '{{ route("populateRider") }}',
		data : {
			id : $(this).val()
		},
		success:function(data){
			$('.chosen-rider').append('<option value="">Pilih Rider</option>').trigger("chosen:updated");;
			$.each(data, function(k,v){
				$('.chosen-rider').append('<option value="'+v.id+'">'+v.name+'</option>').trigger("chosen:updated");
			})
		}
	});
});
	
$('.chosen-branch-filter').change(function () {
	$('.chosen-rider-filter').html('').trigger("chosen:updated");
	
	$.ajax({
		type: 'GET',
		url : '{{ route("populateRider") }}',
		data : {
			id : $(this).val()
		},
		success:function(data){
			$('.chosen-rider-filter').append('<option value="">Semua</option>').trigger("chosen:updated");;
			$.each(data, function(k,v){
				$('.chosen-rider-filter').append('<option value="'+v.id+'">'+v.name+'</option>').trigger("chosen:updated");
			})
		}
	});
});

$('.chosen-e-branch').change(function () {
	$('.chosen-e-rider').html('').trigger("chosen:updated");
	
	$.ajax({
		type: 'GET',
		url : '{{ route("populateRider") }}',
		data : {
			id : $(this).val()
		},
		success:function(data){
			$('.chosen-e-rider').append('<option value="">Pilih Rider</option>').trigger("chosen:updated");;
			$.each(data, function(k,v){
				$('.chosen-e-rider').append('<option value="'+v.id+'">'+v.name+'</option>').trigger("chosen:updated");
			})
		}
	});
});
	
$('.chosen-rider').change(function () {
	if($('.chosen-branch').val() == ''){
		$.ajax({
			type: 'GET',
			url : '{{ route("populateBranch") }}',
			data : {
				id : $(this).val()
			},
			success:function(data){
				$('.chosen-branch').val(data).trigger('chosen:updated');
			}
		});
	}
});	

$('.chosen-rider-filter').change(function () {
	if($('.chosen-branch-filter').val() == ''){
		$.ajax({
			type: 'GET',
			url : '{{ route("populateBranch") }}',
			data : {
				id : $(this).val()
			},
			success:function(data){
				$('.chosen-branch-filter').val(data).trigger('chosen:updated');
			}
		});
	}
});
	
$('.chosen-e-rider').change(function () {
	$.ajax({
		type: 'GET',
		url : '{{ route("populateBranch") }}',
		data : {
			id : $(this).val()
		},
		success:function(data){
			$('.chosen-e-branch').val(data).trigger('chosen:updated');
		}
	});
});
	
$('.kira').on('change', function() {
		no1 = $('[name="outbound"]').val();
		no2 = $('[name="inbound"]').val();
		result = no1 - no2;

		$('[name="failed"]').val(result);
});
	
$('.kiraedit').on('change', function() {
		no1 = $('[name="e_outbound"]').val();
		no2 = $('[name="e_inbound"]').val();
		result = no1 - no2;

		$('[name="e_failed"]').val(result);
});
</script>
@endsection