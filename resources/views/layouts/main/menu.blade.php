<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 100%;">
        <div class="sidebar-collapse" style="overflow: hidden; width: auto; height: 100%;">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element">
                        <span>
                            <img alt="image" src="{{ URL::asset('contact/images/logo.png') }}" width="150" />
                        </span>
                    </div>
                </li>
                <li class="{{ Request::is('dashboard') ? 'active' : '' }}">
                    <a href="{{ url('dashboard')}}">
                        <span class="pull-left">
                            <i class="fa fa-bar-chart-o"></i>
                        </span>
                        <span class="clear">
                            <span class="nav-label menuload">Laman Utama</span>
                        </span>
                    </a>
                </li>
                <li class="{{ (preg_match('/(parcel)/', Request::path()) ? 'active' : '') }}">
                    <a href="{{ url('parcelList') }}">
                        <span class="pull-left">
                            <i class="fa fa-briefcase"></i>
                        </span>
                        <span class="clear">
                            <span class="nav-label">Pengurusan Parcel</span>
                        </span>
                    </a>
                </li>
				@if(Auth::user()->admin() && !in_array(Auth::user()->id, ['3','4']))
				<li class="{{ (preg_match('/(salary)/', Request::path()) ? 'active' : '') }}">
                    <a href="{{ url('salary')}}">
                        <span class="pull-left">
                        <i class="fa fa-money"></i>
                    </span>
                        <span class="clear">
                        	<span class="nav-label menuload">Pengurusan Gaji</span>
                        </span>
                    </a>
                </li>
				@endif
				@if(!in_array(Auth::user()->id, ['3','4']))
                <li class="{{ (preg_match('/(rider)/', Request::path()) ? 'active' : '') }}">
                    <a href="#">
                        <span class="pull-left">
                            <i class="fa fa-motorcycle"></i>
                        </span>
                        <span class="clear">
                            <span class="nav-label">Pengurusan Rider</span> <span class="fa arrow"></span>
                        </span>
                    </a>
                    <ul class="nav nav-second-level">
                        <li class="{{ Request::is('riderList') ? 'active' : '' }} menuload">
                            <a href="{{ url('riderList') }}">Rider</a>
                        </li>
                        @if(Auth::user()->admin())
                        <li class="{{ Request::is('riderMode') ? 'active' : '' }} menuload">
                            <a href="{{ url('riderMode') }}">Mode</a>
                        </li>
                        @endif
                    </ul>
                </li>
				@endif
                @if(Auth::user()->admin() && !in_array(Auth::user()->id, ['3','4']))
				<li class="{{ (preg_match('/(branch)/', Request::path()) ? 'active' : '') }}">
                    <a href="{{ url('branch')}}">
                        <span class="pull-left">
                        <i class="fa fa-building"></i>
                    </span>
                        <span class="clear">
                        <span class="nav-label menuload">Pengurusan Branch</span>
                        </span>
                    </a>
                </li>
				<li class="{{ (preg_match('/(bank)/', Request::path()) ? 'active' : '') }}">
                    <a href="{{ url('bank')}}">
                        <span class="pull-left">
                        <i class="fa fa-university"></i>
                    </span>
                        <span class="clear">
                        <span class="nav-label menuload">Pengurusan Bank</span>
                        </span>
                    </a>
                </li>
                <li class="{{ (preg_match('/(user)/', Request::path()) ? 'active' : '') }}">
                    <a href="#">
                        <span class="pull-left">
                            <i class="fa fa-users"></i>
                        </span>
                        <span class="clear">
                            <span class="nav-label">Pengurusan Pengguna</span> <span class="fa arrow"></span>
                        </span>
                    </a>
                    <ul class="nav nav-second-level">
                        <li class="{{ Request::is('userUser') ? 'active' : '' }} menuload">
                            <a href="{{ url('userUser') }}">Pengguna</a>
                        </li>
                        <li class="{{ Request::is('userRole') ? 'active' : '' }} menuload">
                            <a href="{{ url('userRole') }}">Peranan</a>
                        </li>
                    </ul>
                </li>
				<li class="{{ (preg_match('/(audit)/', Request::path()) ? 'active' : '') }}">
                    <a href="{{ url('audit')}}">
                        <span class="pull-left">
                        <i class="fa fa-history"></i>
                    </span>
                        <span class="clear">
                        <span class="nav-label menuload">Audit Trail</span>
                        </span>
                    </a>
                </li>
                @endif
            </ul>
        </div>
    </div>
</nav>