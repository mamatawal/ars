<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Scopes\ActiveScope;
use OwenIt\Auditing\Contracts\Auditable;

class Parcel extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    
    protected $auditInclude = [
        'id','outbound','inbound','failed','lost','dateout','rider_id','type_id','branch_id','created_by','updated_by','disable',
    ];
    
    protected static function boot()
    {
        parent::boot();
		
		static::addGlobalScope(new ActiveScope);
    }
	
	public function branch()
    {
        return $this->belongsTo('App\Branch');
    }
	
	public function type()
    {
        return $this->belongsTo('App\Type');
    }
	
	public function rider()
    {
        return $this->belongsTo('App\Rider');
    }
	
	public function creator()
    {
        return $this->belongsTo('App\User', 'created_by');
    }
	
	public function updater()
    {
        return $this->belongsTo('App\User', 'updated_by');
    }
}
