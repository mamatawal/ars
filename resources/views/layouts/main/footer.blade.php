    <!-- Mainly scripts -->
	<script src="{{ URL::asset('inspinia/js/jquery-3.1.1.min.js') }}"></script>
	<script src="{{ URL::asset('inspinia/js/bootstrap.min.js') }}"></script>
	<script src="{{ URL::asset('inspinia/js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
	<script src="{{ URL::asset('inspinia/js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>

	<!-- Custom and plugin javascript -->
	<script src="{{ URL::asset('inspinia/js/inspinia.js') }}"></script>
	<script src="{{ URL::asset('inspinia/js/plugins/pace/pace.min.js') }}"></script>
	<script src="{{ URL::asset('contact/js/blockUI/jquery.blockUI.js') }}"></script>
	<script src="/install.js"></script>
	<script type="text/javascript">
		if ('serviceWorker' in navigator) {
		  window.addEventListener('load', () => {
			navigator.serviceWorker.register('/service-worker.js')
				.then((reg) => {
				  console.log('Service worker registered.', reg);
				});
		  });
		}

		$('input').attr('autocomplete','off');
	
		var logo = "{{ URL::asset('contact/images/logo144.png') }}";
		var loader = "{{ URL::asset('contact/images/22.gif') }}";
		$.blockUI({ 
	            message: '<img src="'+logo+'" width="80"/><br /><img src="'+loader+'" />',
	            css: { 
	                border: 'none', 
	                padding: '15px', 
	                backgroundColor: '#000', 
	                '-webkit-border-radius': '10px', 
	                '-moz-border-radius': '10px', 
	                opacity: .5, 
	                color: '#fff' 
	            }
	        });
	    $.blockUI.defaults.baseZ = 500000; 
        
        $(document).on('click','.menuload',function() {
            $.blockUI({ 
                message: '<img src="'+logo+'" width="80"/><br /><img src="'+loader+'" />',
                css: { 
                    border: 'none', 
                    padding: '15px', 
                    backgroundColor: '#000', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    opacity: .5, 
                    color: '#fff' 
                }
            });
        });
		
		$(document).ready(function(){
			$(document).ajaxStart(function() {
				$.blockUI({ 
					message: '<img src="'+logo+'" width="80"/><br /><img src="'+loader+'" />',
					css: { 
						border: 'none', 
						padding: '15px', 
						backgroundColor: '#000', 
						'-webkit-border-radius': '10px', 
						'-moz-border-radius': '10px', 
						opacity: .5, 
						color: '#fff' 
					}
				});
			});
			$.blockUI.defaults.baseZ = 500000;

			$(document).ajaxStop(function() {
				$.unblockUI();
			});
		});
	</script>
	<script type="text/javascript">   
	$(document).ready(function() { 
	   $.unblockUI(); //only make sure the document is full loaded, including scripts.  
	});  
	</script>
	@yield('js')