@extends('layouts/main/print')

@section('content')
<div class="wrapper wrapper-content p-xl">
	<div class="row">
		<div class="col-sm-12">
			<h5>NAME : {{ $rider->name }}</h5>
			<h5>NO IC : {{ $rider->icno }}</h5>
			{{ $rider->bank->code }} : {{ $rider->accno }}<br>
			PPL : {{ $rider->branch->name }}<br>
			MONTH : {{ $monthname }}
			<span class="pull-right">MODE : {{ $modes }}</span><br>
		</div>
	</div>

	<div class="table-responsive m-t">
		<table class="table invoice-table">
			<thead>
				<tr>
					<th>Tarikh</th>
					<th>Parcel Dihantar</th>
					<th>Gaji Pokok Harian</th>
					<th>Elaun Parcel</th>
					<th>Jumlah Komisyen</th>
				</tr>
			</thead>
			<tbody id="tbody">
				<?php 
					$totalinbound = 0;
					$totalbasic = 0;
					$totalallowance = 0;
					$totalall = 0;
					
					foreach($rs as $r){
						$totalinbound += $r->inbound;
						$totalbasic += $r->basic;
						$totalallowance += $r->allowance;
						$totalall += $r->total; 
				?>
					<tr>
						<td>{{ $r->date_field }}</td>
						<td class="text-right">{{ $r->inbound }}</td>
						<td class="text-right">{{ number_format($r->basic, 2, '.', '') }}</td>
						<td class="text-right">{{ number_format($r->allowance, 2, '.', '') }}</td>
						<td class="text-right">{{ number_format($r->total, 2, '.', '') }}</td>
					</tr>
					<?php } ?>
					<tr>
						<th>Jumlah</th>
						<th class="text-right">{{ $totalinbound }}</th>
						<th class="text-right">{{ number_format($totalbasic, 2, '.', '') }}</th>
						<th class="text-right">{{ number_format($totalallowance, 2, '.', '') }}</th>
						<th class="text-right">{{ number_format($totalall, 2, '.', '') }}</th>
					</tr>
				</tbody>
		</table>
	</div><!-- /table-responsive -->

	<table class="table invoice-total">
		<tbody>
			<tr>
				<td><strong>ADDITION :-</strong></td>
				<td></td>
			</tr>
			<tr>
				<td>SUPERVISION ALLOWANCE :</td>
				<td>{{ number_format($salary->supervision, 2, '.', '') }}</td>
			</tr>
			<tr>
				<td>OTHER ADDITION :</td>
				<td>{{ number_format($salary->otheraddition, 2, '.', '') }}</td>
			</tr>
			<tr>
				<td>ATTENDANCE INCENTIVE :</td>
				<td>{{ number_format($salary->attendance, 2, '.', '') }}</td>
			</tr>
			<tr>
				<td>PERFORMANCE INCENTIVE :</td>
				<td>{{ number_format($salary->performance, 2, '.', '') }}</td>
			</tr>
			<tr>
				<td><strong>DEDUCTION :-</strong></td>
				<td></td>
			</tr>
			<tr>
				<td>ADVANCE :</td>
				<td>{{ number_format($salary->advance, 2, '.', '') }}</td>
			</tr>
			<tr>
				<td>SOCSO :</td>
				<td>{{ number_format($salary->socso, 2, '.', '') }}</td>
			</tr>
			<tr>
				<td>OTHER DEDUCTION :</td>
				<td>{{ number_format($salary->otherdeduction, 2, '.', '') }}</td>
			</tr>
			<?php 
				$totalsalary = $totalall + $salary->supervision + $salary->otheraddition + $salary->attendance + $salary->performance - $salary->advance - $salary->socso - $salary->otherdeduction;
			?>
			<tr>
				<td><strong>TOTAL SALARY :</strong></td>
				<td>{{ number_format($totalsalary, 2, '.', '') }}</td>
			</tr>
		</tbody>
	</table>
	<div class="well m-t">
		<center>*THIS IS COMPUTER GENERATED, NO SIGNATURE NEEDED*</center>
	</div>
</div>
@endsection