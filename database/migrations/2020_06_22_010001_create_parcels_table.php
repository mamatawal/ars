<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParcelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parcels', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('outbound');
			$table->integer('inbound')->nullable();
            $table->integer('failed')->nullable();
			$table->integer('lost')->nullable();
			$table->timestamp('dateout')->nullable();
			$table->integer('level1_id');
			$table->integer('level2_id')->nullable();
			$table->integer('level3_id')->nullable();
            $table->integer('rider_id');
            $table->integer('type_id');
            $table->integer('branch_id');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->string('disable');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parcels');
    }
}
