@extends('layouts.login.main')

@section('content')
<div class="row p-sm login-overlay">
    <div class="row">
        <div class="col-md-6">
            <p class="text-white">
                <small>ARS Rider Management System</small>
            </p>
			<center><img src="{{ URL::asset('contact/images/logo.png') }}" width="300"/></center>
            <p>&nbsp;</p>
        </div>
        <div class="col-md-6">
            <div class="ibox-content">
                <form id="password-renew-form" name="formemail" action="{{ route('password.email') }}" method="post">
					{{ csrf_field() }}
                    @if (session('status'))
					<div class="alert alert-success contact-alert">
						<strong>
							{{ session('status') }}
						</strong>
					</div>
                    @endif
                    @if ($errors->has('reset'))
					<div class="alert alert-danger contact-alert">
						<strong>
							{{ $errors->first('reset') }}
						</strong>
					</div>
                    @endif
					<div class="form-group{{ $errors->has('icno') || $errors->has('reset') ? ' has-error' : '' }}">    
						<label><strong>Nombor Kad Pengenalan</strong></label>
						<div class="input-group m-b">
							<span class="input-group-addon">
								<i class="fa fa-address-card-o"></i>
							</span>
							<input type="text" name="icno" class="form-control" placeholder="Nombor Kad Pengenalan" data-required="true" value="{{ old('icno') }}"/>
						</div>
						@if ($errors->has('icno'))
							<span class="help-block">
								<strong>{{ $errors->first('icno') }}</strong>
							</span>
						@endif
					</div>
					<div class="form-group{{ $errors->has('email') || $errors->has('reset') ? ' has-error' : '' }}">    
						<label><strong>Alamat Emel</strong></label>
						<div class="input-group m-b">
							<span class="input-group-addon">
								<i class="fa fa-envelope-o"></i>
							</span>
							<input type="email" name="email" class="form-control" placeholder="Alamat Emel" data-required="true" value="{{ old('email') }}"/>
						</div>
						@if ($errors->has('email'))
							<span class="help-block">
								<strong>{{ $errors->first('email') }}</strong>
							</span>
						@endif
					</div>
                    <button type="submit" class="btn btn-success block full-width m-b">Hantar Link Reset Kata Laluan</button>
                    <span class="pull-right">
                        <i class="fa fa-home"></i> <a href="/"><small>Utama</small></a>
                    </span> 
                </form>
                <p class="m-t">
                    <small>
                        ARS Rider Management System <a class="block-login"><span class="badge badge-success">ARS</span></a>
                    </small>
                </p>
            </div>
        </div>
    </div>
</div>
@endsection
